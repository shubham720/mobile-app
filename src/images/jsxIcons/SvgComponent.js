import * as React from "react"
import Svg, { G, Rect, Path } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={39}
      height={36}
      viewBox="0 0 39 36"
      {...props}
    >
      <G data-name="Group 607">
        <G data-name="Group 366">
          <Rect
            data-name="Rectangle 138"
            width={39}
            height={36}
            rx={10}
            fill="#2a3795"
          />
        </G>
        <Path
          d="M12 23a2.534 2.534 0 001 2v2a1 1 0 001 1h1a1 1 0 001-1v-1h8v1a1 1 0 001 1h1a1 1 0 001-1v-2a2.534 2.534 0 001-2V13c0-3.5-3.58-4-8-4s-8 .5-8 4zm4 1c-.83 0-2-.17-2-1a2.492 2.492 0 012-2c.83 0 1 1.17 1 2s-.17 1-1 1zm9 0c-.83 0-2-.17-2-1a2.492 2.492 0 012-2c.83 0 1 1.17 1 2s-.17 1-1 1zm1-6H14v-5h12z"
          fill="#fff"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
