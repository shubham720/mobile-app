import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button, FlatList } from 'react-native';
import {connect} from 'react-redux';
import {cityFrom} from '../redux/actions/cityFromAction';

function CityFromComponent({cityData,cityFrom}) {

  let data = [];

  useEffect(() => {
    cityFrom()
    console.log("making call")
  },[])

  if(cityData.cityFromList.error){
    console.log("here is error", cityData);
  } else if(cityData.cityFromList.loading){
    console.log("here is LOADING");
  } else if(cityData.cityFromList.cityList){
    for (var i = 0; i < 1000; i++) {
        var stringCount = i.toString();
      if(cityData.cityFromList.cityList[stringCount]){
        data.push(cityData.cityFromList.cityList[stringCount]);
      }
    }
  }

  return (
    <View style={styles.view}>
      <Text>Choose City here</Text>
      <FlatList
        data={data}
        renderItem = {({item}) => (
          <Text style={styles.textRecycler}>{item.id}</Text>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
      backgroundColor: 'white',
      padding: 10,
      height: '140%',
      width: '100%'
  },
  textRecycler:{
    backgroundColor: "green",
    color: "white",
    marginBottom: 2,
    padding: 5,
    borderRadius: 5,
    borderStyle: "solid"
  }
})

const mapStateToProps = state => {
  return {
      cityData: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    cityFrom: () => dispatch(cityFrom())
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (CityFromComponent)