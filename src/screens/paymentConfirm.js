import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Platform, TouchableOpacity } from 'react-native';
import OrangeButton from '../components/OrangeButton';
import {connect} from 'react-redux';
import { BLUE, ORANGE, WHITE } from '../utilities/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { HOME_SCREEN_NAME } from '../utilities/screenNames';
import { markBooking } from '../redux/actions/markBooked';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { USER_ROLE } from '../utilities/constants';


function index({ navigation, markBooking, route }) {

  useEffect(() => {
    initiate();
  },[])

  const initiate = async () => {
    navigation.setOptions({
      title: "Booking Successful"
    })
    var userRole = await AsyncStorage.getItem(USER_ROLE); 
    if(userRole != 6) {
      if(route.params){
        if(route.params.booking_id) {
          markBooking(route.params.booking_id);
        }
      }
    }
  }

  const makePayment = () => {
    navigation.reset({
      index: 0,
      routes: [{name: HOME_SCREEN_NAME}],
    });
  };
  
  return (
    <View style={styles.mainView}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <MaterialCommunityIcons name="check-circle-outline" color={ORANGE} size={100}/>
          <Text style={styles.paymentHeader}>Booking Successful</Text>
        </View>
        <View style={styles.bottomButtons}>
          <OrangeButton text='Continue' onPress={() => {makePayment()}} />
        </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
    height: '100%',
    width: '100%',
    padding: 20
  },
  paymentHeader: {
    padding: 5,
    color: BLUE,
    fontFamily: 'Montserrat-Bold'
  },
  bottomButtons: {
    justifyContent: 'space-between',
  },
});

const mapStateToProps = (state) => {
  return {
      // response: state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // paymentOffer: (data) => dispatch(paymentOffer(data)),
    // paymentRequest: (data) => dispatch(paymentRequest(data))
    markBooking: (booking_id) => dispatch(markBooking(booking_id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)