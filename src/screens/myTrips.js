import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, TextInput } from 'react-native';
import {connect} from 'react-redux';
import {bookings} from '../redux/actions/bookingsAction';
import LightBlueButton from '../components/LightBlueButton';
import Spinner from 'react-native-loading-spinner-overlay';
import { BLUE, DARKPURPLE, GREY, ORANGE, WHITE } from '../utilities/colors';
import { BOOKING_DETAILS_SCREEN_NAME, FILTER_BOOKINGS_SCREEN_NAME } from '../utilities/screenNames';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { BASE_URL, OPERATOR_ALL_ROUTES, USER_ROLE } from '../utilities/constants';
import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from 'moment';

function index({bookingsData,bookings, navigation, route}) {

  useEffect(() => {
    retrieveData()
  }, [])

  useEffect(() => {
    if (route.params) {
      const { fromId, fromText, toId, toText, bookingId, email, bookingDateFrom, bookingDateTo, travelDateFrom, travelDateTo } = route.params;
      var url = "&filter=";
      if(bookingId) {
        url+="id="+bookingId+",";
      }
      if(email) {
          url+="tickets.passengers.email_address="+email+",";
      }
      if (fromId) {
          url+="tickets.stopFrom.location.id="+fromId+",";
      }
      if (toId) {
          url+="tickets.stopTo.location.id="+toId+",";
      }
      if (travelDateFrom && !travelDateTo) {
          url+="tickets.date>="+travelDateFrom+",";
      }
      if (!travelDateFrom && travelDateTo) {
          url+="tickets.date<="+travelDateTo+",";
      }
      if (travelDateFrom && travelDateTo) {
          url+="tickets.inBetweenDate="+travelDateFrom+"D"+travelDateTo+",";
      }
      if (bookingDateFrom) {
          url+="created_at>="+bookingDateFrom+"%2017:00,";
      }
      if (bookingDateTo) {
          url+="created_at<="+bookingDateTo+"%2016:59,";
      }
      pagination = "lastId=0&pos=0&previous=0&limit=25";
      if(url.substr(url.length - 1) == ",") {
        url = url.slice(0, -1) + '';
      }
      if(route.params.refresh == 1) {
        console.log("refresh happn")
      }
      bookings(pagination + postBookingString+url);
    }
  },[route.params]);

  var pagination = "lastId=0&pos=0&previous=0&limit=25&";
  const [pageValue, setPageValue] = useState("0/0");
  const [postBookingString, setPostBookingString] = useState("");
  const [paginationEnable, setPaginationEnable] = useState(0);
  const [previous, setPrevious] = useState(0);
  const [current, setCurrent] = useState(0);
  const [count, setCount] = useState(25);
  const [next, setNext] = useState(0);
  const [data, setData] = useState([]);
  const [cityData, setCityData] = useState(null);
  const [searchText, setSearchText] = useState("");
  const [role,setRole] = useState(6);

  const retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem(USER_ROLE);
      setRole(value);
      if (value == 6) {
        setPostBookingString("filter=booking_status=booked|cancelled|blocked");
      }
      pagination = "lastId=0&pos=0&previous=0&limit=25";
      bookings(pagination + postBookingString);
      setCityData(JSON.parse(await AsyncStorage.getItem(OPERATOR_ALL_ROUTES)));
    } catch (error) {
      console.log(error);
    }
  };


  useEffect(() => {
    if (bookingsData.bookings.payload) {
      if (bookingsData.bookings.payload.data) {
        // console.log("data here", bookingsData.bookings.payload.data);
        setData(bookingsData.bookings.payload.data);
        setCurrent(bookingsData.bookings.payload.meta.cursor.current);
        setNext(bookingsData.bookings.payload.meta.cursor.next);
        setCount(bookingsData.bookings.payload.meta.cursor.count);
        setPrevious(bookingsData.bookings.payload.meta.cursor.prev);
        setPageValue((current + 1) + "/" + (current + count));
      }
    }
   },[bookingsData.bookings])


  if(bookingsData.bookings.error){
    console.log("here is error", bookingsData);
  }
    
  const openBookingDetails = (item) => {
    navigation.navigate(BOOKING_DETAILS_SCREEN_NAME,{
      booking_id: item.id
    });
  };

  const previousPage = () => {
    if (count == 25) {
      setPaginationEnable(1);
      pagination = "lastId=" + next + "&pos=" + (current - 25) + "&previous=" + (previous+25) + "&limit=25&";
      bookings(pagination);
    }
  }

  const nextPage = () => {
    if (count == 25) {
      setPaginationEnable(1);
      pagination = "lastId=" + next + "&pos=" + (current + 25) + "&previous=" + (previous - 25) + "&limit=25&";
      bookings(pagination);
    }
  }

  const searchBooking = () => {
    if(searchText.length > 0) {
      if (!isNaN(searchText)) {
        bookings(pagination+"filter=id="+searchText);
      } else {
        bookings(pagination+"filter=tickets.passengers.email_address="+searchText);
      }
    } else {
      setPaginationEnable(1);
      pagination = "lastId=0&pos=0&previous=0&limit=25";
      bookings(pagination);
    }
  }

  const filter = () => {
    navigation.navigate(FILTER_BOOKINGS_SCREEN_NAME);
  }

  return (
    <View style={styles.view}>
      <Spinner
          visible={bookingsData.bookings.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.viewList}>

        <View style={{ height: 60, padding: 10, flexDirection: 'row'}}>
          <TextInput 
            style={styles.inputView}
            value={searchText}
            placeholder="Booking id"
            onChangeText={(text) => {setSearchText(text)}}>
          </TextInput>
          <TouchableOpacity onPress={() => { searchBooking()}}>
            <View style={{flex: 1, borderRadius: 10, padding: 10, backgroundColor: ORANGE}}>
              <MaterialCommunityIcons name="magnify" color={WHITE} size={24}  />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {filter()}}>
            <View style={{flex: 1, borderRadius: 10, padding: 10, backgroundColor: ORANGE, marginStart: 10}}>
              <MaterialCommunityIcons name="filter" color={WHITE} size={24}  />
            </View>
          </TouchableOpacity>
        </View>
        
        <FlatList
          data={data}
          numColumns={1}
          renderItem = {({item}) => {
            if( role != 6 && (item.attributes.booking_status == 'booked' || item.attributes.booking_status == 'cancelled' )){
              return <TouchableOpacity onPress={() => openBookingDetails(item)}>
                <View style={styles.textRecycler}>
                  <Text style={styles.bookingText}>#{item.id} - {moment(item.attributes.created_at).format("DD-MMM-YY hh:mm")} - {item.attributes.booking_status}</Text>
                  {cityData &&
                    <View><Text>{cityData.result.details[item.relationships.locationFrom.data.id].name} - {cityData.result.details[item.relationships.locationTo.data.id].name}</Text></View>
                  }
                  {/*  */}
                </View>
              </TouchableOpacity>
            } else if(role == 6 && (item.attributes.booking_status == 'booked')){
             return <TouchableOpacity onPress={() => openBookingDetails(item)}>
                <View style={styles.textRecycler}>
                  <Text style={styles.bookingText}>#{item.id} - {moment(item.attributes.created_at).format("DD-MMM-YY hh:mm")} - {item.attributes.booking_status}</Text>
                  {cityData &&
                    <View><Text>{cityData.result.details[item.relationships.locationFrom.data.id].name} - {cityData.result.details[item.relationships.locationTo.data.id].name}</Text></View>
                  }
                  {/*  */}
                </View>
              </TouchableOpacity>
            }
          }
          }
        />

        <View style={{ flexDirection: 'row', height: 60, backgroundColor: WHITE, alignItems: 'center'}}>
          
          <TouchableOpacity style={{ flex: 1, alignItems: 'center' }} onPress={() => previousPage() }>
              <MaterialCommunityIcons name="keyboard-backspace" color={DARKPURPLE} size={24} style={{}} />
          </TouchableOpacity>
        
          <View style={{ flex: 1, alignItems: 'center', color: DARKPURPLE }}>
             <Text style={{color: DARKPURPLE }}>{ pageValue }</Text>
          </View>
          
          <TouchableOpacity style={{ flex: 1, alignItems: 'center' }} onPress={() => nextPage() }>
              <MaterialCommunityIcons name="arrow-right" color={DARKPURPLE} size={24} />
          </TouchableOpacity>
        
        </View>

      </View>
    </View>
  );

}

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'white',
    height: '100%',
    width: '100%',
  },
  viewList: {
    flex: 1,
    marginBottom: 0
  },
  textRecycler: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderBottomColor: 'black',
    borderColor: 'white',
    borderRadius: 0,
  },
  button: {
    marginTop: 20
  },
  bookingText: {
    fontWeight: '700',
    color: BLUE,
    fontSize: 16,
    paddingBottom: 20
  },
  inputView: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GREY,
    padding: 10,
    borderRadius: 10,
    marginStart: 10,
    marginEnd: 10,
    flex: 5
  },
})

const mapStateToProps = state => {
  return {
      bookingsData: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    bookings: (string) => dispatch(bookings(string))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)