import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image, Platform } from 'react-native';
import {connect} from 'react-redux';
import {searchBus} from '../redux/actions/searchBusAction';
import {BOARDING_ARRIVAL_SCREEN_NAME, SEAT_LAYOUT_SCREEN_NAME} from '../utilities/screenNames';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import { BACKGROUND, BLUE, GREY, ORANGE, WHITE } from '../utilities/colors';
import LocalizedStrings from '../utilities/localizationSettings';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

function index({response,cityTo, navigation, route, searchBus}) {

  const flatListRef = React.useRef();

  const {
    from_id,
    from_text,
    to_id,
    to_text,
    date,
    return_date,
    return_enabled
   } = route.params;

  var data = [];
  const [toId, setToId] = useState(0);
  const [toText,setToText] = useState("");
  const [dates, setDates] = useState([]);
  const [currentDate,setCurrentDate] = useState(date);
  const [isAndroid, setIsAndroid] = useState(false);
  const [toIndex,setToIndex] = useState(10);
  const [scrollToIn,setScrollToIn] = useState(0);

  useEffect(() => {
    searchBus(date,from_id,to_id);
  },[])

  useEffect(() => {
      navigation.setOptions({
        title: from_text+" - "+to_text
      })
  },[])

  useEffect(() => {
    var todayDate = moment(new Date());
    var dateChoosen = moment(date,"YYYY-MM-DD");
    var totalDates = dateChoosen.diff(todayDate, 'days');
    var startDate = null;
    if (totalDates >= 10) {
      totalDates = 10;
      startDate = moment(date,"YYYY-MM-DD").subtract(10, "days");
    } else {
      startDate = moment(date,"YYYY-MM-DD").subtract(totalDates, "days");
    }
    var mainDates = totalDates + 10;
    var scrollTO = 0;
    for (var i=0; i <= mainDates; i++) {
      var obj = {};
      obj.date = moment(startDate, "YYYY-MM-DD").add(i, 'days');
      if (moment(startDate, "YYYY-MM-DD").add(i, 'days').format("YYYY-MM-DD") == moment(dateChoosen).format("YYYY-MM-DD")) {
        obj.isSelected = true;
        scrollFlatList(i);
      } else {
        obj.isSelected = false;
      }
      obj.index = i;
      dates.push(obj);
    }
  },[]);

  const scrollFlatList = (index) => {
    setTimeout(() => {
      flatListRef.current.scrollToIndex({ animated: false, offset: 2, index: index })
    },2000);
  }

  if (response.searchResults.error) {
    console.log("here is error", response.searchResults.error);
  } else if (response.searchResults.searchResults) {
      data = response.searchResults.searchResults.result;
  }

  const openBoardingArrival = (itemCurrent) => {
    var stop_to = null,stop_from = null;
    for (const item of itemCurrent.stops) {
      if(item.disembarking_allowed && item.disembarking_option) {
        stop_to = item.name+" - "+item.address;
      }
      if(item.boarding_allowed && item.boarding_option){
        stop_from = item.name+" - "+item.address;
      }
    }
    if(stop_to && stop_from){
      navigation.navigate(BOARDING_ARRIVAL_SCREEN_NAME, {
        from_address: stop_from,
        to_address: stop_to,
        header: from_text+" - "+to_text,
        itemCurrent: itemCurrent,
        from_id: from_id,
        from_text: from_text,
        to_id: to_id,
        to_text: to_text,
        date: date,
        return_date: return_date,
        return_enabled: return_enabled
      })
    }
  }

  const citySelected = (itemCurrent) => {
    var stop_to = null,stop_from = null;
    for (const item of itemCurrent.stops) {
      if(item.disembarking_allowed && item.disembarking_option) {
        stop_to = item.id;
      }
      if(item.boarding_allowed && item.boarding_option){
        stop_from = item.id;
      }
    }
    if(stop_to && stop_from) {
      navigation.navigate(SEAT_LAYOUT_SCREEN_NAME, {
        date: currentDate,
        route_schedule_id: itemCurrent.id,
        stop_from: stop_from,
        stop_to: stop_to,
        from_id: from_id,
        from_text: from_text,
        to_id: to_id,
        to_text: to_text,
        vehicle: itemCurrent.vehicle.subtype,
        departure_date: itemCurrent.departure_datetime,
        departure_time: itemCurrent.departure_datetime,
        price_min: itemCurrent.seat_price_min,
        price_max: itemCurrent.seat_price_max,
        return_date: return_date,
        return_enabled: return_enabled
      })
    }
  };

  const getNewData = (date) => {
    searchBus(date,from_id,to_id);
  }

  const changeSelcted = (item) => {
    for (const itemOne of dates) {
      if(itemOne.index == item.index) {
        itemOne.isSelected = true;
      } else {
        itemOne.isSelected = false;
      }
    }
  };

  

  const renderItemDate = (item) => {
    if (item.isSelected) {
      return <TouchableOpacity onPress = {() => {
        getNewData(moment(item.date).format("yyyy-MM-DD"));
        setCurrentDate(moment(item.date).format("yyyy-MM-DD"));
        changeSelcted(item);
        }}>
      <View style={styles.datesCardSelected}>
        <Text style={styles.dateTextSelected}>{moment(item.date).format("DD MMMM")}</Text>
      </View>
    </TouchableOpacity>;
    } else {
      return <TouchableOpacity onPress = {() => {
        getNewData(moment(item.date).format("yyyy-MM-DD"));
        setCurrentDate(moment(item.date).format("yyyy-MM-DD"));
        changeSelcted(item);
        }}>
      <View style={styles.datesCard}>
        <Text style={styles.dateText}>{moment(item.date).format("DD MMMM")}</Text>
      </View>
    </TouchableOpacity>;
    }
  }

  return (
    <View style={styles.view}>

      { isAndroid &&
        <View style={{
          backgroundColor: BLUE,
          flexDirection: 'row',
          paddingTop: 5,
          paddingStart: 5,
          paddingBottom: 5,
          alignItems: 'center'
        }}>
            <TouchableOpacity onPress={navigation.goBack}>
              <View style={{ width: 40 }}>
                <MaterialCommunityIcons name="chevron-left" color={WHITE} size={40} />
              </View>
            </TouchableOpacity>
            <View style={{flex: 1}}>
              <Text style={{
                color: WHITE, 
                fontFamily: 'Montserrat-Bold',
                alignSelf: 'center',
                fontSize: 15}}>
                {from_text+" - "+to_text}
              </Text>
            </View>
            <View style={{ width: 40 }}></View>
        </View>
      }

      <Spinner
          visible={response.searchResults.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.viewList}>
        
        <View>
          {dates &&
            <FlatList
              data={dates}
              ref={flatListRef}
              horizontal
              scrollToIndex={toIndex}
              keyExtractor={(item) => item.date}
              renderItem = {({item}) => (renderItemDate(item))}
              />
          }
        </View>
      
        {data && data.length > 0 && 
          <FlatList
          showsHorizontalScrollIndicator={false}
          data={data}
          renderItem = {({item}) => (
            <TouchableOpacity onPress={() => citySelected(item)}>
              <View style={styles.textRecycler}>

                <View style={styles.textRow}>
                  <View style={styles.leftView}>
                    <Text style={styles.textSmallGrey}>Departure</Text>
                  </View>
                  <View style={styles.rightView}>
                    <Text style={styles.textSmallGrey}>Arrival</Text>
                  </View>
                </View>

                <View style={styles.textRow}>
                  <View style={styles.firstOfThree}>
                    <Text style={styles.blueTextTime}>{moment(item.departure_datetime).format('HH:mm')}</Text>
                  </View>
                  <View style={styles.secondOfThree}>
                    <Text style={styles.textSmallGrey}>{item.duration}</Text>
                  </View>
                  <View style={styles.thirdOfThree}>
                    <Text style={styles.blueTextTime}>{moment(item.arrival_datetime).format('HH:mm')}</Text>
                  </View>
                </View>

                <View style={styles.textRowLower}>
                  <TouchableOpacity onPress={() => openBoardingArrival(item)}>
                    <View style={styles.leftView}>
                      <Text style={styles.orangeTextTime}>Boarding Arrival</Text>
                    </View>
                  </TouchableOpacity>
                  <View style={styles.rightView}>
                  </View>
                </View>

                <View style={styles.textRowLower}>
                  <View style={styles.leftGreyBack}>
                    <View style={{ padding: 8, borderRadius: 8, backgroundColor: BLUE}}>
                      <MaterialCommunityIcons name="bus" color={WHITE} size={24} />
                    </View>
                    <Text style={styles.blueVehicleType}>{item.vehicle.subtype}</Text>
                  </View>
                  <View style={styles.rightOrangeBack}>
                    <Text style={styles.whitePriceText}>
                      {item.currency}{" "+(item.seat_price_min).toFixed(2)}
                      {" - "+(item.seat_price_max).toFixed(2)}
                    </Text>
                  </View>
                </View>

              </View>
              
            </TouchableOpacity>
          )}
        />
      }
      { !data || data.length == 0 && 
        <Text>
          No result found
        </Text>
      }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
      backgroundColor: '#C9CEE7',
      height: '100%',
      width: '100%'
  },
  viewList: {
    paddingTop: 20,
    paddingStart: 20,
    paddingEnd: 20,
    marginBottom: 10,
    flex: 1,
    flexDirection: 'column'
  },
  textRecycler: {
    padding: 10,
    marginBottom: 15,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 10,
    borderColor: 'white',
    flexDirection: 'column',
    flex: 1
  },
  button: {
    marginTop: 20
  },
  textSmallGrey: {
    fontSize: 12,
    color: 'grey',
    fontFamily: 'Montserrat-Medium'
  },
  textRow: {
    flexDirection: 'row',
    flex: 1
  },
  rightView: {
    alignItems: 'flex-end',
    flex: 1
  },
  leftView: {
    alignItems: 'flex-start',
    flex: 1,
  },
  firstOfThree: {
    alignItems: 'flex-start',
    flex: 1
  },
  secondOfThree: {
    alignItems: 'center',
    flex: 1
  },
  thirdOfThree: {
    alignItems: 'flex-end',
    flex: 1
  },
  blueTextTime: {
    fontSize: 18,
    fontWeight: '700',
    color: BLUE
  },
  orangeTextTime: {
    fontSize: 12,
    color: ORANGE,
    fontFamily: 'Montserrat-Bold'
  },
  textRowLower: {
    flexDirection: 'row',
    flex: 1,
    marginTop: 15
  },
  leftGreyBack: {
    backgroundColor: GREY,
    flex:4,
    flexDirection: 'row',
    padding: 10,
    borderTopStartRadius: 10,
    borderBottomStartRadius: 10,
    alignItems: 'center',
  },
  rightOrangeBack: {
    backgroundColor: ORANGE,
    flex:5,
    padding: 10,
    borderTopStartRadius: 10,
    borderBottomStartRadius: 10,
    marginStart: -11,
    marginEnd: -11,
    justifyContent: 'center',
  },
  blueVehicleType: {
    fontSize: 16,
    color: BLUE,
    paddingStart: 5,
    fontFamily: 'Montserrat-Bold'
  },
  whitePriceText: {
    fontSize: 16,
    color: WHITE,
    paddingStart: 5,
    fontFamily: 'Montserrat-Bold'
  },
  datesCard: {
    padding: 10,
    borderWidth: 1,
    backgroundColor: BLUE,
    borderRadius: 10,
    borderColor: BLUE,
    flexDirection: 'column',
    color: WHITE,
    marginTop: 5,
    marginEnd: 5,
    marginStart: 5,
    marginBottom: 20
  },
  dateText: {
    color: WHITE,
    fontFamily: 'Montserrat-Medium'
  },
  datesCardSelected: {
    padding: 10,
    borderWidth: 1,
    backgroundColor: WHITE,
    borderRadius: 10,
    borderColor: WHITE,
    flexDirection: 'column',
    color: BLUE,
    marginTop: 5,
    marginEnd: 5,
    marginStart: 5,
    marginBottom: 20
  },
  dateTextSelected: {
    color: BLUE,
    fontFamily: 'Montserrat-Medium'
  }
})

const mapStateToProps = state => {
  return {
    response: state
  }
}
const mapDispatchToProps = dispatch =>{
  return {
    searchBus: (date,from_id,to_id) => dispatch(searchBus(date,from_id,to_id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)