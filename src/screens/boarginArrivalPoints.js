import React,{useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { BLUE, WHITE, GREY, ORANGE, DARKGREY} from '../utilities/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SEAT_LAYOUT_SCREEN_NAME } from '../utilities/screenNames';
import OrangeButton from '../components/OrangeButton';

export default function index({navigation, route}) {

  const {
    from_address,
    to_address,
    header,
    itemCurrent,
    from_id,
    from_text,
    to_id,
    to_text,
    date,
    return_date,
    return_enabled
   } = route.params;

  const citySelected = () => {
    var stop_to = null,stop_from = null;
    for (const item of itemCurrent.stops) {
      if(item.disembarking_allowed && item.disembarking_option) {
        stop_to = item.id;
      }
      if(item.boarding_allowed && item.boarding_option){
        stop_from = item.id;
      }
    }
    if(stop_to && stop_from){
      navigation.navigate(SEAT_LAYOUT_SCREEN_NAME, {
        date: date,
        route_schedule_id: itemCurrent.id,
        stop_from: stop_from,
        stop_to: stop_to,
        from_id: from_id,
        from_text: from_text,
        to_id: to_id,
        to_text: to_text,
        vehicle: itemCurrent.vehicle.subtype,
        departure_date: itemCurrent.departure_datetime,
        departure_time: itemCurrent.departure_datetime,
        price_min: itemCurrent.seat_price_min,
        price_max: itemCurrent.seat_price_max,
        return_date: return_date,
        return_enabled: return_enabled
      })
    }
  };

  return (
    <View style={styles.mainView}>
      <View style={styles.innerView}>
        <View style={styles.paymentView}>
          <View style={styles.paymentText}>
            <Text style={styles.paymentHeader}>Boarding</Text>
          </View>
          <View style={styles.paymentSelecter}>
            <MaterialCommunityIcons color={BLUE} name="checkbox-blank-circle" size={10}/>
            <Text style={{paddingStart: 10, fontWeight: 'bold', color: BLUE}}>{from_address}</Text>
          </View>
        </View>
        <View style={styles.paymentView}>
          <View style={styles.paymentText}>
            <Text style={styles.paymentHeader}>Arrival</Text>
          </View>
          <View style={styles.paymentSelecter}>
            <MaterialCommunityIcons color={ORANGE} name="checkbox-blank-circle" size={10}/>
            <Text style={{paddingStart: 10, fontWeight: 'bold', color: BLUE}}>{to_address}</Text>
          </View>
        </View>
        </View>
      <OrangeButton text='Next' onPress={() => {citySelected()}} />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: WHITE,
    height: '100%',
    width: '100%',
    flex: 1,
    paddingBottom: 20,
    paddingEnd: 20,
    paddingStart: 20
  },
  innerView: {
    flex: 1
  },
  paymentView: {
    marginTop: 30,
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GREY,
    padding: 20,
    borderRadius: 10,
  },
  paymentText: {
    flexDirection: 'row'
  },
  paymentHeader: {
    marginTop: -35,
    backgroundColor: WHITE,
    padding: 5,
    color: DARKGREY
  },
  paymentSelecter: {
    flexDirection: 'row'
  },
});
