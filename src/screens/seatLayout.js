import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Alert } from 'react-native';
import {connect} from 'react-redux';
import { seatRequest, seatReset } from '../redux/actions/seatAction';
import OrangeButton from '../components/OrangeButton';
import Spinner from 'react-native-loading-spinner-overlay';
import {BACKGROUND,WHITE,GREY, BLUE, ORANGE} from '../utilities/colors';
import {PASSENGER_DETAILS_SCREEN_NAME, PAYMENT_CONFIRM_NAME, RETURN_SEARCH_RESULT_NAME} from '../utilities/screenNames';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {customerBooking} from '../redux/actions/customerBookingAction';
import { paymentRequest } from '../redux/actions/paymentRequestAction';
import { markBooking } from '../redux/actions/markBooked';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { USER_ROLE } from '../utilities/constants';
import Radio from '../components/Radio';
import { USER_FIRST_NAME, USER_LAST_NAME, USER_EMAIL, USER_PHONE } from '../utilities/constants';
import { fill } from 'lodash';

function index({response, navigation, route, seatRequest, customerBooking, paymentRequest, seatReset}) {
  
  const {
    date,
    route_schedule_id,
    stop_from,
    stop_to,
    from_id,
    from_text,
    to_id,
    to_text,
    vehicle,
    departure_date,
    departure_time,
    price_min,
    price_max,
    return_date,
    return_enabled
  } = route.params;

  const [data,setData] = useState([]);
  const [columns,setColumns] = useState(null);
  const [change,setChange] = useState(0);
  const [seatNumbers, setSeatNumbers] = useState([]);
  const [loading,setLoading] = useState(false);
  const [called,setCalled] = useState(false);
  const [isPaymentInitialized, setIsPaymentInitialized] = useState(false);
  const [rewardPointsPayment, setRewardPointsPayment] = useState(0);
  const [isCalledConfirm, setIsCalledConfirm] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email,setEmail] = useState("");
  const [phone,setPhone] = useState("");
  const [nationalityCount, setNationalityCount] = useState(2);
  const [nationality,setNationality] = useState("Non-Cambodian");
  const [fillPassenger, setFillPassenger] = useState(0);
  const [isAdmin, setIsAdmin] = useState(false);

  const passengerDetails = [
    {text: "Fill passenger details?",id: 1}
  ];

  useEffect(() => {
      seatReset();
      seatRequest(date,route_schedule_id,stop_from,stop_to);
      setLoading(true);
      navigation.setOptions({
        title: "Select Seats"
      });
      retrieveData();
  },[])

  const retrieveData = async () => {
    try {
      const UserFirstName = await AsyncStorage.getItem(USER_FIRST_NAME);
      if (UserFirstName !== null) {
        setFirstName(UserFirstName);
      }
      const UserLastName = await AsyncStorage.getItem(USER_LAST_NAME);
      if (UserLastName !== null) {
        setLastName(UserLastName);
      }
      const UserEmail = await AsyncStorage.getItem(USER_EMAIL);
      if (UserEmail !== null) {
        setEmail(UserEmail);
      }
      const UserPhone = await AsyncStorage.getItem(USER_PHONE);
      if (UserPhone !== null) {
        setPhone(UserPhone);
      }
      const userRole = await AsyncStorage.getItem(USER_ROLE);
      if (userRole != 6) {
        setIsAdmin(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    var newData = [];
    if (response.seatReducer.success) {
      newData = response.seatReducer.payload.result[0].seats;
      if(newData){
        if(newData.length > 0) {
          var i=0, prevColumn = 0;
          for (const item of newData) {
            if(newData[i]){
              newData[i].isSelected = false;
              newData[i].indexMain = i;
              if(prevColumn < item.column) {
                prevColumn = item.column;
              } else {
                setColumns(prevColumn);
              }
            }
            i++;
          }
          setLoading(false);
        }
      }
      setData(newData);
      seatReset();
    }
  },[response.seatReducer]);

  const selectSeat = (itemData) => {
    var newData = data;
    itemData.isSelected = !itemData.isSelected;
    newData[itemData.indexMain] = itemData;
    const index = seatNumbers.indexOf(itemData.seat_number);
    if (index > -1) {
      seatNumbers.splice(index, 1);
    } else {
      seatNumbers.push(itemData.seat_number);
    }
    setData(newData);
    setChange(change+1);
  };

  var price = 0, return_price = 0, total_price = 0;
  if (return_enabled) {
      if (nationality === "Cambodian") {
          price = price_min;
          return_price = return_price_min;
      } else if(nationality === "Non-Cambodian") {
          price = price_max;
          return_price = return_price_max;
      }
      total_price = price*(seatNumbers.length) + return_price*(seatNumbers.length);
  } else {
      if (nationality === "Cambodian") {
          price = price_min;
      } else if(nationality === "Non-Cambodian") {
          price = price_max;
      }
      total_price = price*(seatNumbers.length);
  }

  const openPassengerDetails = () => {
    if(seatNumbers.length > 0) {
      if(return_enabled == 1) {
        navigation.navigate(RETURN_SEARCH_RESULT_NAME,{
          date: date,
          route_schedule_id: route_schedule_id,
          seats: seatNumbers,
          from_id: from_id,
          from_text: from_text,
          to_id: to_id,
          to_text: to_text,
          stop_to_id: stop_to,
          stop_from_id: stop_from,
          vehicle: vehicle,
          departure_date: departure_date,
          departure_time: departure_time,
          price_min: price_min,
          price_max: price_max,
          return_enabled: return_enabled,
          return_date: return_date
        });
      } else {

        if(!isAdmin) {
          navigation.navigate(PASSENGER_DETAILS_SCREEN_NAME,{
            date: date,
            route_schedule_id: route_schedule_id,
            seats: seatNumbers,
            from_id: from_id,
            from_text: from_text,
            to_id: to_id,
            to_text: to_text,
            stop_to_id: stop_to,
            stop_from_id: stop_from,
            vehicle: vehicle,
            departure_date: departure_date,
            departure_time: departure_time,
            price_min: price_min,
            price_max: price_max,
            return_enabled: return_enabled,
            return_date: return_date
          });
        } else if (isAdmin) {
          if(fillPassenger === 1){
            navigation.navigate(PASSENGER_DETAILS_SCREEN_NAME,{
              date: date,
              route_schedule_id: route_schedule_id,
              seats: seatNumbers,
              from_id: from_id,
              from_text: from_text,
              to_id: to_id,
              to_text: to_text,
              stop_to_id: stop_to,
              stop_from_id: stop_from,
              vehicle: vehicle,
              departure_date: departure_date,
              departure_time: departure_time,
              price_min: price_min,
              price_max: price_max,
              return_enabled: return_enabled,
              return_date: return_date
            });
          } else {
            setLoading(true);
          var bookingObj = {};
          bookingObj.action = "tentative";
          bookingObj.connecting = "0";
          bookingObj.request_from = "mobile_app";
          var seatArray = [], ticket_requests = {}, ticketRequestArray = [];
          var i = 0;
          for (const iterator of seatNumbers) {
              var seatObj = {};
              if (i == 0) {
                  seatObj.is_primary = true;
              } else {
                  seatObj.is_primary = false;
              }
              seatObj.min_price = price_min;
              seatObj.number = iterator;
              seatObj.passenger_id = i;
              seatObj.price = price_max;
              seatArray.push(seatObj);
              i++;
          }
          ticket_requests.address = " ";
          ticket_requests.contact_phone = "+855 "+phone;
          ticket_requests.date = date;
          ticket_requests.pickup_time = " ";
          ticket_requests.route_schedule_id = route_schedule_id;
          ticket_requests.stop_id_from = stop_from;
          ticket_requests.stop_id_to = stop_to;
          ticket_requests.taxi_drop_off = " ";
          ticket_requests.seat_requests = seatArray;
          ticketRequestArray.push(ticket_requests);
          var passenger_requests_data = [];
          var countI = 0
          for (const iterator of seatNumbers) {
              var passenger_requests = {};
              passenger_requests.id = countI;
              passenger_requests.agency_id ="0";
              passenger_requests.block_phone = "0";
              passenger_requests.block_type = "Fixed";
              passenger_requests.bookingType = "other";
              passenger_requests.booking_remarks = "";
              passenger_requests.title = "Mr.";
              passenger_requests.first_name = firstName;
              passenger_requests.last_name = lastName;
              passenger_requests.name = firstName+" "+lastName;
              passenger_requests.gender = "male";
              passenger_requests.email_address = email;
              passenger_requests.mobile_number = "+855 " + phone;
              passenger_requests.nationality = nationality;
              passenger_requests_data.push(passenger_requests);
              countI++;
          }
          bookingObj.passenger_requests = passenger_requests_data;
          bookingObj.ticket_requests = ticketRequestArray;
          customerBooking(bookingObj);
          setCalled(true);
          }
        }
      }
    } else {
      Alert.alert(
        "",
        "Please select at least one seat",
        [
            {
                text: "Ok",
                style: "cancel",
            }
        ],
        {
            cancelable: true
        }
      );
    }
  }

  useEffect(() => {
    if (called) {
      if(response.customerBooking.success){
        makePayment(response.customerBooking.payload.result[0].booking_id);
      }
    }
},[response.customerBooking]);

const makePayment = (booking_id) => {
  setLoading(true);
  setIsPaymentInitialized(true);
  var paymentObj = {};
  paymentObj.address = "";
  paymentObj.contact_phone=phone;
  paymentObj.appCod = 0;
  paymentObj.booking_id = booking_id;
  paymentObj.coupon_code = "";
  paymentObj.currency = "USD";
  paymentObj.donation_amount = 0;
  paymentObj.experiences = [];
  paymentObj.insurance = 0;
  paymentObj.insuranceAmount  = 0;
  paymentObj.insuranceType = 0;
  paymentObj.payment_method = "paymanual";
  paymentObj.reward_points = rewardPointsPayment;
  paymentObj.reward_points_type = "user";
  paymentObj.total_amount = total_price;
  paymentObj.affiliate_id = null;
  paymentRequest(paymentObj);
}

useEffect(() => {
  if(isPaymentInitialized) {
      if(response.paymentRequest.payload) {
          if(response.paymentRequest.payload.status == "success") {
            // markPayment(response.paymentRequest.payload.result[0].booking_id);
            setLoading(false);
            setIsCalledConfirm(false);
            navigation.navigate(PAYMENT_CONFIRM_NAME,{
                  booking_id: response.paymentRequest.payload.result[0].booking_id
            });
          }
      }
  }
},[response.paymentRequest]);

  const renderItem = (item) => {
    if (item.bookable && item.available && !item.isSelected) {
      return <TouchableOpacity onPress={() => selectSeat(item)}>
              <View style={styles.viewAvailable}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>
            </TouchableOpacity>;
    } else if (item.bookable && item.available && item.isSelected) {
      return <TouchableOpacity onPress={() => selectSeat(item)}>
              <View style={styles.viewSelected}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>
            </TouchableOpacity>;
    } else if (!item.bookable && !item.available && item.seat_number.length > 0) {
      if(item.seat_number.toLowerCase().trim() == "driver"){
        return <View style={styles.viewHidden}>
          <Text style={styles.textHidden}>{item.seat_number}</Text>
        </View>;
      } else {
        return  <View style={styles.viewOccupied}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>;
      }
    } else {
      return <View style={styles.viewHidden}>
        <Text style={styles.textHidden}>{item.seat_number}</Text>
      </View>;
    }
  }

  const radioSelected = (count,text) => {
    if(fillPassenger === 0){
      setFillPassenger(1);
    } else {
      setFillPassenger(0);
    }
  }

  return (
    <View style={styles.view}>

      <Spinner
          visible={loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />

      <View
       style={{
        height: '100%',
        width: '100%',
      }}>
          <View style={styles.infoTabRow}>
            <View style={styles.infoTab}>
              <MaterialCommunityIcons name="checkbox-blank-circle" color={WHITE} size={20} />
              <Text style={styles.textInfo}>Available</Text>
            </View>
            <View style={styles.infoTab}>
              <MaterialCommunityIcons name="checkbox-blank-circle" color={ORANGE} size={20} />
              <Text style={styles.textInfo}>Selected</Text>
            </View>
            <View style={styles.infoTab}>
              <MaterialCommunityIcons name="checkbox-blank-circle" color={BLUE} size={20} />
              <Text style={styles.textInfo}>Occupied</Text>
            </View>
          </View>

            <View style={styles.viewList}>
              { data && columns > 0 &&
                <FlatList
                  data={data}
                  numColumns={columns}
                  keyExtractor={(item) => item.indexMain}
                  extraData={change}
                  renderItem = {({item}) => renderItem(item)}
                />
              }
            </View>
            
        <View style={{
          padding: 20
        }}>
          { isAdmin && 
            <Radio
              data={passengerDetails}
              value={fillPassenger}
              columns={1}
              onPress={(count,text) => radioSelected(count,text)}
            >
            </Radio>
          }
          

          <OrangeButton text='Continue' onPress={() => openPassengerDetails()} style={styles.button}></OrangeButton>
        </View>
      </View>
    </View>
  );

}

const styles = StyleSheet.create({
  view: {
    backgroundColor: BACKGROUND,
    height: '100%',
    width: '100%'
  },
  viewList: {
    flex: 1,
    alignItems: 'center'
  },
  bottomButtons: {
    padding: 20,
    justifyContent: 'space-between',
  },
  viewAvailable: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: WHITE,
    backgroundColor: WHITE,
    color: GREY,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  textAvailable: {
    color: GREY
  },
  viewSelected: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: ORANGE,
    backgroundColor: ORANGE,
    color: WHITE,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  viewHidden: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: BACKGROUND,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  viewOccupied: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: BLUE,
    backgroundColor: BLUE,
    color: WHITE,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  textHidden: {
    color: BACKGROUND
  },
  textSelected: {
    color: WHITE
  },
  infoTabRow: {
    flexDirection: 'row',
    padding: 5
  },
  infoTab: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center'
  },
  textInfo: {
    padding: 5,
    color: BLUE,
    fontFamily: 'Montserrat-Bold'
  },
  button:{
    marginBottom: 20
  },
})

const mapStateToProps = state => {
  return {
      response: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    seatRequest: (date,route_schedule_id,stop_from,stop_to) => dispatch(seatRequest(date,route_schedule_id,stop_from,stop_to)),
    customerBooking: (data) => dispatch(customerBooking(data)),
    paymentRequest: (data) => dispatch(paymentRequest(data)),
    markBooking: (data) => dispatch(markBooking(data)),
    seatReset: () => dispatch(seatReset())
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (index)