import React, { useEffect, useState } from 'react';
import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View, Alert } from 'react-native';
import OrangeButton from '../components/OrangeButton';
import SearchFromTo from '../components/SearchFromTo';
import DatesField from '../components/DatesField';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';
import {connect} from 'react-redux';
import { CITY_FROM_SCREEN_NAME, CITY_TO_SCREEN_NAME, SEARCH_RESULTS_SCREEN_NAME } from '../utilities/screenNames';
import { BLUE, GREY, WHITE } from '../utilities/colors';
import LocalizedStrings from '../utilities/localizationSettings';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getStops } from '../redux/actions/allOperatorStops';
import { cityFrom } from '../redux/actions/cityFromAction';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { USER_ROLE } from '../utilities/constants';

function index({navigation, route, getStops}) {
    
    React.useEffect(() => {
        console.log(route.params);
        if (route.params?.fromIdSelected && route.params?.fromTextSelected) {
            const { fromIdSelected, fromTextSelected } = route.params;
            setFromId(fromIdSelected);
            setFromText(fromTextSelected);
        }
        if (route.params?.toIdSelected && route.params?.toTextSelected) {
            const { toIdSelected, toTextSelected } = route.params;
            setToId(toIdSelected);
            setToText(toTextSelected);
        }
    },[route.params]);

    useEffect(() => {
        retrieveData();
        getStops();
    },[])

    const retrieveData = async () => {
        try {
          const userRole = await AsyncStorage.getItem(USER_ROLE);
          if (userRole != 6) {
            setIsOpUser(true);
          }
        } catch (error) {
          console.log(error);
        }
    };

    const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
    const [isDateTimePickerReturnVisible, setIsDateTimePickerReturnVisible] = useState(false);
    const [fromId,setFromId] = useState(123);
    const [fromText,setFromText] = useState("Sihanoukville");
    const [toId,setToId] = useState(159);
    const [toText,setToText] = useState("Koh Rong");
    const [date, setDate] = useState(moment().format("yyyy-MM-DD"));
    const [returnDate, setReturnDate] = useState("Select date");
    const [returnEnabled, setReturnEnabled] = useState(0);
    const [isOpUser,setIsOpUser] = useState(false);

    const clearReturnDate = () => {
        setReturnDate("Select date");
        setReturnEnabled(0);
    }

    const showDateTimePicker = (value) => {
        if(value == 1) {
            setIsDateTimePickerVisible(true);
        } else if(value == 2) {
            setIsDateTimePickerReturnVisible(true);
        }
    };
    const  hideDateTimePicker = (value) => {
        if(value == 1) {
            setIsDateTimePickerVisible(false);
        } else if(value == 2) {
            setIsDateTimePickerReturnVisible(false);
        }
    };
    const handleDatePicked = date => {
        hideDateTimePicker(1);
        setDate(moment(date).format("yyyy-MM-DD"));
    };
    const handleDateReturnPicked = date => {
        hideDateTimePicker(2);
        setReturnDate(moment(date).format("yyyy-MM-DD"));
        setReturnEnabled(1);
    };

    const openFromCity = () =>
    (
        navigation.navigate(CITY_FROM_SCREEN_NAME,{page: 1})
    );
    const openToCity = () =>
    {
        if(fromId && fromText){
            navigation.navigate(CITY_TO_SCREEN_NAME,{
                from_id: fromId,
                page: 1
            })
        }
    };

    const moveToSearch = () => {
        if(fromId && fromText && toId && toText && date) {
            navigation.navigate(SEARCH_RESULTS_SCREEN_NAME,{
                from_id: fromId,
                from_text: fromText,
                to_id: toId,
                to_text: toText,
                date: date,
                return_date: returnDate,
                return_enabled: returnEnabled
            })
        } else {
            Alert.alert(
                "Message",
                "Please select Departure, arrival and departure date at least",
                [
                    {
                        text: "Ok",
                        style: "cancel",
                    }
                ],
                {
                    cancelable: true
                }
            );
        }
    };

    const changeFromTo = () => {
        if(fromId && fromText && toId && toText) {
            var fromIdChange = fromId;
            var fromTextChange = fromText;
            setFromId(toId);
            setFromText(toText);
            setToId(fromIdChange);
            setToText(fromTextChange);
        } else {
            Alert.alert(
                "Message",
                "Please select Departure and Arrival",
                [
                    {
                        text: "Ok",
                        style: "cancel",
                    }
                ],
                {
                    cancelable: true
                }
            );
        }
    }

    return (
        <View style={styles.view}>
            <ImageBackground source={require('../images/png/home_image.png')} style={styles.backgroundImage}>
            <View style={styles.viewInner}>
                <View style={{flexDirection: 'row', marginTop: 50}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.textBig}>Buvea Sea</Text>
                        <Text style={styles.textWhiteSmall}>Buvea Sea Cambodia</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'flex-end' }}> 
                        <Image source={require('../images/png/bv_logo.png')} 
                        style={{width: 100,height: 100,resizeMode: 'contain'}} />
                    </View>
                </View>

                <View style={styles.text}>
                    <SearchFromTo
                        label={LocalizedStrings.from}
                        textMain={fromText}
                        type={1}
                        onPress={() => {
                            openFromCity()
                        }}>
                    </SearchFromTo>
                </View>

                <View style={{marginTop: -20, marginBottom: -45, zIndex: 1, width: 40, height: 40, alignSelf: 'flex-end'}}>
                    <TouchableOpacity onPress={() => {
                            changeFromTo()
                        }}>
                            <View style={{backgroundColor: BLUE, borderRadius: 7, padding: 5, alignItems: 'center'}}>
                                <MaterialCommunityIcons name="swap-vertical" color={WHITE}  size={26}/>
                            </View>
                    </TouchableOpacity>
                </View>
            
                <View style={{marginBottom: 10, marginTop: 25}}>
                    <SearchFromTo
                        label={LocalizedStrings.to}
                        textMain={toText}
                        type={2}
                        onPress={() => {
                            openToCity()
                        }}>
                    </SearchFromTo>
                </View>
                

                <View style={styles.dateRow}>
                    <View style={{flex: 1, marginEnd: 5}}>
                        <DatesField
                            style={styles.text}
                            label={LocalizedStrings.departure}
                            textMain={date}
                            onPress={() => {showDateTimePicker(1)}}>
                        </DatesField>
                    </View>
                    { !isOpUser && 
                    <View style={{flex: 1, marginStart: 5, borderStartWidth: 1, borderColor: GREY}}>
                        <DatesField
                            style={styles.text}
                            label={LocalizedStrings.return}
                            textMain={returnDate}
                            onPress={() => {showDateTimePicker(2)}}>
                        </DatesField>
                    </View>
                    }
                    
                </View>

                { !isOpUser && 
                <View style={{marginTop: -60, marginBottom: 5, marginEnd: 5, zIndex: 1, width: 20, alignSelf: 'flex-end'}}>
                    <TouchableOpacity onPress={() => {
                            clearReturnDate()
                        }}>
                            <View style={{backgroundColor: BLUE, borderRadius: 10, padding: 5, alignItems: 'center'}}>
                                <MaterialCommunityIcons name="close" color={WHITE}  size={10}/>
                            </View>
                    </TouchableOpacity>
                </View>
                }
                
                <View style={{marginTop: 50}}>
                    <OrangeButton text={LocalizedStrings.search} onPress={() => {moveToSearch()}} ></OrangeButton>
                </View>
                
                <DateTimePicker
                    isVisible={isDateTimePickerVisible}
                    onConfirm={(date) => {handleDatePicked(date)}}
                    onCancel={() => hideDateTimePicker(1)}
                />
                <DateTimePicker
                    isVisible={isDateTimePickerReturnVisible}
                    onConfirm={(date) => {handleDateReturnPicked(date)}}
                    onCancel={() => hideDateTimePicker(2)}
                />
            </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
    },
    viewInner: {
        flex: 1,
        marginTop: 40,
        padding: 20
    },
    text: {
        marginBottom: 10
    },
    bottomSheet: {
        flex: 1,
        width: '100%'
    },
    textBig: {
        color: WHITE,
        fontWeight: '700',
        fontSize: 25,
        marginTop: 20,
        fontFamily: 'Montserrat-Bold'
    },
    textWhiteSmall: {
        fontSize: 12,
        color: WHITE,
        marginBottom: 30,
        marginTop: 5,
        fontFamily: 'Montserrat-Bold'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    dateRow:{
        flexDirection: 'row',
        backgroundColor: WHITE,
        borderRadius: 10
    }
});


const mapStateToProps = (state) => {
    return {
        result: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getStops: () => dispatch(getStops())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)