import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Picker } from 'react-native';
import {connect} from 'react-redux';
import {getOperatorDetail} from '../redux/actions/operatorDataAction';
import Spinner from 'react-native-loading-spinner-overlay';
import { BLUE, WHITE, GREY, ORANGE, BACKGROUND } from '../utilities/colors';
import { USER_ROLE } from '../utilities/constants';
import moment from 'moment';
import DateTimePicker from "react-native-modal-datetime-picker";
import TextInputFlat from '../components/TextInputFlat';
import LightBlueButton from '../components/OrangeButton';
import {seatRequest} from '../redux/actions/seatAction';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getJourneyData } from '../redux/actions/journeyData';
import { modifyBooking } from '../redux/actions/ModifyBookingAction';
import OrangeButton from '../components/OrangeButton';

function index({response,getOperatorDetail, navigation, route, getJourneyData, seatRequest, modifyBooking}) {

  const { booking_id,ticket_id,route_schedule_id,stop_from_id,stop_to_id, date } = route.params;
  let seatCount = 0, seatsAll;
  const [data,setData] = useState(null);
  const [isAdmin,setIsAdmin] = useState(true);
  const [fromText,setFromText] = useState("Loading...");
  const [newDate,setNewDate] = useState(date);
  const [seats,setSeats] = useState("Loading...");
  const [dataSeats,setDataSeats] = useState([]);
  const [columns,setColumns] = useState(null);
  const [change,setChange] = useState(0);
  const [seatNumbers, setSeatNumbers] = useState([]);
  const [loading,setLoading] = useState(false);
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [isDateTimePickerReturnVisible, setIsDateTimePickerReturnVisible] = useState(false);
  const [departureDateTime,setDepartureDateTime] = useState(false);
  const [arrivalDateTime, setArrivalDateTime] = useState(false);
  const [routeScheduleId, setRouteScheduleId] = useState(route_schedule_id);
  const [selectedValue,setSelectedValue] = useState('java');
  const [minPrice,setMinPrice] = useState(0);
  const [maxPrice,setMaxPrice] = useState(0);

  const retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem(USER_ROLE);
      if (value !== null) {
        if(value == 6) {
          setIsAdmin(false);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    navigation.setOptions({
      title: "Modify Booking"
    })
    getOperatorDetail(ticket_id);
    getJourneyData(booking_id,ticket_id,date);
    seatRequest(date,route_schedule_id,stop_from_id,stop_to_id);
    retrieveData();
  },[])

  useEffect(() => {
    if (response.journeyData.error) {
      console.log("here is error", response);
    } else if (response.journeyData.loading) {
      setLoading(true);
    } else if (response.journeyData.payload) {
      setData(response.journeyData.payload.result);
      setDepartureDateTime(response.journeyData.payload.result.departure_datetime);
      setArrivalDateTime(response.journeyData.payload.result.arrival_datetime);
      setMinPrice(response.journeyData.payload.result.seat_price_min);
      setMaxPrice(response.journeyData.payload.result.seat_price_max);
      setSeats(response.journeyData.payload.result.seats);
      setLoading(false);
    }
  },[response.journeyData])

  useEffect(() => {
    var newData = [];
    if(response.seatReducer.seats.loading){
      setLoading(true);
    }
    if (response.seatReducer.seats.result) {
      newData = response.seatReducer.seats.result[0].seats;
      if(newData){
        if(newData.length > 0){
          var i=0, prevColumn = 0;
          for (const item of newData) {
            if(newData[i]){
              newData[i].isSelected = false;
              newData[i].indexMain = i;
              if(prevColumn < item.column) {
                prevColumn = item.column;
              } else {
                setColumns(prevColumn);
              }
            }
            i++;
          }
        }
      }
      setDataSeats(newData);
      setLoading(false);
    } else {
      console.log("not found");
    }
  },[response.seatReducer.seats]);

  useState(() => {
    if(response.modifyBooking.payload) {
      if(response.modifyBooking.payload.result){
        getOperatorDetail(ticket_id);
        getJourneyData(booking_id,ticket_id,newDate);
        seatRequest(newDate,route_schedule_id,stop_from_id,stop_to_id);
        retrieveData();
      }
    }
  },[response.modifyBooking]);


  const showDateTimePicker = () => {
    setIsDateTimePickerVisible(true);
  };
  const  hideDateTimePicker = (value) => {
    setIsDateTimePickerVisible(false);
  };
  const handleDatePicked = date => {
    hideDateTimePicker();
    setNewDate(moment(date).format("yyyy-MM-DD"));
  };
  const loadSeats = () => {
    setLoading(true);
    console.log("requesting",date,route_schedule_id,stop_from_id,stop_to_id);
    seatRequest(newDate,route_schedule_id,stop_from_id,stop_to_id);
  };

  const selectSeat = (itemData) => {
    var newData = dataSeats;
    itemData.isSelected = !itemData.isSelected;
    newData[itemData.indexMain] = itemData;
    const index = seatNumbers.indexOf(itemData.seat_number);
    if (index > -1) {
      seatNumbers.splice(index, 1);
    } else {
      seatNumbers.push(itemData.seat_number);
    }
    setDataSeats(newData);
    setChange(change+1);
  };

  const modefyBookingRequest = () => {
    setLoading(true);
    if(seatNumbers.length > 0){
      var obj = {}, i = 0;
      obj.booking_id = booking_id;
      obj.ticket_id = ticket_id;
      obj.date = newDate;
      obj.old_seats = seats;
      obj.departure_datetime = departureDateTime;
      obj.arrival_datetime = arrivalDateTime;
      obj.route_schedule_id = routeScheduleId;
      obj.sendCouponCode = false;
      var seatRequestArray = [];
      for (const item of seatNumbers) {
        var seatRequestData = {};
        if(i == 0) {
          seatRequestData.is_primary = true;
        } else {
          seatRequestData.is_primary = false;
        }
        seatRequestData.number = item;
        seatRequestData.min_price = minPrice;
        seatRequestData.price = maxPrice;
        seatRequestData.currency = "USD";
        seatRequestArray.push(seatRequestData);
        i++;
      }
      obj.seatData = seatRequestArray;
      console.log('modify data', obj);
      modifyBooking(obj);
    } else {
      console.log("not modefyiable");
      setLoading(false);
    }
  }

  const renderItem = (item) => {
    if (item.bookable && item.available && !item.isSelected) {
      return <TouchableOpacity onPress={() => selectSeat(item)}>
              <View style={styles.viewAvailable}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>
            </TouchableOpacity>;
    } else if (item.bookable && item.available && item.isSelected) {
      return <TouchableOpacity onPress={() => selectSeat(item)}>
              <View style={styles.viewSelected}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>
            </TouchableOpacity>;
    } else if (!item.bookable && !item.available && item.seat_number.length > 0) {
      return  <View style={styles.viewOccupied}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>;
    } else {
      return <View style={styles.viewHidden}>
        <Text style={styles.textHidden}>{item.seat_number}</Text>
      </View>;
    }
  }
    

  return (
    <View style={styles.view}>

      <Spinner
          visible={loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      <View style={{flex: 1}}>

      {data ? (
        <View style={styles.viewList}>
          <Text style={styles.whiteText}>{"Journey Date: "+moment(data.departure_datetime).format("DD-MM-YYYY hh:mm")}</Text>
          <Text style={styles.whiteText}>{data.name}</Text>
          <Text style={styles.whiteText}>{"Seats: "+data.seats}</Text>
        </View>
      ):(
        <View style={styles.viewList}>
          {/* <Text style={styles.whiteText}>Something gone wrong!</Text> */}
        </View>
      )}

      {/* <View style={{flexDirection: 'row'}}>
        <Picker
          selectedValue={selectedValue}
          style={{ height: 50, width: '100%' }}
          onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        >
          <Picker.Item label="Java" value="java" />
          <Picker.Item label="JavaScript" value="js" />
        </Picker>
      </View> */}

      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 1, marginEnd: 5}}>
          <TextInputFlat
            label="Date"
            textMain={newDate}
            onPress={() => showDateTimePicker()}
          >
          </TextInputFlat>
        </View>
        <View style={{flex: 1, marginStart: 5}}>
          <LightBlueButton text="Load" onPress={() => loadSeats()} />
        </View>
      </View>

      <View style={styles.infoTabRow}>
          <View style={styles.infoTab}>
            <MaterialCommunityIcons name="checkbox-blank-circle" color={WHITE} size={20} />
            <Text style={styles.textInfo}>Available</Text>
          </View>
          <View style={styles.infoTab}>
            <MaterialCommunityIcons name="checkbox-blank-circle" color={ORANGE} size={20} />
            <Text style={styles.textInfo}>Selected</Text>
          </View>
          <View style={styles.infoTab}>
            <MaterialCommunityIcons name="checkbox-blank-circle" color={BLUE} size={20} />
            <Text style={styles.textInfo}>Occupied</Text>
          </View>
        </View>

      <View style={styles.seatsLayout}>
        { dataSeats && columns > 0 &&
          <FlatList
            data={dataSeats}
            numColumns={columns}
            keyExtractor={(item) => item.indexMain}
            extraData={change}
            renderItem = {({item}) => renderItem(item)}
          />
        }
      </View>
      
      </View>
      <OrangeButton text='Register' onPress={() => {modefyBookingRequest()}} />

      <DateTimePicker
        isVisible={isDateTimePickerVisible}
        onConfirm={(date) => {handleDatePicked(date)}}
        onCancel={() => hideDateTimePicker(1)}
      />
    </View>
  );

}

const styles = StyleSheet.create({
  view: {
      backgroundColor: BACKGROUND,
      padding: 20,
      height: '100%',
      width: '100%',
      flex: 1
  },
  seatsLayout: {
    marginBottom: 10,
    marginStart: -20,
    marginEnd: -20,
    flex: 1,
    alignItems: 'center'
  },
  viewList:{
    marginBottom: 10,
    borderRadius: 10,
    padding: 10,
    borderColor: BLUE,
    backgroundColor: BLUE,
    color: WHITE
  },
  textRecycler:{
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderBottomColor: 'black',
    borderColor: 'white',
    borderRadius: 0,
  },
  button:{
    marginTop: 20
  },
  whiteText:{
    color: WHITE
  },
  viewAvailable: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: WHITE,
    backgroundColor: WHITE,
    color: GREY,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  textAvailable: {
    color: GREY
  },
  viewSelected: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: ORANGE,
    backgroundColor: ORANGE,
    color: WHITE,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  viewHidden: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: BACKGROUND,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  viewOccupied: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: BLUE,
    backgroundColor: BLUE,
    color: WHITE,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  textHidden: {
    color: BACKGROUND
  },
  textSelected: {
    color: WHITE
  },
  infoTabRow: {
    flexDirection: 'row',
    padding: 5
  },
  infoTab: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textInfo: {
    padding: 5,
    color: BLUE,
    fontWeight: '700'
  }
})

const mapStateToProps = state => {
  return {
      response: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getOperatorDetail: (ticket_id) => dispatch(getOperatorDetail(ticket_id)),
    seatRequest: (date,route_id,from,to) => dispatch(seatRequest(date,route_id,from,to)),
    getJourneyData: (booking_id,ticket_id,date) => dispatch(getJourneyData(booking_id,ticket_id,date)),
    modifyBooking: (data) => dispatch(modifyBooking(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)