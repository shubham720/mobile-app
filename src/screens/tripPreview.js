import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View, Platform, Alert } from 'react-native';
import OrangeButton from '../components/OrangeButton';
import GreyButton from '../components/GreyButton';
import TextReview from '../components/TextReview';
import {connect} from 'react-redux';
import { BLUE, GREY, WHITE } from '../utilities/colors';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import {customerBooking} from '../redux/actions/customerBookingAction';
import { CHECKOUT_SCREEN_NAME, PAYMENT_CONFIRM_NAME } from '../utilities/screenNames';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { paymentRequest } from '../redux/actions/paymentRequestAction';
import { markBooking } from '../redux/actions/markBooked';
import { USER_ROLE } from '../utilities/constants';

function index({navigation, route, customerBooking, response, paymentRequest, markBooking}) {

    const [loading,setLoading] = useState(false);
    const [called,setCalled] = useState(false);
    const [isPaymentInitialized, setIsPaymentInitialized] = useState(false);
    const [rewardPointsPayment, setRewardPointsPayment] = useState(0);
    const [isCalledConfirm, setIsCalledConfirm] = useState(false);
    
    const {
        date,
        route_schedule_id,
        seats,
        from_id,
        from_text,
        to_id,
        to_text,
        stop_to_id,
        stop_from_id,
        vehicle,
        departure_date,
        departure_time,
        price_min,
        price_max,
        first_name,
        last_name,
        email,
        phone,
        nationality,
        return_enabled,
        return_date,
        return_route_schedule_id,
        return_stop_from,
        return_stop_to,
        return_from_id,
        return_from_text,
        return_to_id,
        return_to_text,
        return_seats,
        return_vehicle,
        return_departure_date,
        return_departure_time,
        return_price_min,
        return_price_max
    } = route.params;

    var price = 0, return_price = 0, total_price = 0;
    if (return_enabled) {
        if (nationality === "Cambodian") {
            price = price_min;
            return_price = return_price_min;
        } else if(nationality === "Non-Cambodian") {
            price = price_max;
            return_price = return_price_max;
        }
        total_price = price*(seats.length) + return_price*(seats.length);
    } else {
        if (nationality === "Cambodian") {
            price = price_min;
        } else if(nationality === "Non-Cambodian") {
            price = price_max;
        }
        total_price = price*(seats.length);
    }

    const checkResponse = async () => {
      const user_role = await AsyncStorage.getItem(USER_ROLE);
      if(response.customerBooking.payload.status == "success"){
      if(user_role != 6) {
            console.log("making request", response.customerBooking.payload.result[0].booking_id);
            makePayment(response.customerBooking.payload.result[0].booking_id);
      } else {
        setLoading(false);
        navigation.navigate(CHECKOUT_SCREEN_NAME,
            {
                payload: response.customerBooking.payload.result,
                phone: phone
            }
        );
        }
      }
    }

    useEffect(() => {
        if (called) {
            checkResponse();
        }
    },[response.customerBooking.payload.result]);

    const moveToTripPreview = async () =>
    {
        setLoading(true);
        var bookingObj = {};
        bookingObj.action = "tentative";
        bookingObj.connecting = "0";
        bookingObj.request_from = "mobile_app";
        var seatArray = [], ticket_requests = {}, ticketRequestArray = [];
        var i = 0;
        for (const iterator of seats) {
            var seatObj = {};
            if (i == 0) {
                seatObj.is_primary = true;
            } else {
                seatObj.is_primary = false;
            }
            seatObj.min_price = price;
            seatObj.number = iterator;
            seatObj.passenger_id = i;
            seatObj.price = price;
            seatArray.push(seatObj);
            i++;
        }
        ticket_requests.address = " ";
        ticket_requests.contact_phone = "+855 "+phone;
        ticket_requests.date = date;
        ticket_requests.pickup_time = " ";
        ticket_requests.route_schedule_id = route_schedule_id;
        ticket_requests.stop_id_from = stop_from_id;
        ticket_requests.stop_id_to = stop_to_id;
        ticket_requests.taxi_drop_off = " ";
        ticket_requests.seat_requests = seatArray;
        ticketRequestArray.push(ticket_requests);
        if (return_enabled == 1){
            var seatArray = [], ticket_requests_two = {};
            var i = 0;
            for (const iterator of return_seats) {
                var seatObj = {};
                if (i == 0) {
                    seatObj.is_primary = true;
                } else {
                    seatObj.is_primary = false;
                }
                seatObj.min_price = price;
                seatObj.number = iterator;
                seatObj.passenger_id = i;
                seatObj.price = price;
                seatArray.push(seatObj);
                i++;
            }
            ticket_requests_two.address = " ";
            ticket_requests_two.contact_phone = "+855 "+phone;
            ticket_requests_two.date = return_date;
            ticket_requests_two.pickup_time = " ";
            ticket_requests_two.route_schedule_id = return_route_schedule_id;
            ticket_requests_two.stop_id_from = return_stop_from;
            ticket_requests_two.stop_id_to = return_stop_to;
            ticket_requests_two.taxi_drop_off = " ";
            ticket_requests_two.seat_requests = seatArray;
            ticketRequestArray.push(ticket_requests_two);
        }
        var passenger_requests_data = [];
        var countI = 0
        for (const iterator of seats) {
            var passenger_requests = {};
            passenger_requests.id = countI;
            passenger_requests.agency_id ="0";
            passenger_requests.block_phone = "0";
            passenger_requests.block_type = "Fixed";
            passenger_requests.bookingType = "other";
            passenger_requests.booking_remarks = "";
            passenger_requests.title = "Mr.";
            passenger_requests.first_name = first_name;
            passenger_requests.last_name = last_name;
            passenger_requests.name = first_name+" "+last_name;
            passenger_requests.gender = "male";
            passenger_requests.email_address = email;
            passenger_requests.mobile_number = "+855 " + phone;
            passenger_requests.nationality = nationality;
            passenger_requests_data.push(passenger_requests);
            countI++;
        }
        bookingObj.passenger_requests = passenger_requests_data;
        bookingObj.ticket_requests = ticketRequestArray;
        console.log("book obj: ",return_enabled, bookingObj);
        customerBooking(bookingObj);
        setCalled(true);
    };

    const goBack = (selection) => {
        console.log(selection);
    }

    if(response.error) {
        console.log(response.error);
    }

    const makePayment = (booking_id) => {
        setLoading(true);
        setIsPaymentInitialized(true);
        console.log("requesting payment");
        var paymentObj = {};
        paymentObj.address = "";
        paymentObj.contact_phone=phone;
        paymentObj.appCod = 0;
        paymentObj.booking_id = booking_id;
        paymentObj.coupon_code = "";
        paymentObj.currency = "USD";
        paymentObj.donation_amount = 0;
        paymentObj.experiences = [];
        paymentObj.insurance = 0;
        paymentObj.insuranceAmount  = 0;
        paymentObj.insuranceType = 0;
        paymentObj.payment_method = "paymanual";
        paymentObj.reward_points = rewardPointsPayment;
        paymentObj.reward_points_type = "user";
        paymentObj.total_amount = total_price;
        paymentObj.affiliate_id = null;
        paymentRequest(paymentObj);
    }

    useEffect(() => {
        if(isPaymentInitialized) {
            if(response.paymentRequest.payload) {
                if(response.paymentRequest.payload.status == "success") {
                    markPayment(response.paymentRequest.payload.result[0].booking_id);
                }
            }
        }
    },[response.paymentRequest]);

    const markPayment = (booking_id) => {
        setLoading(true);
        setIsCalledConfirm(true);
        markBooking(booking_id);
    }

    useEffect(() => {
        if (isCalledConfirm) {
            if (response.markBooking.payload) {
                if (response.markBooking.success) {
                    setLoading(false);
                    navigation.navigate(PAYMENT_CONFIRM_NAME);
                }
            }
        }
    },[response.markBooking]);

    return (
        <View style={styles.view}>

            <Spinner
                visible={loading}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={{
                padding: 20, 
                height: '100%',
                width: '100%'
            }}>
            <ScrollView>
                <View>
                    <View style={styles.viewList}>
                        <TextReview
                            label="From"
                            textMain={from_text}
                            imgIndex={0}
                        />
                        <TextReview
                            label="To"
                            textMain={to_text}
                            imgIndex={0}
                        />
                        <TextReview
                            label="Seat"
                            textMain={seats.join(",")}
                            imgIndex={1}
                        />
                        <TextReview
                            label="Type"
                            textMain={vehicle}
                            imgIndex={2}
                        />
                        <TextReview
                            label="Departure Date"
                            textMain={moment(departure_date).format('DD-MM-yyyy')}
                            imgIndex={3}
                        />
                        <TextReview
                            label="Departure Time"
                            textMain={moment(departure_time).format('HH:mm')}
                            imgIndex={4}
                        />
                        <TextReview
                            label="Price"
                            textMain={price*(seats.length)}
                            imgIndex={5}
                        />
                    </View>
                    { return_enabled == 1 &&

                    <View style={styles.viewList}>
                        <TextReview
                            label="From"
                            textMain={to_text}
                            imgIndex={0}
                        />
                        <TextReview
                            label="To"
                            textMain={from_text}
                            imgIndex={0}
                        />
                        <TextReview
                            label="Seat"
                            textMain={return_seats.join(",")}
                            imgIndex={1}
                        />
                        <TextReview
                            label="Type"
                            textMain={return_vehicle}
                            imgIndex={2}
                        />
                        <TextReview
                            label="Departure Date"
                            textMain={moment(return_departure_date).format('DD-MM-yyyy')}
                            imgIndex={3}
                        />
                        <TextReview
                            label="Departure Time"
                            textMain={moment(return_departure_date).format('HH:mm')}
                            imgIndex={4}
                        />
                        <TextReview
                            label="Price"
                            textMain={return_price*(seats.length)}
                            imgIndex={5}
                        />
                    </View>
                    }
                </View>
            </ScrollView>
            
            <View style={styles.bottomButtons}>
                
                    <OrangeButton text='Confirm' onPress={() => {moveToTripPreview()}}></OrangeButton>
                
            </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: WHITE,
        height: '100%',
        width: '100%'
    },
    viewList:{
        marginBottom: 10,
        flex: 1,
        backgroundColor: WHITE,
        borderWidth: 1,
        borderColor: GREY,
        padding: 20,
        borderRadius: 10,
    },
    text: {
        color: '#24B3E3',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    input: {
        color: '#353031',
        backgroundColor: '#ffffff',
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 3,
        marginRight: 10,
        padding: 10,
        flex: 1
    },
    bottomButtons: {
        marginTop: 10,
        // flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        width: '45%',
        textAlign: 'center',
    },
    buttonGrey: {
        width: '40%',
    }
});


const mapStateToProps = (state) => {
    return {
        response: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        customerBooking: (data) => dispatch(customerBooking(data)),
        paymentRequest: (data) => dispatch(paymentRequest(data)),
        markBooking: (data) => dispatch(markBooking(data)) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)