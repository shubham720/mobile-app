import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Alert, Platform } from 'react-native';
import {connect} from 'react-redux';
import {seatRequest} from '../redux/actions/seatAction';
import OrangeButton from '../components/OrangeButton';
import Spinner from 'react-native-loading-spinner-overlay';
import {BACKGROUND,WHITE,GREY, BLUE, ORANGE} from '../utilities/colors';
import {PASSENGER_DETAILS_SCREEN_NAME} from '../utilities/screenNames';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

function index({response, navigation, route, seatRequest}) {
  
  const { 
    date,
    route_schedule_id,
    stop_from,
    stop_to,
    from_id,
    from_text,
    to_id,
    to_text,
    seats,
    vehicle,
    departure_date,
    departure_time,
    price_min,
    price_max,
    return_date,
    return_enabled,
    return_route_schedule_id,
    return_stop_from,
    return_stop_to,
    return_from_id,
    return_from_text,
    return_to_id,
    return_to_text,
    return_vehicle,
    return_departure_date,
    return_departure_time,
    return_price_min,
    return_price_max
  } = route.params;


  console.log("Return seat layout", route.params);

  const [data,setData] = useState([]);
  const [columns,setColumns] = useState(null);
  const [change,setChange] = useState(0);
  const [seatNumbers, setSeatNumbers] = useState([]);
  const [isAndroid, setIsAndroid] = useState(false);

  useEffect(() => {
      seatRequest(return_date,return_route_schedule_id,return_stop_from,return_stop_to);
      navigation.setOptions({
        title: "Select Seats"
      });
  },[])

  useEffect(() => {
    var newData = [];
    if (response.seatReducer.seats.result) {
      newData = response.seatReducer.seats.result[0].seats;
      if(newData){
        if(newData.length > 0) {
          var i=0, prevColumn = 0;
          for (const item of newData) {
            if(newData[i]){
              newData[i].isSelected = false;
              newData[i].indexMain = i;
              if(prevColumn < item.column) {
                prevColumn = item.column;
              } else {
                setColumns(prevColumn);
              }
            }
            i++;
          }
        }
      }
      setData(newData);
    } else {
      console.log("not found");
    }
  },[response.seatReducer.seats]);

  const selectSeat = (itemData) => {
    var newData = data;
    itemData.isSelected = !itemData.isSelected;
    newData[itemData.indexMain] = itemData;
    const index = seatNumbers.indexOf(itemData.seat_number);
    if (index > -1) {
      seatNumbers.splice(index, 1);
    } else {
      seatNumbers.push(itemData.seat_number);
    }
    setData(newData);
    setChange(change+1);
  };

  

  // if (response.seatReducer.seats.result) {
  //   if(!data){
  //     data = response.seatReducer.seats.result[0].seats;
  //   }
  //   if(data){
  //     if(data.length > 0){
  //       var i=0, prevColumn = 0;
  //       for (const item of data) {
  //         if(data[i]){
  //           data[i].isSelect = false;
  //           data[i].selectedClass = styles.textAvailable;
  //           data[i].seelctedClassView = styles.viewAvailable;
  //           data[i].indexMain = i;
  //           if(prevColumn < item.column) {
  //             prevColumn = item.column;
  //           } else {
  //             columns = prevColumn;
  //           }
  //         }
  //         i++;
  //       }
  //     }
  //   }
  // } else {
  //   console.log("not found");
  // }

  const openPassengerDetails = () => {
    if(seatNumbers.length > 0) {
      if(seatNumbers.length === seats.length){
        navigation.navigate(PASSENGER_DETAILS_SCREEN_NAME,{
          date: date,
          route_schedule_id: route_schedule_id,
          seats: seats,
          from_id: from_id,
          from_text: from_text,
          to_id: to_id,
          to_text: to_text,
          stop_to_id: stop_to,
          stop_from_id: stop_from,
          vehicle: vehicle,
          departure_date: departure_date,
          departure_time: departure_time,
          return_seats: seatNumbers,
          price_min: price_min,
          price_max: price_max,
          return_enabled: return_enabled,
          return_date: return_date,
          return_route_schedule_id: return_route_schedule_id,
          return_stop_from: return_stop_from,
          return_stop_to: return_stop_to,
          return_from_id: return_from_id,
          return_from_text: return_from_text,
          return_to_id: return_to_id,
          return_to_text: return_to_text,
          return_vehicle: return_vehicle,
          return_departure_date: return_departure_date,
          return_departure_time: return_departure_time,
          return_price_min: return_price_min,
          return_price_max: return_price_max
        });
      } else {
        Alert.alert(
          "",
          "Seat quantity ("+seats.length+") should be same in return",
          [
              {
                  text: "Ok",
                  style: "cancel",
              }
          ],
          {
              cancelable: true
          }
        );
      }
    } else {
      Alert.alert(
        "",
        "Please select at least one seat",
        [
            {
                text: "Ok",
                style: "cancel",
            }
        ],
        {
            cancelable: true
        }
      );
    }
  }

  const renderItem = (item) => {
    if (item.bookable && item.available && !item.isSelected) {
      return <TouchableOpacity onPress={() => selectSeat(item)}>
              <View style={styles.viewAvailable}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>
            </TouchableOpacity>;
    } else if (item.bookable && item.available && item.isSelected) {
      return <TouchableOpacity onPress={() => selectSeat(item)}>
              <View style={styles.viewSelected}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>
            </TouchableOpacity>;
    } else if (!item.bookable && !item.available && item.seat_number.length > 0) {
      return  <View style={styles.viewOccupied}>
                <Text style={styles.textAvailable}>{item.seat_number}</Text>
              </View>;
    } else {
      return <View style={styles.viewHidden}>
        <Text style={styles.textHidden}>{item.seat_number}</Text>
      </View>;
    }
  }

  return (
    <View style={styles.view}>

      {isAndroid &&
        <View style={{
          backgroundColor: BLUE,
          flexDirection: 'row',
          paddingTop: 5,
          paddingStart: 5,
          paddingBottom: 5,
          alignItems: 'center'
        }}>
            <TouchableOpacity onPress={navigation.goBack}>
              <View style={{ width: 40 }}>
                <MaterialCommunityIcons name="chevron-left" color={WHITE} size={40} />
              </View>
            </TouchableOpacity>
            <View style={{flex: 1}}>
              <Text style={{
                color: WHITE, 
                fontFamily: 'Montserrat-Bold',
                alignSelf: 'center',
                fontSize: 15}}>
                Select Seats
              </Text>
            </View>
            <View style={{ width: 40 }}></View>
        </View>
      }

      <Spinner
          visible={response.seatReducer.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      <View style={{
        height: '100%',
        width: '100%',
      }}>
        <View style={styles.infoTabRow}>
          <View style={styles.infoTab}>
            <MaterialCommunityIcons name="checkbox-blank-circle" color={WHITE} size={20} />
            <Text style={styles.textInfo}>Available</Text>
          </View>
          <View style={styles.infoTab}>
            <MaterialCommunityIcons name="checkbox-blank-circle" color={ORANGE} size={20} />
            <Text style={styles.textInfo}>Selected</Text>
          </View>
          <View style={styles.infoTab}>
            <MaterialCommunityIcons name="checkbox-blank-circle" color={BLUE} size={20} />
            <Text style={styles.textInfo}>Occupied</Text>
          </View>
        </View>
      
        <View style={styles.viewList}>
          { columns > 0 &&
            <FlatList
              data={data}
              numColumns={columns}
              keyExtractor={(item) => `${item.indexMain}`}
              extraData={change}
              renderItem = {({item}) => renderItem(item)}
            />
          }
        </View>
        <View style={{
            padding: 20
          }}>
          <OrangeButton text='Continue' onPress={() => openPassengerDetails()} style={styles.button}></OrangeButton>
        </View>
      </View>
    </View>
  );

}

const styles = StyleSheet.create({
  view: {
    backgroundColor: BACKGROUND,
    height: '100%',
    width: '100%'
  },
  viewList: {
    flex: 1,
    alignItems: 'center'
  },
  viewAvailable: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: WHITE,
    backgroundColor: WHITE,
    color: GREY,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  textAvailable: {
    color: GREY
  },
  viewSelected: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: ORANGE,
    backgroundColor: ORANGE,
    color: WHITE,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  viewHidden: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: BACKGROUND,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  viewOccupied: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderColor: BLUE,
    backgroundColor: BLUE,
    color: WHITE,
    borderRadius: 5,
    margin: 5,
    width: 40,
    alignItems: 'center',
    alignContent: 'center',
  },
  textHidden: {
    color: BACKGROUND
  },
  button:{
    marginTop: 20
  },
  infoTabRow: {
    flexDirection: 'row',
    padding: 5
  },
  infoTab: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textInfo: {
    padding: 5,
    color: BLUE,
    fontFamily: 'Montserrat-Bold'
  }
})

const mapStateToProps = state => {
  return {
      response: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    seatRequest: (date,route_schedule_id,stop_from,stop_to) => dispatch(seatRequest(date,route_schedule_id,stop_from,stop_to))
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (index)