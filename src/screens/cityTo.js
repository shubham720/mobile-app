import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, TextInput } from 'react-native';
import {connect} from 'react-redux';
import {cityTo} from '../redux/actions/cityToAction';
import LightBlueButton from '../components/LightBlueButton';
import Spinner from 'react-native-loading-spinner-overlay';
import { FILTER_BOOKINGS_SCREEN_NAME, HOME_SCREEN_NAME, SEARCH_SCREEN_NAME } from '../utilities/screenNames';
import { BLUE, GREY, UNDERLINEGREY, WHITE } from '../utilities/colors';
import filter from 'lodash.filter';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { OPERATOR_ALL_ROUTES, OPERATOR_STOPS } from '../utilities/constants';

function index({cityData,cityTo, navigation, route}) {
  
  React.useEffect(() => {
    console.log(route.params);
    if (route.params?.page) {
      if(route.params.page == 2){
        setPage(2);
      } else {
        setPage(1);
      }
    }
  },[route.param]);

  const { from_id } = route.params;
  const [data,setData] = useState([]);
  const [filteredData,setFilteredData] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [page,setPage] = useState(0);

  useEffect(() => {
    initiate();
  },[])

  const initiate = async () => {
    var newArray = [];
    var stringCount = from_id.toString();
    var cityData = JSON.parse(await AsyncStorage.getItem(OPERATOR_ALL_ROUTES));
    if(cityData.result.destinations[stringCount]) {
      var stopsVaild = (await AsyncStorage.getItem(OPERATOR_STOPS)).split(',');
      for (const itemMainArray of cityData.result.destinations[stringCount]) {
        for (const item of stopsVaild) {
          if(item == itemMainArray.id) {
            newArray.push(itemMainArray);
          }
        }
      }
    }
    for (const item of newArray) {
      if(item.id == from_id) {
        newArray.splice(newArray.indexOf(item),1);
      }
    }
    setData(newArray);
    setFilteredData(newArray);
  }

  

  const searchthrough = text => {
    const formattedQuery = text.toLowerCase();
    const filtering = filter(data, item => {
      return contains(item, formattedQuery);
    });
    setFilteredData(filtering);
  };
  const contains = ({ name }, query) => {
    if (name.toLowerCase().includes(query)) {
      return true;
    }
    return false;
  };

  const citySelected = (id,name) => {
    if(page == 1) {
      navigation.navigate(HOME_SCREEN_NAME,{
        screen: SEARCH_SCREEN_NAME,
        params: {
          toIdSelected: id,
          toTextSelected: name
        },
        merge: true,
      });
    } else {
      navigation.navigate(FILTER_BOOKINGS_SCREEN_NAME,{
        toIdSelected: id,
          toTextSelected: name
      });
    }
};

  return (
    <View style={styles.view}>
      <Spinner
          visible={cityData.cityToList.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.whiteBoard}> 
      <View style={{
          alignItems: 'center',
          marginTop: -30
        }}>
          <View style={{
            width: 60,
            alignItems: 'center',
            borderWidth: 10,
            borderWidth: 3,
            borderBottomColor: GREY,
            borderColor: WHITE,
            borderRadius: 0,
            flexDirection: 'row'
          }}>
          </View>
        </View>
        <View>
          <TextInput 
          style={styles.inputView}
          placeholder="Search"
          value={searchText}
          onChangeText={(text) => {
            searchthrough(text);
            setSearchText(text);
          }}
          ></TextInput>
        </View>
        <View style={styles.viewList}>
          <FlatList
            data={filteredData}
            renderItem = {({item}) => (
              <TouchableOpacity onPress={() => citySelected(item.id,item.name)}>
               <View style={styles.textRecycler}>
                  <View style={{
                    flexDirection: 'row',
                    flex: 1
                  }}>
                    <MaterialCommunityIcons name="map-marker" color={BLUE} size={24} style={styles.mapMark} />
                    <Text style={{fontSize: 20, paddingStart: 10, paddingTop: 5, fontFamily: 'Montserrat-Medium', color: '#707070'}}>{item.name}</Text>
                  </View>
                  <View >
                    <MaterialCommunityIcons name="checkbox-marked-circle" color={GREY} size={24}  style={{alignSelf: 'flex-end'}}/>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    </View>
  );

}

const styles = StyleSheet.create({
  view: {
      backgroundColor: BLUE,
      height: '100%',
      width: '100%'
  },
  viewList: {
    marginBottom: 10,
    flex: 1
  },
  whiteBoard:{
    backgroundColor: WHITE,
    borderTopEndRadius: 50,
    borderTopStartRadius: 50,
    flex: 1,
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 40,
    marginTop: 60
  },
  textRecycler: {
    padding: 15,
    marginBottom: 2,
    borderWidth: 1,
    borderBottomColor: UNDERLINEGREY,
    borderColor: 'white',
    borderRadius: 0,
    flexDirection: 'row'
  },
  button: {
    marginTop: 20
  },
  inputView: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GREY,
    padding: 10,
    borderRadius: 10,
    marginStart: 10,
    marginEnd: 10,
    marginTop: 30
  },
  mapMark: {
    borderWidth: 1,
    borderColor: UNDERLINEGREY,
    padding: 5,
    borderRadius: 10
  }
})

const mapStateToProps = state => {
  return {
      cityData: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    cityTo: (from_id) => dispatch(cityTo(from_id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)