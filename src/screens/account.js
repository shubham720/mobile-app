import * as React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';
import { BLUE, WHITE, GREY} from '../utilities/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ACCOUNT_INFO_SCREEN_NAME, LANGUAGE_SCREEN_NAME, LOGIN_REGISTER_SCREEN_NAME, LOGIN_SCREEN_NAME } from '../utilities/screenNames';
import AsyncStorage from "@react-native-async-storage/async-storage";
import LocalizedStrings from '../utilities/localizationSettings';

export default function index({navigation}) {

  const logout = () => {
    AsyncStorage.clear();
    navigation.reset({
      index: 0,
      routes: [{name: LOGIN_REGISTER_SCREEN_NAME}],
    });
  }

  const openAccountInfo = () => {
    navigation.navigate(ACCOUNT_INFO_SCREEN_NAME);
  }

  const openLanguage = () => {
    navigation.navigate(LANGUAGE_SCREEN_NAME);
  }

  return (
    <View style={styles.mainView}>
      <View style={styles.blueView}>
        <Text style={styles.textUnderBlue}>{LocalizedStrings.account}</Text>
      </View>
      <View style={styles.mainMenu}>

        <TouchableOpacity onPress={() => openAccountInfo()}>
          <View style={styles.tabs}>
            <MaterialCommunityIcons name="account" size={26} color={BLUE} />
            <Text style={styles.text}>{LocalizedStrings.account}</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => openLanguage()}>
          <View style={styles.tabs}>
            <MaterialCommunityIcons name="alpha-t-circle-outline" size={26} color={BLUE} />
            <Text style={styles.text}>Language</Text>
          </View>
        </TouchableOpacity>
        
        <TouchableOpacity onPress={() => logout()}>
          <View style={styles.tabs}>
            <MaterialCommunityIcons name="power" size={26} color={BLUE} />
            <Text style={styles.text}>Log out</Text>
          </View>
        </TouchableOpacity>

      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: WHITE,
    height: '100%',
    width: '100%'
  },
  blueView: {
    backgroundColor: BLUE,
    height: 140,
    padding: 20
  },
  textUnderBlue: {
    fontSize: 25,
    color: WHITE,
    paddingTop: 60,
    fontFamily: 'Montserrat-Bold'
  },
  mainMenu: {
    padding:20
  },
  tabs: {
    backgroundColor: WHITE,
    borderBottomColor: GREY,
    borderTopColor: WHITE,
    borderStartColor: WHITE,
    borderEndColor: WHITE,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    color: BLUE,
    paddingBottom: 20,
    paddingEnd: 20,
    paddingTop: 20,
    paddingStart: 10,
    fontWeight: '600',
    fontSize: 18,
    fontFamily: 'Montserrat-Medium'
  }
});
