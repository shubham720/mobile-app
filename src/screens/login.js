import React, { useState,useEffect } from 'react';
import { ImageBackground, StyleSheet, Text, View, Alert, TouchableOpacity } from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import LightBlueButton from '../components/LightBlueButton';
import TextFieldFlat from '../components/TextFieldFlat';
import {login} from '../redux/actions/loginAction';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import {HOME_SCREEN_NAME} from '../utilities/screenNames';
import {USER_ID} from '../utilities/constants';
import { BLUE, ORANGE, WHITE } from '../utilities/colors';
import OrangeButton from '../components/OrangeButton';
import { cityFrom } from '../redux/actions/cityFromAction';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LocalizedStrings from '../utilities/localizationSettings';

function index({navigation, route, login, result, cityFrom}) {

    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [loginClicked,setLoginClicked] = useState(false);

    useEffect(() => {
        AsyncStorage.clear();
        cityFrom();
    },[])

    const doLogin = () =>
    {
        if (email && password) {
            login(email,password)
            setLoginClicked(true);
        } else {
            console.log("email or password empty");
        }
    };
    
      useEffect(() => {
        if (loginClicked) {
            if(result.login.success) {
                navigation.reset({
                    index: 0,
                    routes: [{name: HOME_SCREEN_NAME}],
                });
            }
        } 
        if (result.login.error) {
            Alert.alert(
                "Login failed",
                "Provided Email/password is wrong!",
                [
                    {
                        text: "Ok",
                        style: "cancel",
                    }
                ],
                {
                    cancelable: true
                }
            );
        }
      },[result.login]);

    return (
        <View style={styles.view}>

                <TouchableOpacity onPress={() => navigation.goBack() }>
                    <MaterialCommunityIcons name="chevron-down" color={WHITE} size={30} style={{padding: 20}} />
                </TouchableOpacity>
            <Spinner
                visible={result.login.loading}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
                <View style={styles.viewInner}>
                    <Text style={styles.loginText}>{LocalizedStrings.sign_in}</Text>
                    <TextFieldFlat
                        label="Email"
                        placeholder="abc@example.com"
                        onChangeText={(text) => setEmail(text)}
                        defaultValue={email}
                    />
                    <TextFieldFlat
                        label="Password"
                        secureTextEntry={true}
                        placeholder="********"
                        onChangeText={(text) => setPassword(text)}
                        defaultValue={password}
                    />
                    <OrangeButton text={LocalizedStrings.sign_in} onPress={() => {doLogin()}} />
                </View>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: ORANGE
    },
    viewInner: {
        backgroundColor: WHITE,
        borderTopEndRadius: 50,
        borderTopStartRadius: 50,
        flex: 1,
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 40,
        marginTop: 0
    },
    text: {
        color: '#24B3E3',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        fontFamily: 'Montserrat-Medium'
    },
    bottomSheet: {
        flex: 1,
        width: '100%'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    loginText: {
        alignSelf: 'center',
        marginBottom: 30,
        fontSize: 29,
        color: ORANGE,
        fontFamily: 'Montserrat-Bold'
    }
});


const mapStateToProps = (state) => {
    return {
        result: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (email,password) => dispatch(login(email,password)),
        cityFrom: () => dispatch(cityFrom())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)