import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {searchBus} from '../redux/actions/searchBusAction';
import {connect} from 'react-redux';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SEARCH_SCREEN from './search';
import MY_TRIPS_SCREEN from './myTrips';
import ACCOUNT_SCREEN from './account';
import { SEARCH_SCREEN_NAME,MY_TRIPS_SCREEN_NAME,ACCOUNT_SCREEN_NAME } from '../utilities/screenNames';
import LocalizedStrings from '../utilities/localizationSettings';

function index({navigation, route}) {

    const Tab = createMaterialBottomTabNavigator();

    return (
      <Tab.Navigator
        initialRouteName={SEARCH_SCREEN_NAME}
        activeColor="grey"
        inactiveColor="#D3D3D3"
        barStyle={{
          backgroundColor: '#fff' ,
        }}
        options={{
          fontFamily: 'Montserrat-Medium',
        }}
      >
      <Tab.Screen
        name={SEARCH_SCREEN_NAME}
        component={SEARCH_SCREEN}
        options={{
          tabBarLabel: LocalizedStrings.search,
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="magnify" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen
        name={MY_TRIPS_SCREEN_NAME}
        component={MY_TRIPS_SCREEN}
        options={{
          tabBarLabel: LocalizedStrings.my_trips,
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="glasses" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen
        name={ACCOUNT_SCREEN_NAME}
        component={ACCOUNT_SCREEN}
        options={{
          tabBarLabel: LocalizedStrings.account,
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account-circle-outline" color={color} size={24} />
          ),
        }}
      />
    </Tab.Navigator> 
    );
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
        // result: this.state
    }
}

const mapDispatchToProps = (dispatch) => {
    // console.log(dispatch);
    return {
        // search: dispatch(searchBus("hello"))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)