import React,{useEffect} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { BLUE, WHITE, GREY } from '../utilities/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { USER_ID } from '../utilities/constants';
import { HOME_SCREEN_NAME, LANGUAGE_SCREEN_NAME, LOGIN_SCREEN_NAME } from '../utilities/screenNames';
import AsyncStorage from "@react-native-async-storage/async-storage";
// import analytics from '@segment/analytics-react-native';

export default function index({navigation}) {
  
  const retrieveData = async () => {
    try {

      // await analytics.setup('66EJst1CClJ9SGkLFP1Ef40sVdHzwPSt', {
      //   // Record screen views automatically!
      //   recordScreenViews: true,
      //   // Record certain application events automatically!
      //   trackAppLifecycleEvents: true
      // })

      const value = await AsyncStorage.getItem(USER_ID);
      if (value !== null) {
        navigation.reset({
          index: 0,
          routes: [{name: LANGUAGE_SCREEN_NAME}],
        });
      } else {
        navigation.reset({
          index: 0,
          routes: [{name: LOGIN_SCREEN_NAME}],
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    retrieveData()
  },[]);

  return (
    <View style={styles.mainView}>
      <View style={styles.blueView}>
        <Text style={styles.textUnderBlue}>Camboticket Buvasea</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: BLUE,
    height: '100%',
    width: '100%'
  },
  blueView: {
    backgroundColor: BLUE,
    height: 140,
    padding: 20
  },
  textUnderBlue: {
    fontSize: 25,
    fontWeight: '700',
    color: WHITE,
    paddingTop: 60
  },
  mainMenu: {
    padding:20
  },
  tabs: {
    backgroundColor: WHITE,
    borderBottomColor: GREY,
    borderTopColor: WHITE,
    borderStartColor: WHITE,
    borderEndColor: WHITE,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    color: BLUE,
    paddingBottom: 20,
    paddingEnd: 20,
    paddingTop: 20,
    paddingStart: 10,
    fontWeight: '600',
    fontSize: 18
  }
});
