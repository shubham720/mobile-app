import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TextInput, ScrollView, Alert, Platform } from 'react-native';
import OrangeButton from '../components/OrangeButton';
import TextFieldFlat from '../components/TextFieldFlat';
import { connect } from 'react-redux';
import Radio from '../components/Radio';
import { BACKGROUND, BLUE, WHITE } from '../utilities/colors';
import { TRIP_PREVIEW_SCREEN_NAME } from '../utilities/screenNames';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { USER_EMAIL, USER_FIRST_NAME, USER_LAST_NAME, USER_PHONE } from '../utilities/constants';

function index({navigation, route}) {
    
    const {
        date,
        route_schedule_id,
        seats,
        from_id,
        from_text,
        to_id,
        to_text,
        stop_to_id,
        stop_from_id,
        vehicle,
        departure_date,
        departure_time,
        price_min,
        price_max,
        return_enabled,
        return_date,
        return_route_schedule_id,
        return_stop_from,
        return_stop_to,
        return_from_id,
        return_from_text,
        return_to_id,
        return_to_text,
        return_seats,
        return_vehicle,
        return_departure_date,
        return_departure_time,
        return_price_min,
        return_price_max
    } = route.params;

    useEffect(() => {
        retrieveData();
    },[]);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email,setEmail] = useState("");
    const [phone,setPhone] = useState("");
    const [nationalityCount, setNationalityCount] = useState(0);
    const [nationality,setNationality] = useState("");
    const [isAndroid, setIsAndroid] = useState(false);

    const retrieveData = async () => {
        try {
          const UserFirstName = await AsyncStorage.getItem(USER_FIRST_NAME);
          if (UserFirstName !== null) {
            setFirstName(UserFirstName);
          }
          const UserLastName = await AsyncStorage.getItem(USER_LAST_NAME);
          if (UserLastName !== null) {
            setLastName(UserLastName);
          }
          const UserEmail = await AsyncStorage.getItem(USER_EMAIL);
          if (UserEmail !== null) {
            setEmail(UserEmail);
          }
          const UserPhone = await AsyncStorage.getItem(USER_PHONE);
          if (UserPhone !== null) {
            setPhone(UserPhone);
          }
        } catch (error) {
          console.log(error);
        }
      };

    const moveToTripPreview = () =>
    {
        if(firstName && lastName && email && phone && nationality) {
            navigation.navigate(TRIP_PREVIEW_SCREEN_NAME, {
                date: date,
                route_schedule_id: route_schedule_id,
                seats: seats,
                from_id: from_id,
                from_text: from_text,
                to_id: to_id,
                to_text: to_text,
                stop_to_id: stop_to_id,
                stop_from_id: stop_from_id,
                vehicle: vehicle,
                departure_date: departure_date,
                departure_time: departure_time,
                price_min: price_min,
                price_max: price_max,
                first_name: firstName,
                last_name: lastName,
                email: email,
                phone: phone,
                nationality: nationality,
                return_date: return_date,
                return_enabled: return_enabled,
                return_route_schedule_id: return_route_schedule_id,
                return_stop_from: return_stop_from,
                return_stop_to: return_stop_to,
                return_from_id: return_from_id,
                return_from_text: return_from_text,
                return_to_id: return_to_id,
                return_to_text: return_to_text,
                return_seats: return_seats,
                return_vehicle: return_vehicle,
                return_departure_date: return_departure_date,
                return_departure_time: return_departure_time,
                return_price_min: return_price_min,
                return_price_max: return_price_max
            })
        } else {
            var string = "";
            if(!firstName)
                string+="fill First name, ";
            if(!lastName)
                string+="fill Last name, ";
            if(!email)
                string+="fill email, ";
            if(!phone)
                string+="fill phone number, ";
            if(!nationality)
                string+="select Nationality";

            Alert.alert(
                "Incorrect input",
                string,
                [
                    {
                        text: "Ok",
                        style: "cancel",
                    }
                ],
                {
                    cancelable: true
                }
            );
        }
    };

    const nationalityData = [
        {text: "Cambodian",id: 1},
        {text: "Non-Cambodian",id: 2}
    ];

    const radioSelected = (count,text) => {
        console.log(count,text);
        setNationalityCount(count);
        setNationality(text);
    }

    return (
        <View style={styles.view}>
            { isAndroid &&
        <View style={{
          backgroundColor: BLUE,
          flexDirection: 'row',
          paddingTop: 5,
          paddingStart: 5,
          paddingBottom: 5,
          alignItems: 'center'
        }}>
            <TouchableOpacity onPress={navigation.goBack}>
              <View style={{ width: 40 }}>
                <MaterialCommunityIcons name="chevron-left" color={WHITE} size={40} />
              </View>
            </TouchableOpacity>
            <View style={{flex: 1}}>
              <Text style={{
                color: WHITE, 
                fontFamily: 'Montserrat-Bold',
                alignSelf: 'center',
                fontSize: 15}}>
                Passenger Details
              </Text>
            </View>
            <View style={{ width: 40 }}></View>
        </View>
      }
      <View style={{
            padding: 20, 
            height: '100%',
            width: '100%'
        }}>
            <ScrollView>
                <View style={styles.viewList}>
                    <Radio
                        data={nationalityData}
                        value={nationalityCount}
                        columns={2}
                        onPress={(count,text) => radioSelected(count,text)}
                    >
                    </Radio>
                    <TextFieldFlat
                        label="First Name"
                        placeholder="Abc"
                        onChangeText={(text) => setFirstName(text)}
                        defaultValue={firstName}
                    />
                    <TextFieldFlat
                        label="Last Name"
                        placeholder="Abc"
                        onChangeText={(text) => setLastName(text)}
                        defaultValue={lastName}
                    />
                    <TextFieldFlat
                        label="Email"
                        placeholder="abc@example.com"
                        onChangeText={(text) => setEmail(text)}
                        defaultValue={email}
                    />
                    <TextFieldFlat
                        label="Phone"
                        placeholder="00000000"
                        onChangeText={(text) => setPhone(text)}
                        defaultValue={phone}
                    />
                </View>
            </ScrollView>
            <OrangeButton text='Next' onPress={() => {moveToTripPreview()}} style={styles.button}></OrangeButton>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: BACKGROUND,
        height: '100%',
        width: '100%'
    },
    viewList:{
      marginBottom: 10,
      flex: 1
    },
    text: {
        color: '#24B3E3',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    bottomSheet:{
        flex: 1,
        width: '100%'
    },
    input: {
        color: '#353031',
        backgroundColor: '#ffffff',
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 3,
        marginRight: 10,
        padding: 10,
        flex: 1
    },
    button:{
        // marginTop: 20
    }
});


const mapStateToProps = (state) => {
    // console.log(state);
    return {
        result: this.state
    }
}

const mapDispatchToProps = (dispatch) => {
    // console.log(dispatch);
    return {
        // search: dispatch(search())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)