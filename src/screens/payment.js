import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import WebView from 'react-native-webview';
import OrangeButton from '../components/OrangeButton';
import {connect} from 'react-redux';
import { BLUE, DARKGREY, GREY, WHITE } from '../utilities/colors';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import {paymentOffer} from '../redux/actions/paymentOfferAction';
import TextFieldFlat from '../components/TextFieldFlat'
import RadioImages from '../components/RadioImages';
import { CHECKOUT_SCREEN_NAME } from '../utilities/screenNames';


function index({navigation, route, paymentOffer, response}) {

  console.log("Payment params: ",route.params);
  const {payment_id,url,postData,key,value} = route.params;

  const cancelPayment = () => {
    navigation.goBack();
  }
  
  return (
    <View style={styles.mainView}>
      <View style={styles.bottomButtons}>
        <OrangeButton text='Cancel' onPress = {() => {cancelPayment()}} />
      </View>
      <View style={styles.viewData}>
        <WebView
          source={{
            uri: url,
            method: "POST",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
            body: postData
          }}
          startInLoadingState
          scalesPageToFit
          javaScriptEnabled = {true}
          style={{ flex: 1 }}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
    height: '100%',
    width: '100%'
  },
  viewData: {
    marginBottom: 10,
    flex: 1,
  },
  paymentView: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GREY,
    padding: 20,
    borderRadius: 10,
    marginStart: 20,
    marginEnd: 20
  },
  paymentText: {
    flexDirection: 'row'
  },
  paymentHeader: {
    marginTop: -30,
    backgroundColor: WHITE,
    padding: 3,
    color: BLUE
  },
  calculationsView: {
    backgroundColor: BLUE,
    marginTop: 20,
    padding: 20,
    minHeight: 150
  },
  paymentSelecter: {
    flexDirection: 'row'
  },
  pricesContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  whiteTextLeft: {
    color: WHITE,
    flex: 1,
    textAlign: 'left'
  },
  whiteTextRight: {
    color: WHITE,
    flex: 1,
    textAlign: 'right'
  },
  tncView: {
    backgroundColor: GREY,
    minHeight: 50,
    padding: 10,
    flexDirection: 'column',
    textAlign: 'center'
  },
  bottomButtons: {
    justifyContent: 'space-between',
  },
  blueTextLowerLeft: {
    flex: 1,
    fontSize: 18,
    fontWeight: '800',
    color: BLUE,
    paddingStart: 5,
    paddingBottom: 10,
    textAlign: 'left'
  },
  blueTextLowerRight: {
    flex: 1,
    fontSize: 18,
    fontWeight: '800',
    color: BLUE,
    paddingEnd: 5,
    paddingBottom: 10,
    textAlign: 'right'
  },
});

const mapStateToProps = (state) => {
  return {
      response: state.paymentDetails
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentOffer: (data) => dispatch(paymentOffer(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)