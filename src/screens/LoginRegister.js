import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import LightBlueButton from '../components/LightBlueButton';
import {connect} from 'react-redux';
import { LOGIN_SCREEN_NAME, REGISTER_SCREEN_NAME, HOME_SCREEN_NAME} from '../utilities/screenNames';
import stringsOfLanguages from '../utilities/localizationSettings';
import { WHITE } from '../utilities/colors';
import DarkBlueButton from '../components/DarkBlueButton';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { USER_ID, USER_LANGUAGE } from '../utilities/constants';
import LocalizedStrings from '../utilities/localizationSettings';

function index({navigation, route, login, result}) {

    const retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem(USER_ID);
          const langVal = await AsyncStorage.getItem(USER_LANGUAGE); 
          if(langVal !== null) {
            stringsOfLanguages.setLanguage(langVal);  
          }
          if (value !== null) {
            navigation.reset({
              index: 0,
              routes: [{name: HOME_SCREEN_NAME}],
            });
          }
        } catch (error) {
          console.log(error);
        }
    };

    useEffect(() =>{
        retrieveData();
    },[]);


    const openLogin = () =>
    {
      navigation.navigate(LOGIN_SCREEN_NAME);
    };
    
    const openRegister = () =>
    {
      navigation.navigate(REGISTER_SCREEN_NAME);
    };

    return (
        <View style={styles.view}>
            <ImageBackground source={require('../images/png/backimage.png')} style={styles.backgroundImage}>
            <View style={styles.viewInner}>
              <View style={{alignContent: 'center', alignItems: 'center'}}> 
                <Image source={require('../images/png/bv_logo.png')} style={{width: 100,height: 100,resizeMode: 'contain'}} />
                <Text style={styles.textBig}>Buvea Sea</Text>
              </View>
              
              <LightBlueButton text={LocalizedStrings.sign_up} onPress={() => openRegister()} />
                <View style={{alignItems: 'center', flexDirection: 'column', margin: 20}}>
                    <Text style={{color: WHITE, fontFamily: 'Montserrat-Medium'}}>{LocalizedStrings.already_have_account}</Text> 
                </View>
                <DarkBlueButton text={LocalizedStrings.sign_in} onPress={() => openLogin()} />
            </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        // padding: 20,
        justifyContent: 'center',
        backgroundColor: 'blue'
    },
    viewInner: {
        flex: 1,
        padding: 40,
        justifyContent: 'center',
    },
    text: {
        color: '#24B3E3',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    bottomSheet: {
        width: '50%',
        flexDirection: 'column',
        margin: 20
    },
    textLang: {
        fontSize: 15,
        fontWeight: '600',
        color: WHITE,
        textAlign: 'center',
        marginStart: 0,
        minWidth: 80
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    textBig: {
      color: WHITE,
      fontWeight: '700',
      fontSize: 25,
      marginBottom: 30,
      fontFamily: 'Montserrat-Bold'
  },
});


const mapStateToProps = (state) => {
    return {
        // result: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // login: (email,password) => dispatch(login(email,password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)