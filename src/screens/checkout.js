import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, ScrollView, Alert, Platform } from 'react-native';
import OrangeButton from '../components/OrangeButton';
import {connect} from 'react-redux';
import { BLUE, DARKGREY, GREY, WHITE } from '../utilities/colors';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import {paymentOffer} from '../redux/actions/paymentOfferAction';
import { paymentRequest } from '../redux/actions/paymentRequestAction';
import TextFieldFlat from '../components/TextFieldFlat'
import RadioImages from '../components/RadioImages';
import { PAYMENT_CONFIRM_NAME, PAYMENT_SCREEN_NAME } from '../utilities/screenNames';
import { BASE_API_URL } from '../utilities/constants';
import { ABA_PAY_URL, WING_PAY_URL } from '../utilities/api';


function index({navigation, route, paymentOffer,paymentRequest, response}) {

  const params = route.params.payload[0];
  const phone = route.params.phone;
  const [payment,setPayment] = useState(0);
  const [paymentMethod, setPaymentMethod] = useState("");
  const [couponCodeEntered, setCouponCodeEntered] = useState("");
  const [rewardPointsPayment, setRewardPointsPayment] = useState(0);
  const [ticketPrice,setTicketPrice] = useState((params.final_amount).toFixed(2));
  const [mainPrice,setMainPrice] = useState((0).toFixed(2));
  const [serviceCharge,setServiceCharge] = useState((0).toFixed(2));
  const [discount,setDiscount] = useState((0).toFixed(2));
  const [couponCodePrice,setCouponCodePrice] = useState((0).toFixed(2));
  const [addressVisible,setAddressVisible] = useState(false);
  const [address, setAddress] = useState("");
  const [isAndroid, setIsAndroid] = useState(false);
  const [loading,setLoading] = useState(false);

  useEffect(() => {
      navigation.setOptions({
        title: "Checkout",
        headerStyle: {
          backgroundColor: BLUE,
        },
        headerTintColor: WHITE,
        headerTitleStyle: {
          fontFamily: 'Montserrat-Bold'
        },
      })
  },[])

  const paymentList = [
    {id:1,text:'Payway',image_url: require('../images/png/credit_card.png'), width: 70},
    {id:2,text:'Pipay',image_url: require('../images/png/pi_pay.png'), width: 70},
    {id:3,text:'Wing',image_url: require('../images/png/wing_pay.png'), width: 70},
    {id:4,text:'Abapay',image_url: require('../images/png/aba_pay.png'), width: 70},
    {id:5,text:'Cod',image_url: require('../images/png/cod.png'), width: 70}
  ];

  const radioSelected = (id,text) => {
    setPayment(id);
    setPaymentMethod(text);
    getCalculations(text);
    if(id == 5)
      setAddressVisible(true);
    else
      setAddressVisible(false);
  };

  const makePayment = () => {
    if(payment != 0) {
      if(payment == 5) {
        if (address.length > 0) {
          var paymentObj = {};
          paymentObj.address = address;
          paymentObj.contact_phone =phone;
          paymentObj.appCod = 0;
          paymentObj.booking_id = params.booking_id;
          paymentObj.coupon_code = couponCodeEntered;
          paymentObj.currency = "USD";
          paymentObj.donation_amount = 0;
          paymentObj.experiences = [];
          paymentObj.insurance = 0;
          paymentObj.insuranceAmount  = 0;
          paymentObj.insuranceType = 0;
          paymentObj.payment_method = paymentMethod.toLowerCase();
          paymentObj.reward_points = rewardPointsPayment;
          paymentObj.reward_points_type = "user";
          paymentObj.total_amount = ticketPrice;
          paymentObj.affiliate_id = null;
          paymentRequest(paymentObj);
          setLoading(true);
        } else {
          Alert.alert(  
            "",
            "Fill address for cash on delivery",
            [
                {
                    text: "Ok",
                    style: "cancel",
                }
            ],
            {
                cancelable: true
            }
          );
        }
      } else {
        var paymentObj = {};
        paymentObj.address = address;
        paymentObj.contact_phone =phone;
        paymentObj.appCod = 0;
        paymentObj.booking_id = params.booking_id;
        paymentObj.coupon_code = couponCodeEntered;
        paymentObj.currency = "USD";
        paymentObj.donation_amount = 0;
        paymentObj.experiences = [];
        paymentObj.insurance = 0;
        paymentObj.insuranceAmount  = 0;
        paymentObj.insuranceType = 0;
        paymentObj.payment_method = paymentMethod.toLowerCase();
        paymentObj.reward_points = rewardPointsPayment;
        paymentObj.reward_points_type = "user";
        paymentObj.total_amount = ticketPrice;
        paymentObj.affiliate_id = null;
        paymentRequest(paymentObj);
        setLoading(true);
      }
    } else {
      Alert.alert(  
        "",
        "Select a payment method",
        [
            {
                text: "Ok",
                style: "cancel",
            }
        ],
        {
            cancelable: true
        }
      );
    }
  };

  const getCalculations = (text) => {
    var paymentObj = {};
    paymentObj.payment_method_name = text;
    paymentObj.coupon_code = couponCodeEntered;
    paymentObj.reward_requested = rewardPointsPayment;
    paymentObj.booking_id = params.booking_id;
    paymentOffer(paymentObj);
    setLoading(true);
  };

  useEffect(() => {
    if (payment != 0 && response.paymentDetails.payload.status == "success") {
      setCouponCodePrice((response.paymentDetails.payload.result.coupon_discount).toFixed(2));
      setServiceCharge((response.paymentDetails.payload.result.service_fee).toFixed(2));
      setDiscount((response.paymentDetails.payload.result.discount_allowed).toFixed(2));
      setMainPrice(((Number(ticketPrice)+Number(response.paymentDetails.payload.result.service_fee))-(Number(response.paymentDetails.payload.result.coupon_discount)+Number(response.paymentDetails.payload.result.discount_allowed))).toFixed(2));
      setLoading(false);
    }
  },[response.paymentDetails])

  useEffect(() => {
    if(response.paymentRequest.payload) {
      if(response.paymentRequest.payload.status == "success") {
        setLoading(false);
        result = response.paymentRequest.payload.result[0];
        if(payment == 1) {
          var paymentObj = {};
          paymentObj.payment_id = result.payment_id;
          paymentObj.url = ABA_PAY_URL;
          paymentObj.postData = "url=" + encodeURI(result.payment_request.url)
            + "&amount=" + encodeURI(result.payment_request.payload.amount)
            + "&firstname=" + encodeURI(result.payment_request.payload.firstname)
            + "&lastname=" + encodeURI(result.payment_request.payload.lastname)
            + "&phone_country_code=" + encodeURI("+855")
            + "&phone=" + encodeURI("9717934787")
            + "&email=" + encodeURI(result.payment_request.payload.email)
            + "&hash=" + result.payment_request.payload.hash
            + "&payment_option=" + encodeURI(result.payment_request.payload.payment_option)
            + "&tran_id=" + encodeURI(result.payment_request.payload.tran_id)
          navigation.navigate(PAYMENT_SCREEN_NAME,paymentObj);
        } else if(payment == 2) {
          var paymentObj = {};
          paymentObj.payment_id = result.payment_id;
          paymentObj.url = result.payment_request.url;
          paymentObj.postData = "mid=" +encodeURI(result.payment_request.payload.mid)
                      + "&sid="+ encodeURI(result.payment_request.payload.sid)
                      + "&did=" + encodeURI(result.payment_request.payload.did)
                      + "&orderid=" + encodeURI(result.payment_request.payload.orderid)
                      + "&orderAmount=" + encodeURI(result.payment_request.payload.orderAmount)
                      + "&currency=" + encodeURI(result.payment_request.payload.currency)
                      + "&orderDesc=" + encodeURI(result.payment_request.payload.orderDesc)
                      + "&orderDate=" + encodeURI(result.payment_request.payload.orderDate)
                      + "&lang=" + encodeURI(result.payment_request.payload.lang)
                      + "&confirmURL=" + encodeURI(result.payment_request.payload.confirmURL)
                      + "&cancelURL=" + encodeURI(result.payment_request.payload.cancelURL)
                      + "&trType=" + encodeURI(result.payment_request.payload.trType)
                      + "&payerEmail=" + encodeURI(result.payment_request.payload.payerEmail)
                      + "&var1=" + encodeURI(result.payment_request.payload.var1)
                      + "&digest=" + encodeURI(result.payment_request.payload.digest);
          if (!result.payment_request.headers[0].key && result.payment_request.headers[0].value) {
            paymentObj.key = result.payment_request.headers[0].key;
            paymentObj.value = result.payment_request.headers[0].value;
          }
          navigation.navigate(PAYMENT_SCREEN_NAME,paymentObj);
        } else if(payment == 3) {
          var paymentObj = {};
          paymentObj.payment_id = result.payment_id;
          paymentObj.url = WING_PAY_URL;
          paymentObj.postData = "url=" + encodeURI(result.payment_request.url)
            + "&username=" + encodeURI(result.payment_request.payload.username)
            + "&rest_api_key=" + encodeURI(result.payment_request.payload.rest_api_key)
            + "&ask_remember=" + encodeURI(result.payment_request.payload.ask_remember)
            + "&return_url=" + encodeURI(result.payment_request.payload.return_url)
            + "&bill_till_rbtn=" + encodeURI(result.payment_request.payload.bill_till_rbtn)
            + "&bill_till_number=" + encodeURI(result.payment_request.payload.bill_till_number)
            + "&amount=" + encodeURI(result.payment_request.payload.amount)
            + "&currency=" + encodeURI(result.payment_request.payload.currency)
            + "&sandbox=" + encodeURI(result.payment_request.payload.sandbox)
            + "&remark=" + encodeURI(result.payment_request.payload.remark);
            navigation.navigate(PAYMENT_SCREEN_NAME,paymentObj);
        } else if(payment == 4) {
          var paymentObj = {};
          paymentObj.payment_id = result.payment_id;
          paymentObj.url = ABA_PAY_URL;
          paymentObj.postData = "url=" + encodeURI(result.payment_request.url)
            + "&amount=" + encodeURI(result.payment_request.payload.amount)
            + "&firstname=" + encodeURI(result.payment_request.payload.firstname)
            + "&lastname=" + encodeURI(result.payment_request.payload.lastname)
            + "&phone_country_code=" + encodeURI("+855")
            + "&phone=" + encodeURI("9717934787")
            + "&email=" + encodeURI(result.payment_request.payload.email)
            + "&hash=" + result.payment_request.payload.hash
            + "&payment_option=" + encodeURI(result.payment_request.payload.payment_option)
            + "&tran_id=" + encodeURI(result.payment_request.payload.tran_id)
          navigation.navigate(PAYMENT_SCREEN_NAME,paymentObj);
        } else if(payment == 5){
          navigation.navigate(PAYMENT_CONFIRM_NAME);
        }
      }
    }
  },[response.paymentRequest]);
  
  return (
    <View style={styles.mainView}>
      
      <Spinner
        visible={loading}
        textContent={'Loading...'}
        textStyle={styles.spinnerTextStyle}
      />

      { isAndroid &&
        <View style={{
          backgroundColor: BLUE,
          flexDirection: 'row',
          paddingTop: 5,
          paddingStart: 5,
          paddingBottom: 5,
          alignItems: 'center'
        }}>
            <TouchableOpacity onPress={navigation.goBack}>
              <View style={{ width: 40 }}>
                <MaterialCommunityIcons name="chevron-left" color={WHITE} size={40} />
              </View>
            </TouchableOpacity>
            <View style={{flex: 1}}>
              <Text style={{
                color: WHITE, 
                fontFamily: 'Montserrat-Bold',
                alignSelf: 'center',
                fontSize: 15}}>
                Checkout
              </Text>
            </View>
            <View style={{ width: 40 }}></View>
        </View>
      }
      
      <ScrollView style={styles.viewData}>

      <View style={{marginStart: 20,marginEnd: 20, marginBottom: 10, marginTop: 10}}>
        <TextFieldFlat
          label="Have Coupon Code?"
          value={couponCodeEntered}
          onChangeText={setCouponCodeEntered}
        />
      </View>

      <View style={styles.paymentView}>
        <View style={styles.paymentText}>
          <Text style={styles.paymentHeader}>Payment Method</Text>
        </View>
        <View style={styles.paymentSelecter}>
          <RadioImages
            data={paymentList}
            value={payment}
            onPress={(id,text) => radioSelected(id,text)}>
          </RadioImages>
        </View>
      </View>


    { addressVisible && 
      <View style={{marginStart: 20, marginEnd: 20, marginTop: 20}}>
        <Text style={{color: 'green', marginStart: 10}}>Address valid for Phnom Penh only.</Text>
        <TextFieldFlat
          label="Cash pickup address"
          value={address}
          onChangeText={setAddress} />
      </View>
    }
      

      <View style={styles.calculationsView}>
        <View style={styles.pricesContainer}>
          <Text style={styles.whiteTextLeft}>Ticket (x2)</Text>
          <Text style={styles.whiteTextRight}>$ {ticketPrice}</Text>
        </View>
        <View style={styles.pricesContainer}>
          <Text style={styles.whiteTextLeft}>Service Charge</Text>
          <Text style={styles.whiteTextRight}>$ {serviceCharge}</Text>
        </View>
        <View style={styles.pricesContainer}>
          <Text style={styles.whiteTextLeft}>Discount</Text>
          <Text style={styles.whiteTextRight}>$ {discount}</Text>
        </View>
        <View style={styles.pricesContainer}>
          <Text style={styles.whiteTextLeft}>Coupon code</Text>
          <Text style={styles.whiteTextRight}>$ {couponCodePrice}</Text>
        </View>
      </View>

      <View style={styles.tncView}>
        <Text style={{color: DARKGREY, textAlign: 'center', fontFamily: 'Montserrat-Medium'}}>By proceeding, I have read and accepted the</Text>
        <Text style={{color: BLUE, textAlign: 'center', fontFamily: 'Montserrat-Medium'}}>Terms and conditions and Privacy Policy</Text>
      </View>

      </ScrollView>

      <View style={styles.bottomButtons}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.blueTextLowerLeft}>Total</Text>
          <Text style={styles.blueTextLowerRight}>$ {mainPrice}</Text>
        </View>
        <OrangeButton text='Confirm' onPress={() => {makePayment()}} />
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
    height: '100%',
    width: '100%'
  },
  viewData: {
    marginBottom: 10,
    flex: 1,
  },
  paymentView: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GREY,
    padding: 20,
    borderRadius: 10,
    marginStart: 20,
    marginEnd: 20
  },
  paymentText: {
    // flexDirection: 'row'
  },
  paymentHeader: {
    marginTop: -30,
    backgroundColor: WHITE,
    padding: 3,
    color: BLUE,
    fontFamily: 'Montserrat-Medium'
  },
  calculationsView: {
    backgroundColor: BLUE,
    marginTop: 20,
    padding: 20,
    minHeight: 150
  },
  paymentSelecter: {
    flexDirection: 'row'
  },
  pricesContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  whiteTextLeft: {
    color: WHITE,
    flex: 1,
    textAlign: 'left',
    fontFamily: 'Montserrat-Medium'
  },
  whiteTextRight: {
    color: WHITE,
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Montserrat-Medium'
  },
  tncView: {
    backgroundColor: GREY,
    minHeight: 50,
    padding: 10,
    flexDirection: 'column',
    textAlign: 'center'
  },
  bottomButtons: {
    padding: 20,
    justifyContent: 'space-between',
  },
  blueTextLowerLeft: {
    flex: 1,
    fontSize: 18,
    // fontWeight: '800',
    color: BLUE,
    paddingStart: 5,
    paddingBottom: 10,
    textAlign: 'left',
    fontFamily: 'Montserrat-Bold'
  },
  blueTextLowerRight: {
    flex: 1,
    fontSize: 18,
    color: BLUE,
    paddingEnd: 5,
    paddingBottom: 10,
    textAlign: 'right',
    fontFamily: 'Montserrat-Bold'
  },
});

const mapStateToProps = (state) => {
  return {
      response: state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paymentOffer: (data) => dispatch(paymentOffer(data)),
    paymentRequest: (data) => dispatch(paymentRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)