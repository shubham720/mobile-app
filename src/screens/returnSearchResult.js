import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image, Platform } from 'react-native';
import {connect} from 'react-redux';
import {searchBus} from '../redux/actions/searchBusAction';
import {RETURN_SEAT_LAYOUT_NAME, SEAT_LAYOUT_SCREEN_NAME} from '../utilities/screenNames';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import { BACKGROUND, BLUE, GREY, ORANGE, WHITE } from '../utilities/colors';
import LocalizedStrings from '../utilities/localizationSettings';

function index({response,cityTo, navigation, route, searchBus}) {

  const {
    from_id,
    from_text,
    to_id,
    to_text,
    date,
    return_date,
    return_enabled,
    route_schedule_id,
    seats,
    stop_to_id,
    stop_from_id,
    vehicle,
    departure_date,
    departure_time,
    price_min,
    price_max
   } = route.params;

   console.log("Return search result", route.params);

  var data = [];
  const [toId, setToId] = useState(0);
  const [toText,setToText] = useState("");
  const [dates, setDates] = useState([]);
  const [isAndroid, setIsAndroid] = useState(false);

  useEffect(() => {
    searchBus(return_date,to_id,from_id);
  },[])

  useEffect(() => {
      navigation.setOptions({
        title: to_text+" - "+from_text
      })
  },[])

  useEffect(() => {
    for (var i=0; i<=10; i++) {
      var obj = {};
      obj.date = moment(return_date, "YYYY-MM-DD").add(i, 'days');
      if(i == 0) {
        obj.isSelected = true;
      } else {
        obj.isSelected = false;
      }
      obj.index = i;
      dates.push(obj);
    }
  },[]);

  if(response.searchResults.error) {
    console.log("here is error", response.searchResults.error);
  } else if(response.searchResults.searchResults){
      data = response.searchResults.searchResults.result;
  }
    
  const citySelected = (itemCurrent) => {
    var stop_to = null,stop_from = null;
    for (const item of itemCurrent.stops) {
      if(item.disembarking_allowed && item.disembarking_option) {
        stop_to = item.id;
      }
      if(item.boarding_allowed && item.boarding_option){
        stop_from = item.id;
      }
    }
    if(stop_to && stop_from){
      navigation.navigate(RETURN_SEAT_LAYOUT_NAME, {
        date: date,
        route_schedule_id: route_schedule_id,
        stop_from: stop_from_id,
        stop_to: stop_to_id,
        from_id: from_id,
        from_text: from_text,
        to_id: to_id,
        to_text: to_text,
        seats: seats,
        vehicle: vehicle,
        departure_date: departure_date,
        departure_time: departure_time,
        price_min: price_min,
        price_max: price_max,
        return_route_schedule_id: itemCurrent.id,
        return_stop_from: stop_from,
        return_stop_to: stop_to,
        return_from_id: from_id,
        return_from_text: from_text,
        return_to_id: to_id,
        return_to_text: to_text,
        return_vehicle: itemCurrent.vehicle.subtype,
        return_departure_date: itemCurrent.departure_datetime,
        return_departure_time: itemCurrent.departure_datetime,
        return_price_min: itemCurrent.seat_price_min,
        return_price_max: itemCurrent.seat_price_max,
        return_enabled: return_enabled,
        return_date: return_date
      })
    }
  };

  const getNewData = (date) => {
    searchBus(date,from_id,to_id);
  }

  const changeSelcted = (item) => {
    for (const itemOne of dates) {
      if(itemOne.index == item.index){
        itemOne.isSelected = true;
      } else {
        itemOne.isSelected = false;
      }
    }
  };

  const renderItemDate = (item) => {
    if(item.isSelected){
      return <TouchableOpacity onPress = {() => {
        getNewData(moment(item.date).format("yyyy-MM-DD"));
        changeSelcted(item);
        }}>
      <View style={styles.datesCardSelected}>
        <Text style={styles.dateTextSelected}>{moment(item.date).format("DD MMMM")}</Text>
      </View>
    </TouchableOpacity>;
    } else {
      return <TouchableOpacity onPress = {() => {
        getNewData(moment(item.date).format("yyyy-MM-DD"));
        changeSelcted(item);
        }}>
      <View style={styles.datesCard}>
        <Text style={styles.dateText}>{moment(item.date).format("DD MMMM")}</Text>
      </View>
    </TouchableOpacity>;
    }
  }

  return (
    <View style={styles.view}>
      {isAndroid &&
        <View style={{
          backgroundColor: BLUE,
          flexDirection: 'row',
          paddingTop: 5,
          paddingStart: 5,
          paddingBottom: 5,
          alignItems: 'center'
        }}>
            <TouchableOpacity onPress={navigation.goBack}>
              <View style={{ width: 40 }}>
                <MaterialCommunityIcons name="chevron-left" color={WHITE} size={40} />
              </View>
            </TouchableOpacity>
            <View style={{flex: 1}}>
              <Text style={{
                color: WHITE, 
                fontFamily: 'Montserrat-Bold',
                alignSelf: 'center',
                fontSize: 15}}>
                {to_text+" - "+from_text}
              </Text>
            </View>
            <View style={{ width: 40 }}></View>
        </View>
      }

      <Spinner
          visible={response.searchResults.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.viewList}>
        
        <View>
          {dates &&
            <FlatList
              data={dates}
              horizontal
              keyExtractor={(item) => item.date}
              renderItem = {({item}) => renderItemDate(item) }/>
          }
        </View>
      
        {data && data.length > 0 && 
          <FlatList
          data={data}
          renderItem = {({item}) => (
            <TouchableOpacity onPress={() => citySelected(item)}>
              <View style={styles.textRecycler}>

                <View style={styles.textRow}>
                  <View style={styles.leftView}>
                    <Text style={styles.textSmallGrey}>Departure</Text>
                  </View>
                  <View style={styles.rightView}>
                    <Text style={styles.textSmallGrey}>Arrival</Text>
                  </View>
                </View>

                <View style={styles.textRow}>
                  <View style={styles.firstOfThree}>
                    <Text style={styles.blueTextTime}>{moment(item.departure_datetime).format('HH:mm')}</Text>
                  </View>
                  <View style={styles.secondOfThree}>
                    <Text style={styles.textSmallGrey}>{item.duration}</Text>
                  </View>
                  <View style={styles.thirdOfThree}>
                    <Text style={styles.blueTextTime}>{moment(item.arrival_datetime).format('HH:mm')}</Text>
                  </View>
                </View>

                <View style={styles.textRowLower}>
                  <View style={styles.leftView}>
                    <Text style={styles.orangeTextTime}>Boarding Arrival</Text>
                  </View>
                  <View style={styles.rightView}>
                    
                  </View>
                </View>

                <View style={styles.textRowLower}>
                  <View style={styles.leftGreyBack}>
                    <Image
                      source={require('../images/png/bus.png')}
                    />
                    <Text style={styles.blueVehicleType}>{item.vehicle.subtype}</Text>
                  </View>
                  <View style={styles.rightOrangeBack}>
                  <Text style={styles.whitePriceText}>
                    {item.currency}{" "+(item.seat_price_min).toFixed(2)}
                    {" - "+(item.seat_price_max).toFixed(2)}
                    </Text>
                  </View>
                </View>

              </View>
              
            </TouchableOpacity>
          )}
        />
      }
      { !data || data.length == 0 && 
        <Text>
          No result found
        </Text>
      }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
      backgroundColor: '#C9CEE7',
      height: '100%',
      width: '100%'
  },
  viewList: {
    padding: 20,
    marginBottom: 10,
    flex: 1,
    flexDirection: 'column'
  },
  textRecycler: {
    padding: 10,
    marginBottom: 15,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 10,
    borderColor: 'white',
    flexDirection: 'column',
    flex: 1
  },
  button: {
    marginTop: 20
  },
  textSmallGrey: {
    fontSize: 12,
    color: 'grey',
    fontFamily: 'Montserrat-Medium'
  },
  textRow: {
    flexDirection: 'row',
    flex: 1
  },
  rightView: {
    alignItems: 'flex-end',
    flex: 1
  },
  leftView: {
    alignItems: 'flex-start',
    flex: 1,
  },
  firstOfThree: {
    alignItems: 'flex-start',
    flex: 1
  },
  secondOfThree: {
    alignItems: 'center',
    flex: 1
  },
  thirdOfThree: {
    alignItems: 'flex-end',
    flex: 1
  },
  blueTextTime: {
    fontSize: 18,
    fontWeight: '700',
    color: '#1D2489',
    fontFamily: 'Montserrat-Medium'
  },
  orangeTextTime: {
    fontSize: 12,
    fontWeight: '700',
    color: '#FF5700',
    fontFamily: 'Montserrat-Medium'
  },
  textRowLower: {
    flexDirection: 'row',
    flex: 1,
    marginTop: 15
  },
  leftGreyBack: {
    backgroundColor: GREY,
    flex:1,
    flexDirection: 'row',
    padding: 10,
    borderTopStartRadius: 10,
    borderBottomStartRadius: 10,
    alignItems: 'center',
  },
  rightOrangeBack: {
    backgroundColor: ORANGE,
    flex:2,
    padding: 10,
    borderTopStartRadius: 10,
    borderBottomStartRadius: 10,
    marginStart: -11,
    marginEnd: -11,
    justifyContent: 'center',
  },
  blueVehicleType: {
    fontSize: 18,
    fontWeight: '800',
    color: BLUE,
    paddingStart: 5,
    fontFamily: 'Montserrat-Medium'
  },
  whitePriceText: {
    fontSize: 18,
    fontWeight: '800',
    color: WHITE,
    paddingStart: 5,
    fontFamily: 'Montserrat-Medium'
  },
  datesCard: {
    padding: 10,
    borderWidth: 1,
    backgroundColor: BLUE,
    borderRadius: 10,
    borderColor: BLUE,
    flexDirection: 'column',
    color: WHITE,
    marginTop: 5,
    marginEnd: 5,
    marginStart: 5,
    marginBottom: 20
  },
  dateText: {
    color: WHITE,
    fontFamily: 'Montserrat-Medium'
  },
  datesCardSelected: {
    padding: 10,
    borderWidth: 1,
    backgroundColor: WHITE,
    borderRadius: 10,
    borderColor: WHITE,
    flexDirection: 'column',
    color: BLUE,
    marginTop: 5,
    marginEnd: 5,
    marginStart: 5,
    marginBottom: 20
  },
  dateTextSelected: {
    color: BLUE,
    fontFamily: 'Montserrat-Medium'
  }
})

const mapStateToProps = state => {
  return {
    response: state
  }
}
const mapDispatchToProps = dispatch => {
  return {
    searchBus: (date,from_id,to_id) => dispatch(searchBus(date,from_id,to_id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)