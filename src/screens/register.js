import React, { useState,useEffect } from 'react';
import { ImageBackground, SafeAreaView, ScrollView, StyleSheet, Text, View, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import LightBlueButton from '../components/LightBlueButton';
import TextFieldFlat from '../components/TextFieldFlat';
import {register} from '../redux/actions/registerAction';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import {HOME_SCREEN_NAME, LOGIN_SCREEN_NAME} from '../utilities/screenNames';
import {USER_ID} from '../utilities/constants';
import { BLUE, GREY, ORANGE, WHITE } from '../utilities/colors';
import SearchableDropdown from 'react-native-searchable-dropdown';
import { COUNTRYCODES } from '../utilities/constants';
import OrangeButton from '../components/OrangeButton';
import DropDownPicker from 'react-native-dropdown-picker';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MaterialBottomTabView } from '@react-navigation/material-bottom-tabs';
import { TouchableOpacity } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LocalizedStrings from '../utilities/localizationSettings';

function index({navigation, route, register, result}) {

    useEffect(() => {
        var main = COUNTRYCODES;
        var child = COUNTRYCODES[0].children;
        var i = 0;
        for (const item of child) {
            main[0].children[i].id = i;
            i++;
        }
        setItems(main);
    },[]);

    const [firstname,setFristname] = useState("");
    const [lastname,setLastname] = useState("");
    const [country,setCountry] = useState("");
    const [phone,setPhone] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [confPass, setConfPass] = useState("");
    const [index,setIndex] = useState(0);
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items,setItems] = useState([]);
    const [selectedItems,setSelectedItems] = useState([]);

    const onSelectedItemsChange = (item) => {
        setSelectedItems(item);
        setCountry(items[0].children[item]);
    };

    const doRegister = () =>
    {   
        if (firstname && lastname && email && password && country && phone && confPass) {
            if(password == confPass) {
                AsyncStorage.clear();
                console.log({
                    first_name: firstname,
                    last_name: lastname,
                    email: email,
                    password: password,
                    password_confirmation: password,
                    registration_operator: 0,
                    active: 0,
                    mobile_number: phone,
                    user_role_id: 6,
                    country_code: country.dial_code,
                    otp_type: "Register",
                    country_c: {
                        name: country.name,
                        dial_code: country.dial_code,
                        code: country.code
                    }
                });
                register({
                    first_name: firstname,
                    last_name: lastname,
                    email: email,
                    password: password,
                    password_confirmation: password,
                    registration_operator: 0,
                    active: 0,
                    mobile_number: phone,
                    user_role_id: 6,
                    country_code: country.dial_code,
                    otp_type: "Register",
                    country_c: {
                        name: country.name,
                        dial_code: country.dial_code,
                        code: country.code
                    }
                });
            } else {
                Alert.alert(
                    "Invalid input",
                    "Passwords not matching",
                    [
                        {
                            text: "Ok",
                            style: "cancel",
                        }
                    ],
                    {
                        cancelable: true
                    }
                );
            }
        } else {
            Alert.alert(
                "Invalid input",
                "Please fill all the inputs and select country",
                [
                    {
                        text: "Ok",
                        style: "cancel",
                    }
                ],
                {
                    cancelable: true
                }
            );
        }
    };
    
    useEffect(() => {
        if(result.success) {
            navigation.reset({
                index: 0,
                routes: [{name: LOGIN_SCREEN_NAME}],
              });
        }
    },[result]);

    return (
        <View style={styles.view}>
            {/* <ImageBackground source={require('../images/png/backimage.png')} style={styles.backgroundImage}> */}
                <TouchableOpacity onPress={() => navigation.goBack() }>
                    <MaterialCommunityIcons name="chevron-down" color={WHITE} size={30} style={{padding: 20}} />
                </TouchableOpacity>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <ScrollView style={{paddingBottom: 400}}>
                    <View style={styles.viewInner}>
                        
                    <Text style={styles.loginText}>{LocalizedStrings.sign_up}</Text>
                    <TextFieldFlat
                        label={LocalizedStrings.first_name}
                        placeholder={LocalizedStrings.first_name}
                        onChangeText={(text) => setFristname(text)}
                        defaultValue={firstname}
                    />
                    <TextFieldFlat
                        label={LocalizedStrings.last_name}
                        placeholder={LocalizedStrings.last_name}
                        onChangeText={(text) => setLastname(text)}
                        defaultValue={lastname}
                    />
                    <TextFieldFlat
                        label="Email"
                        placeholder="abc@example.com"
                        onChangeText={(text) => setEmail(text)}
                        defaultValue={email}
                    />
                    
                    <View style={styles.inputContainer}>
                        <Text style={styles.inputLabel}>Country code</Text>
                        <View style={styles.row}>
                            <View style={{ flex: 1}}>
                                <SectionedMultiSelect
                                    items={items}
                                    IconRenderer={Icon}
                                    single={true}
                                    uniqueKey="id"
                                    subKey="children"
                                    selectText="Choose Country"
                                    showDropDowns={false}
                                    readOnlyHeadings={true}
                                    onSelectedItemsChange={onSelectedItemsChange}
                                    selectedItems={selectedItems}
                                    style={{ color: GREY}}
                                />
                            </View>
                        </View>
                    </View>

                    <TextFieldFlat
                        label={LocalizedStrings.phone_number}
                        placeholder="000000000"
                        onChangeText={(text) => setPhone(text)}
                        defaultValue={phone}
                        keyboardType='number-pad'
                    />
                    <TextFieldFlat
                        label="Password"
                        placeholder="********"
                        onChangeText={(text) => setPassword(text)}
                        defaultValue={password}
                        secureTextEntry={true}
                    />
                    <TextFieldFlat
                        label="Password"
                        placeholder="********"
                        onChangeText={(text) => setConfPass(text)}
                        defaultValue={confPass}
                        secureTextEntry={true}
                    />
                    <OrangeButton text='Register' onPress={() => {doRegister()}} />
                </View>
                </ScrollView>
            {/* </ImageBackground> */}
            </TouchableWithoutFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: ORANGE
    },
    viewInner: {
        backgroundColor: WHITE,
        borderTopEndRadius: 50,
        borderTopStartRadius: 50,
        flex: 1,
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 40,
        marginTop: 0,
        paddingBottom: 350
    },
    text: {
        color: '#24B3E3',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        fontFamily: 'Montserrat-Medium'
    },
    bottomSheet: {
        flex: 1,
        width: '100%'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    loginText: {
        alignSelf: 'center',
        marginBottom: 30,
        fontSize: 29,
        color: ORANGE,
        fontWeight: '700',
        fontFamily: 'Montserrat-Medium'
    },
    inputContainer: {
        backgroundColor: '#f4f6f8',
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderRadius: 10,
        marginVertical: 5,
        marginBottom:10
      },
      row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
      inputLabel: {
        fontSize: 12,
        color: 'grey',
      },
});


const mapStateToProps = (state) => {
    return {
        result: state.registerReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        register: (data) => dispatch(register(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)