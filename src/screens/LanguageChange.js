import React, { useState,useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, ImageBackground } from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import LightBlueButton from '../components/LightBlueButton';
import TextFieldFlat from '../components/TextFieldFlat';
import {login} from '../redux/actions/loginAction';
import moment from 'moment';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import {HOME_SCREEN_NAME, LOGIN_REGISTER_SCREEN_NAME, LOGIN_SCREEN_NAME} from '../utilities/screenNames';
import {USER_ID, USER_LANGUAGE} from '../utilities/constants';
import stringsOfLanguages from '../utilities/localizationSettings';
import { WHITE } from '../utilities/colors';

function index({navigation, route, login, result}) {

    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");

    const retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem(USER_ID);
          if (value !== null) {
            navigation.reset({
              index: 0,
              routes: [{name: HOME_SCREEN_NAME}],
            });
          } else {
            navigation.reset({
                index: 0,
                routes: [{name: LOGIN_REGISTER_SCREEN_NAME}],
              });
          }
        } catch (error) {
          console.log(error);
        }
    };

    const changeLanguage = (text) =>
    {
      AsyncStorage.setItem(USER_LANGUAGE,text);
      stringsOfLanguages.setLanguage(text);
      retrieveData();
    };

    return (
        <View style={styles.view}>
            <ImageBackground source={require('../images/png/backpng.png')} style={styles.backgroundImage}>
            <View style={styles.viewInner}>
                <View style={{alignItems: 'center', flexDirection: 'column'}}>
                    <Text style={{color: WHITE, fontSize: 20, fontWeight: '800', fontFamily: 'Montserrat-Medium'}}>ជ្រើសរើសភាសា</Text> 
                    <Text style={styles.textCenter}>Choose language</Text> 
                </View>
                <View style={styles.langHolder}>
                     <TouchableOpacity onPress={() => {changeLanguage("en")}}>
                        <View style={styles.bottomSheet}> 
                            <Image source={require('../images/png/en_flag.png')} />
                            <Text style={styles.textLang}>
                                English
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {changeLanguage("kh")}}>
                        <View style={styles.bottomSheet}>
                            <View>
                            <Image source={require('../images/png/kh_flag.png')} />
                            <Text style={styles.textLang}>
                                ចក្រភពខ្មែរ
                            </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                   
                </View>
            </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        // padding: 20,
        justifyContent: 'center',
        backgroundColor: 'blue'
    },
    viewInner: {
        flex: 1,
        padding: 20,
        justifyContent: 'center',
    },
    text: {
        color: '#24B3E3',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        fontFamily: 'Montserrat-Medium'
    },
    bottomSheet:{
        width: '50%',
        flexDirection: 'column',
        margin: 20
    },
    textLang: {
        fontSize: 15,
        fontWeight: '600',
        color: WHITE,
        textAlign: 'center',
        marginStart: 0,
        minWidth: 80,
        fontFamily: 'Montserrat-Medium'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    langHolder: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    textCenter:{
        fontFamily: 'Montserrat-Medium',
        color: WHITE
    }
});


const mapStateToProps = (state) => {
    return {
        // result: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // login: (email,password) => dispatch(login(email,password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)