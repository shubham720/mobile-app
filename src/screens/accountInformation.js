import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, Alert } from 'react-native';
import { BLUE, WHITE, GREY, BACKGROUND} from '../utilities/colors';
import TextFieldFlat from '../components/TextFieldFlat';
import { LOGIN_SCREEN_NAME } from '../utilities/screenNames';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import {accountUpdate} from '../redux/actions/accountUpdate';
import OrangeButton from '../components/OrangeButton';
import { USER_EMAIL, USER_FIRST_NAME, USER_LAST_NAME, USER_PHONE } from '../utilities/constants';

function index({navigation, response,accountUpdate}) {

  const retrieveData = async () => {
    try {
      const UserFirstName = await AsyncStorage.getItem(USER_FIRST_NAME);
      if (UserFirstName !== null) {
        setFirstName(UserFirstName);
      }
      const UserLastName = await AsyncStorage.getItem(USER_LAST_NAME);
      if (UserLastName !== null) {
        setLastName(UserLastName);
      }
      const UserEmail = await AsyncStorage.getItem(USER_EMAIL);
      if (UserEmail !== null) {
        setEmail(UserEmail);
      }
      const UserPhone = await AsyncStorage.getItem(USER_PHONE);
      if (UserPhone !== null) {
        setPhone(UserPhone);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    retrieveData();
  },[]);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email,setEmail] = useState("");
  const [phone,setPhone] = useState("");
  const [nationalityCount, setNationalityCount] = useState(0);
  const [nationality,setNationality] = useState("");

  const update = () => {
    if(firstName && lastName && email && phone && nationality) {
      var dataObj = {};
      accountUpdate(dataObj);
    } else {

    }
  }

  return (
    <View style={styles.mainView}>
      <Spinner
          visible={response.bookingDetail.loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.mainMenu}>
                <TextFieldFlat
                    label="First Name"
                    placeholder="Abc"
                    onChangeText={(text) => setFirstName(text)}
                    defaultValue={firstName}
                />
                <TextFieldFlat
                    label="Last Name"
                    placeholder="Abc"
                    onChangeText={(text) => setLastName(text)}
                    defaultValue={lastName}
                />
                <TextFieldFlat
                    label="Email"
                    placeholder="abc@example.com"
                    onChangeText={(text) => setEmail(text)}
                    defaultValue={email}
                />
                <TextFieldFlat
                    label="Phone"
                    placeholder="00000000"
                    onChangeText={(text) => setPhone(text)}
                    defaultValue={phone}
                />
      </View>
      <OrangeButton text='Update' onPress={() => {moveToTripPreview()}} style={styles.button}></OrangeButton>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: BACKGROUND,
    height: '100%',
    width: '100%',
    padding: 20
  },
  blueView: {
    backgroundColor: BLUE,
    height: 140,
    padding: 20
  },
  textUnderBlue: {
    fontSize: 25,
    fontWeight: '700',
    color: WHITE,
    paddingTop: 60
  },
  tabs: {
    backgroundColor: WHITE,
    borderBottomColor: GREY,
    borderTopColor: WHITE,
    borderStartColor: WHITE,
    borderEndColor: WHITE,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    color: BLUE,
    paddingBottom: 20,
    paddingEnd: 20,
    paddingTop: 20,
    paddingStart: 10,
    fontWeight: '600',
    fontSize: 18
  }
});

const mapStateToProps = state => {
  return {
      response: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    accountUpdate: (data) => dispatch(accountUpdate(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)
