import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, TextInput } from 'react-native';
import {connect} from 'react-redux';
import {cityFrom} from '../redux/actions/cityFromAction';
import LightBlueButton from '../components/LightBlueButton';
import Spinner from 'react-native-loading-spinner-overlay';
import { CITY_FROM_SCREEN_NAME, CITY_TO_SCREEN_NAME, HOME_SCREEN_NAME, MY_TRIPS_SCREEN_NAME, SEARCH_SCREEN_NAME } from '../utilities/screenNames';
import { BLUE, GREY, ORANGE, UNDERLINEGREY, WHITE } from '../utilities/colors';
import filter from 'lodash.filter';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { OPERATOR_ALL_ROUTES, OPERATOR_STOPS } from '../utilities/constants';
import OrangeButton from '../components/OrangeButton';
import DateTimePicker from "react-native-modal-datetime-picker";
import TextInputFlat from "../components/TextInputFlat";
import moment from 'moment';

function index({cityFrom, navigation, route}) {

  React.useEffect(() => {
    console.log(route.params);
    if (route.params?.fromIdSelected && route.params?.fromTextSelected) {
        const { fromIdSelected, fromTextSelected } = route.params;
        setFromId(fromIdSelected);
        setFromText(fromTextSelected);
    }
    if (route.params?.toIdSelected && route.params?.toTextSelected) {
        const { toIdSelected, toTextSelected } = route.params;
        setToId(toIdSelected);
        setToText(toTextSelected);
    }
},[route.params]);

  const [bookingId, setBookingId] = useState(null);
  const [email,setEmail] = useState(null);
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [type,setType] = useState(0);
  const [bookingDateFrom,setBookingDateFrom] = useState();
  const [bookingDateTo,setBookingDateTo] = useState();
  const [travelDateFrom,setTravelDateFrom] = useState();
  const [travelDateTo,setTravelDateTo] = useState();
  const [fromText,setFromText] = useState("Phnom Penh");
  const [fromId,setFromId] = useState(95);
  const [toText,setToText] = useState("Kampot");
  const [toId,setToId] = useState(62);

  const selectBookingFrom = () => {
    setIsDateTimePickerVisible(true);
    setType(1);
  }
  const selectBookingTo = () => {
    setIsDateTimePickerVisible(true);
    setType(2);
  }
  const selectTravelFrom = () => {
    setIsDateTimePickerVisible(true);
    setType(3);
  }
  const selectTravelTo = () => {
    setIsDateTimePickerVisible(true);
    setType(4);
  }

  const openFrom = () => {
    console.log("clockscnas");
    navigation.navigate(CITY_FROM_SCREEN_NAME,{
      page: 2
    });
  }

  const openTo = () => {
    navigation.navigate(CITY_TO_SCREEN_NAME,{
      from_id: fromId,
      page: 2
    });
  }

  const handleDatePicked = (date) => {
    if (type == 1) {
      setBookingDateFrom(moment(date).format("yyyy-MM-DD"));
    }
    if (type == 2) {
      setBookingDateTo(moment(date).format("yyyy-MM-DD"));
    }
    if(type == 3) {
      setTravelDateFrom(moment(date).format("yyyy-MM-DD"));
    }
    if (type == 4) {
      setTravelDateTo(moment(date).format("yyyy-MM-DD"));
    }
    hideDateTimePicker();
    setType(0);
  }
  const  hideDateTimePicker = (value) => {
    setIsDateTimePickerVisible(false);
  };


  const openBooking = () => {
    navigation.navigate(HOME_SCREEN_NAME,{
      screen: MY_TRIPS_SCREEN_NAME,
      params: {
        fromId: fromId,
        fromText: fromText,
        toId: toId,
        toText: toText,
        bookingId: bookingId,
        email: email,
        bookingDateFrom: bookingDateFrom,
        bookingDateTo: bookingDateTo,
        travelDateFrom: travelDateFrom,
        travelDateTo: travelDateTo
       },
      merge: true,
    });
  }


  return (
    <View style={styles.view}>
      <View style={styles.whiteBoard}>

        
        <View>
          <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Booking Id</Text>
          <TextInput 
          style={styles.inputView} 
          value={bookingId}
          placeholder="Booking ID"
          ></TextInput>
        </View>

        <View>
          <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Email</Text>
          <TextInput 
          style={styles.inputView} 
          value={email}
          placeholder="Email"
          ></TextInput>
        </View>

        <View style={{ flexDirection: 'row'}}>
          <View style={{ flex: 1}}>
            <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Location from</Text>
              <TextInputFlat 
              style={styles.inputView} 
              textMain={fromText}
              label="Select location"
              onPress={() =>  openFrom()}
              ></TextInputFlat>
          </View>
          <View style={{ flex: 1}}>
            <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Location to</Text>
            <TextInputFlat 
              style={styles.inputView} 
              textMain={toText}
              label="Select location"
              onPress={() =>  openTo()}
              ></TextInputFlat>
          </View>
        </View>

        <View style={{ flexDirection: 'row'}}>
          <View style={{ flex: 1}}>
            <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Booking date from</Text>
            <TextInputFlat 
              style={styles.inputView} 
              textMain={bookingDateFrom}
              label="Select Date"
              onPress={() =>  selectBookingFrom()}
              ></TextInputFlat>
          </View>
          <View style={{ flex: 1}}>
            <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Booking date to</Text>
            <TextInputFlat 
              style={styles.inputView} 
              textMain={bookingDateTo}
              label="Select Date"
              onPress={() =>  selectBookingTo()}
              ></TextInputFlat>
          </View>
        </View>

        <View style={{ flexDirection: 'row'}}>
          <View style={{ flex: 1}}>
            <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Travel date from</Text>
            <TextInputFlat 
              style={styles.inputView} 
              textMain={travelDateFrom}
              label="Select Date"
              onPress={() =>  selectTravelFrom()}
              ></TextInputFlat>
          </View>
          <View style={{ flex: 1}}>
            <Text style={{marginStart: 10, marginTop: 10, marginBottom: 5, fontFamily: 'Montserrat-Bold', color: ORANGE}}>Travel date to</Text>
            <TextInputFlat 
              style={styles.inputView}
              textMain={travelDateTo}
              label="Select Date"
              onPress={() =>  selectTravelTo()}
              ></TextInputFlat>
          </View>
        </View>
        
      </View>

      <DateTimePicker
        isVisible={isDateTimePickerVisible}
        onConfirm={(date) => {handleDatePicked(date)}}
        onCancel={() => {hideDateTimePicker()}}
      />

      <View style={{ marginTop: 20, marginStart: 10, marginEnd: 10}}>
        <OrangeButton text='Apply' onPress={() => openBooking()} style={styles.button}></OrangeButton>
      </View>
      
    </View>
  );

}

const styles = StyleSheet.create({
  view: {
      backgroundColor: BLUE,
      // padding: 20,
      height: '100%',
      width: '100%',
      backgroundColor: WHITE
  },
  viewList: {
    marginBottom: 10,
    flex: 1
  },
  textRecycler: {
    padding: 15,
    marginBottom: 2,
    borderWidth: 1,
    borderBottomColor: UNDERLINEGREY,
    borderColor: 'white',
    borderRadius: 0,
    flexDirection: 'row'
  },
  textRecyclerSelected: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderBottomColor: 'black',
    borderColor: WHITE,
    backgroundColor: ORANGE,
    borderRadius: 0,
  },
  button: {
    margin: 20
  },
  inputView: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GREY,
    padding: 10,
    borderRadius: 10,
    marginStart: 10,
    marginEnd: 10
  },
  mapMark: {
    borderWidth: 1,
    borderColor: UNDERLINEGREY,
    padding: 5,
    borderRadius: 10
  }
})

const mapStateToProps = state => {
  return {
      cityData: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    cityFrom: () => dispatch(cityFrom())
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)