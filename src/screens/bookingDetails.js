import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Alert, BackHandler } from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {connect} from 'react-redux';
import {bookingDetail} from '../redux/actions/bookingDetailsAction';
import { sendMailOrSMS } from '../redux/actions/sendMailAction';
import { bookingCancel } from '../redux/actions/bookingCancelAction';
import Spinner from 'react-native-loading-spinner-overlay';
import { BLUE, DARKGREY, ORANGE, WHITE } from '../utilities/colors';
import { USER_ID, USER_PHONE, USER_ROLE } from '../utilities/constants';
import moment from 'moment';
import { FloatingAction } from "react-native-floating-action";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { MODIFY_BOOKING_SCREEN } from '.';
import { HOME_SCREEN_NAME, MODIFY_BOOKING_SCREEN_NAME, MY_TRIPS_SCREEN_NAME } from '../utilities/screenNames';
import DialogInput from 'react-native-dialog-input';
import WebView from 'react-native-webview';

function index({ response, bookingDetail, sendMailOrSMS, bookingCancel, navigation, route}) {

  const { booking_id } = route.params;
  let data = null, seats = "", seatCount = 0, mapData ;
  const [isAdmin,setIsAdmin] = useState(true);
  const [email,setEmail] = useState(null);
  const [phone,setPhone] = useState(null);
  const [openDialogPhone,setOpenDialogPhone] = useState(false);
  const [openDialogEmail,setOpenDialogEmail] = useState(false);
  const [sendCheck,setSendCheck] = useState(false);
  const [loading,setLoading] = useState(false);
  const [cancelClicked,setCancelClicked] = useState(false);

  const retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem(USER_ROLE);
      if (value !== null) {
        if(value == 6){
          setIsAdmin(false);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    bookingDetail(booking_id);
    retrieveData();
  },[])

  useEffect(() => {
    if(response.bookingDetail.loading) {
      setLoading(true);
    } else {
      setLoading(false);
    }
    if (response.bookingDetail.error) {
      console.log("here is error", response);
    } else if(response.bookingDetail.payload) {
      console.log("success",response.bookingDetail.payload);
      data = response.bookingDetail.payload;
      if(data.booking_id) {
        navigation.setOptions({
          title: 'Booking - '+data.booking_id
        })
      }
      
      setPhone(data.passengers[0].mobile_number);
      setEmail(data.passengers[0].email_address);
    }
  },[
    response.bookingDetail.payload
  ]);

  useEffect(() => {
    if(response.sendMail.success && sendCheck) {
      Alert.alert(
        "Notification sent",
        "",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "OK"}
        ],
        {
            cancelable: true
        }
      );
    }
    if(response.sendMail.error) {
      console.log("email|",response.sendMail.payload)
    }
    if(response.sendMail.loading){
      setLoading(true);
    } else {
      setLoading(false);
    }
  },[
    response.sendMail
  ]);

  useEffect(() => {
    if(cancelClicked){
      if(response.bookingCancel.success){
        Alert.alert(
          "Canceled booking #"+booking_id+".",
          "",
          [
            {
              text: "Cancel",
              style: "cancel",
              onPress: () => bookingDetail(booking_id)
            },
            { text: "Ok", onPress: () => bookingDetail(booking_id) }
          ],
          {
              cancelable: true
          }
        );
      }
      if(response.bookingCancel.error){
        Alert.alert(
          "Error while Canceling booking #"+booking_id+".",
          "",
          [
            {
              text: "Cancel",
              style: "cancel",
              onPress: () => bookingDetail(booking_id)
            },
            { text: "Retry", onPress: () => cancelBookingNow() }
          ],
          {
              cancelable: true
          }
        );
      }
      if(response.bookingCancel.loading){
        setLoading(true);
      } else {
        setLoading(false);
      }
    }
  },[response.bookingCancel]);

  if(response.bookingDetail.payload) {
    console.log("success",response.bookingDetail.payload);
    data = response.bookingDetail.payload;
    let i=0;
    for (const item of data.tickets[0].seats) {
      if(i==0) {
        seats+= item.seat_number;
      } else {
        seats+= ","+item.seat_number;
      }
      i++;
    }
    seatCount = i;
  }

  const sendEmail = () => {
    setOpenDialogEmail(false);
    setSendCheck(true);
    sendMailOrSMS(booking_id + "?to=" + email, 1);
  }
  const sendSMS = () => {
    setOpenDialogPhone(false);
    setSendCheck(true);
    sendMailOrSMS(booking_id + "?to=" + phone, 1);
  }
  const sendNotification = () => {
    setSendCheck(true);
    sendMailOrSMS(booking_id, 2);
  }
  const cancelBookingNow = () => {
    setCancelClicked(true);
    bookingCancel(booking_id+"?action=cancel");
  }

  const openSctivity = (item) => {
    if(item == "modify_booking") {
      navigation.navigate(MODIFY_BOOKING_SCREEN_NAME,{
        booking_id: data.booking_id,
        ticket_id: data.tickets[0].ticket_id,
        date: data.tickets[0].date,
        stop_from_id: data.tickets[0].stop_from.id,
        stop_to_id: data.tickets[0].stop_to.id,
        seats: (data.tickets[0].seats).length,
        route_schedule_id: data.tickets[0].route_schedule_id,
        users: (data.tickets[0].seats).length
      });
    }
    if(item == "cancel_booking") {
      Alert.alert(
        "Cancel booking #"+booking_id+"?",
        "",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "Yes", onPress: () => cancelBookingNow() }
        ],
        {
            cancelable: true
        }
      );
    }
    if(item == "mail"){
      setOpenDialogEmail(true);
    }
    if(item == "sms"){
      setOpenDialogPhone(true);
    }
    if(item == "mob_notification"){
      Alert.alert(
        "Notification",
        "Send booking notification?",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "Yes", onPress: () => sendNotification() }
        ],
        {
            cancelable: true
        }
      );
    }
  };
  if(data){
    mapData ='<iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=' + data.tickets[0].stop_from.coordinates.lat +','+ data.tickets[0].stop_from.coordinates.lon +'&hl=en&z=14&amp;output=embed"></iframe>';
    mapDataTo ='<iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=' + data.tickets[0].stop_to.coordinates.lat +','+ data.tickets[0].stop_to.coordinates.lon +'&hl=en&z=14&amp;output=embed"></iframe>';
  }
  

  const actions = [
    {
      text: "Modify Booking",
      icon: <MaterialCommunityIcons name="pencil" color={WHITE} size={26} />,
      name: "modify_booking",
      position: 1
    },
    // {
    //   text: <Text style={styles.texts}>Modify Return Booking</Text>,
    //   icon: <MaterialCommunityIcons name="pencil-box-multiple" color={WHITE} size={26} />,
    //   name: "modify_partial",
    //   position: 2
    // },
    {
      text: "Cancel booking",
      icon: <MaterialCommunityIcons name="close-box" color={WHITE} size={26} />,
      name: "cancel_booking",
      position: 3
    },
    // {
    //   text: <Text style={styles.texts}>Cancel partial booking</Text>,
    //   icon: <MaterialCommunityIcons name="close-box-multiple" color={WHITE} size={26} />,
    //   name: "cancel_partial",
    //   position: 4
    // },
    // {
    //   text: <Text style={styles.texts}>Send Mobile notification</Text>,
    //   icon: <MaterialCommunityIcons name="cellphone-sound" color={WHITE} size={26} />,
    //   name: "mob_notification",
    //   position: 5
    // },
    // {
    //   text: <Text style={styles.texts}>Send SMS</Text>,
    //   icon: <MaterialCommunityIcons name="cellphone-message" color={WHITE} size={26} />,
    //   name: "sms",
    //   position: 6
    // },
    {
      text: "Send mail",
      icon: <MaterialCommunityIcons name="email" color={WHITE} size={26} />,
      name: "mail",
      position: 7
    }
  ];

    

  return (
    <View style={styles.view}>

      <Spinner
          visible={loading}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
      />
      

      <View style={{ backgroundColor: BLUE, flexDirection: 'row', padding: 10 }}>
        <View style={{ flex: 1 }}>
          <TouchableOpacity onPress={() => 
              navigation.navigate(HOME_SCREEN_NAME,{
                screen: MY_TRIPS_SCREEN_NAME,
                params: {
                  refresh: 1
                },
                merge: true,
              })
            }>
            <MaterialCommunityIcons name="keyboard-backspace" color={WHITE} size={30} />
            </TouchableOpacity>
          </View>
        <View style={{ flex: 5, justifyContent: 'center' }}>
          <Text style={{ color: WHITE, fontSize: 20, fontFamily: 'Montserrat-Bold', alignContent: 'center', justifyContent: 'center'}}>
            Booking - {booking_id}
          </Text>
        </View>
        <View style={{ flex: 1 }}>
        </View>
      </View>

      {data ? (
        <View style={styles.viewList}>
          <Text style={styles.texts}>{"Booking Date: "+data.booking_date}</Text>
          <Text style={styles.texts}>{"Passenger name: "+data.passengers[0].first_name+" "+data.passengers[0].last_name}</Text>
          <Text style={styles.texts}>{"Departure Date: "+moment(data.tickets[0].stop_from.departure_datetime).format("DD-MM-YYYY hh:mm")}</Text>
          <Text style={styles.texts}>{"Arrival Date: "+moment(data.tickets[0].stop_to.arrival_datetime).format("DD-MM-YYYY hh:mm")}</Text>
          <Text style={styles.texts}>{"Seats: "+seats+" seats ("+ seatCount +")"}</Text>
          <Text style={styles.texts}>{"Payable amount: "+data.tickets[0].price+" USD"}</Text>
          <Text style={styles.texts}>{"Booking status: "+data.booking_status}</Text>
          
        {!isAdmin && 
          <View style={styles.webViewHolder}>
            <Text style={{color: WHITE, paddingBottom: 5, fontFamily: 'Montserrat-Bold'}}>Boarding place</Text>
            <WebView
              source={{
                html: mapData 
              }}
              startInLoadingState
              scalesPageToFit
              javaScriptEnabled = {true}
              style={{ flex: 1 }}
            >
            </WebView>
          </View>
        }

        {!isAdmin && 
          <View style={styles.webViewHolder}>
            <Text style={{color: WHITE, paddingBottom: 5, fontFamily: 'Montserrat-Bold'}}>Arrival place</Text>
            <WebView
              source={{
                html: mapDataTo 
              }}
              startInLoadingState
              scalesPageToFit
              javaScriptEnabled = {true}
              style={{ flex: 1 }}
            >
            </WebView>
          </View>
        }

        </View>

      ):(
        <View style={styles.viewList}>
          {/* <Text>Something gone wrong!</Text> */}
        </View>
      ) }
      

    {isAdmin && 
      <FloatingAction
        actions={actions}
        onPressItem={(item) => {
          openSctivity(item);
        }}
        // onPressMain={() => console.log(`selected button`)}
      />
    }

    <DialogInput isDialogVisible={openDialogEmail}
                title="Send email"
                message=""
                hintInput ="Email"
                initValueTextInput = {email}
                submitInput={ (inputText) => {sendEmail(inputText)} }
                closeDialog={ () => {setOpenDialogEmail(false)}}>
    </DialogInput>
    <DialogInput isDialogVisible={openDialogPhone}
                title="Send SMS"
                message=""
                hintInput ="phone"
                initValueTextInput = {phone}
                submitInput={ (inputText) => {sendSMS(inputText)} }
                closeDialog={ () => {setOpenDialogPhone(false)}}>
    </DialogInput>
    

    </View>
  );

}

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'white',
    // padding: 20,
    height: '100%',
    width: '100%'
  },
  viewList: {
    marginBottom: 10,
    padding: 20,
    flex: 1
  },
  webViewHolder: {
    height: 200,
    width: "100%",
    backgroundColor: BLUE,
    borderWidth: 1,
    padding: 10,
    marginTop: 10,
    borderRadius: 10
  },
  textRecycler: {
    padding: 10,
    marginBottom: 2,
    borderWidth: 1,
    borderBottomColor: 'black',
    borderColor: 'white',
    borderRadius: 0,
  },
  button:{
    marginTop: 20
  },
  texts:{
    fontFamily: 'Montserrat-Medium'
  }
})

const mapStateToProps = state => {
  return {
      response: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    bookingDetail: (booking_id) => dispatch(bookingDetail(booking_id)),
    sendMailOrSMS: (mailOrPhone, type) => dispatch(sendMailOrSMS(mailOrPhone,type)),
    bookingCancel: (dataString) => dispatch(bookingCancel(dataString))
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (index)