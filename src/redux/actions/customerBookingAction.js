import {CUSTOMER_BOOKING_SUCCESS, CUSTOMER_BOOKING_ERROR, CUSTOMER_BOOKING_LOADING} from './types';
import {
  HTTP,
  POST_SEAT_BOOKING
} from '../../utilities/api';

export const fetchingCustomerBookingRequest = () => ({
  type: CUSTOMER_BOOKING_LOADING
});
export const fetchingCustomerBookingError = (error) => ({
  type: CUSTOMER_BOOKING_ERROR,
  payload: error
})
export const fetchingCustomerBookingSuccess = (data) => ({
  type: CUSTOMER_BOOKING_SUCCESS,
  payload: data
})

export function customerBooking(bookingRequest) {
  return async dispatch => {
    console.log("data: ", bookingRequest);
    dispatch(fetchingCustomerBookingRequest());
    HTTP.post(POST_SEAT_BOOKING,bookingRequest)
      .then((response) => {
        if (response.data.status == "success") {
          console.log("halaluya ",response.data)
          dispatch(fetchingCustomerBookingSuccess(response.data));
        } else {
          console.log("error: ", response);
          dispatch(fetchingCustomerBookingError(response))
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingCustomerBookingError(error))
      });
  }
};