import {PAYMENT_REQUEST_SUCCESS, PAYMENT_REQUEST_ERROR, PAYMENT_REQUEST_LOADING} from './types';
import {
  HTTP,
  POST_PAYMENT_ENTRY
} from '../../utilities/api';

export const fetchingPaymentRequestRequest = () => ({
  type: PAYMENT_REQUEST_LOADING
});
export const fetchingPaymentRequestError = (error) => ({
  type: PAYMENT_REQUEST_ERROR,
  payload: error
})
export const fetchingPaymentRequestSuccess = (data) => ({
  type: PAYMENT_REQUEST_SUCCESS,
  payload: data
})

export function paymentRequest(payment_details) {
  return async dispatch => {
    dispatch(fetchingPaymentRequestRequest());
    console.log("payment request data", payment_details);
    HTTP.post(POST_PAYMENT_ENTRY,payment_details)
      .then((response) => {
        if (response.data.status == 'success') {
          dispatch(fetchingPaymentRequestSuccess(response.data));
        } else {
          dispatch(fetchingPaymentRequestError(response))
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingPaymentRequestError(error))
      });
  }
};