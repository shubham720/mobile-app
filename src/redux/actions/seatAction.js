import {SEAT_SUCCESS, SEAT_LOADING, SEAT_ERROR, SEAT_RESET} from './types';
import {
  HTTP,
  GET_SEAT_LAYOUT
} from '../../utilities/api';

export const fetchingSeatResultRequest = () => ({type: SEAT_LOADING});
export const fetchingSeatResultError = (error) => ({
  type: SEAT_ERROR,
  payload: error
})
export const fetchingSeatResultSuccess = (data) => ({
  type: SEAT_SUCCESS,
  payload: data
})
export const seatReset = () => ({
  type: SEAT_RESET
})

export function seatRequest(date,route_schedule_id,stop_from,stop_to) {
  return function (dispatch) {
    dispatch(fetchingSeatResultRequest());
    HTTP.get(GET_SEAT_LAYOUT+date+"/"+stop_from+"/"+stop_to+"/"+route_schedule_id+"/seats")
      .then((response) => {
        if (response.data.status == 'success') {
          dispatch(fetchingSeatResultSuccess(response.data));
        } else {
          dispatch(fetchingSeatResultError(response))
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingSeatResultError(error))
      });
  }
};