import {CITY_FROM_SUCCESS, CITY_FROM_ERROR, CITY_FROM_LOADING} from './types';
import {
  HTTP,
  GET_ALL_ROUTES
} from '../../utilities/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { OPERATOR_ALL_ROUTES, OPERATOR_STOPS } from '../../utilities/constants';

export const fetchingCityFromRequest = () => ({type: CITY_FROM_LOADING});
export const fetchingCityFromError = (error) => ({
  type: CITY_FROM_ERROR,
  payload: error
})
export const fetchingCityFromSuccess = (data) => ({
  type: CITY_FROM_SUCCESS,
  payload: data
})

export function cityFrom() {
  return async dispatch => {
    dispatch(fetchingCityFromRequest());
    HTTP.get(GET_ALL_ROUTES)
      .then((response) => {
        if (response.data.status == 'success') {
            AsyncStorage.setItem(OPERATOR_ALL_ROUTES,JSON.stringify(response.data));
        } else {
          dispatch(fetchingCityFromError(response));
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingCityFromError(error))
      });
  }
};