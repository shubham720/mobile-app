import {REGISTER_SUCCESS, REGISTER_ERROR, REGISTER_LOADING} from './types';
import {
  HTTP,
  GET_USER_EXIST,
  POST_REGISTER_USER
} from '../../utilities/api';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AUTH_TOKEN, USER_ID } from '../../utilities/constants';

export const fetchingRegisterRequest = () => ({
  type: REGISTER_LOADING
});
export const fetchingRegisterError = (error) => ({
  type: REGISTER_ERROR,
  payload: error
})
export const fetchingRegisterSuccess = (data) => ({
  type: REGISTER_SUCCESS,
  payload: data
})

export function register(data) {
  return async dispatch => {
    dispatch(fetchingRegisterRequest());
    HTTP.get(GET_USER_EXIST+data.email).then((res) => {
      console.log(res.data);
      if (res.data.status == '400') {
        console.log(res.data.status);
        console.log(data);
        HTTP.post(POST_REGISTER_USER, data)
        .then((response) => {
          if (response.status == '200') {
            console.log("dispatching success");
            dispatch(fetchingRegisterSuccess(response));
          }
        })
        .catch((error) => {
          console.log('http errors where coming main', error);
          dispatch(fetchingRegisterError(error))
        });
      } else {
        console.log("dispatching error res");
        dispatch(fetchingRegisterError(res))
      }
    }).catch((error) => {
      console.log('http errors where coming user check', error);
      dispatch(fetchingRegisterError(error))
    });
    
  }
};