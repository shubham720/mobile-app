import {PAYMENT_OFFER_SUCCESS, PAYMENT_OFFER_ERROR, PAYMENT_OFFER_LOADING} from './types';
import {
  HTTP,
  POST_VALIDATE_OFFER_APP
} from '../../utilities/api';

export const fetchingPaymentOfferRequest = () => ({
  type: PAYMENT_OFFER_LOADING
});
export const fetchingPaymentOfferError = (error) => ({
  type: PAYMENT_OFFER_ERROR,
  payload: error
})
export const fetchingPaymentOfferSuccess = (data) => ({
  type: PAYMENT_OFFER_SUCCESS,
  payload: data
})

export function paymentOffer(payment_details) {
  return async dispatch => {
    dispatch(fetchingPaymentOfferRequest());
    HTTP.post(POST_VALIDATE_OFFER_APP,payment_details)
      .then((response) => {
        if (response.data.status == 'success') {
          dispatch(fetchingPaymentOfferSuccess(response.data));
        } else {
          dispatch(fetchingPaymentOfferError(response))
        }
      })
      .catch((error) => {
        dispatch(fetchingPaymentOfferError(error))
      });
  }
};