import  {SEND_MAIL_SUCCESS,SEND_MAIL_ERROR,SEND_MAIL_LOADING} from './types';
import {
  HTTP,
  GET_SEND_EMAIL_BOOKING,
  GET_SEND_APP_NOTIFICATION
} from '../../utilities/api';

export const fetchingSendMailRequest = () => ({
  type: SEND_MAIL_LOADING
});
export const fetchingSendMailError = (error) => ({
  type: SEND_MAIL_ERROR,
  payload: error
})
export const fetchingSendMailSuccess = (data) => ({
  type: SEND_MAIL_SUCCESS,
  payload: data
})

export function sendMailOrSMS(emailOrPhone,type) {
  return function (dispatch) {
    dispatch(fetchingSendMailRequest());
    let url = null;
    if(type == 1){
      url = GET_SEND_EMAIL_BOOKING+emailOrPhone;
    } else {
      url = GET_SEND_APP_NOTIFICATION+emailOrPhone;
    }
    HTTP.get(url)
      .then((response) => {
        // console.log("res po",response);
        // if (response.data.status == 'success') {
          dispatch(fetchingSendMailSuccess(response.data));
        // } else {
        //   dispatch(fetchingSendMailError(response))
        // }
      })
      .catch((error) => {
        dispatch(fetchingSendMailError(error))
      });
  }
};