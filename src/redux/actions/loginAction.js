import { LOGIN_SUCCESS, LOGIN_ERROR, LOGIN_LOADING } from "../actions/types";
import {
  GET_USER_DATA,
  HTTP,
  POST_USER_CRED,
  GET_ALL_ROUTES
} from '../../utilities/api';
import { AUTH_TOKEN, USER_COUNTRY_CODE, USER_EMAIL, USER_FIRST_NAME, USER_ID, USER_LAST_NAME, USER_NAME, USER_OPERATOR_ID, USER_PHONE, USER_ROLE } from "../../utilities/constants";
import {Alert} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";

export const fetchingLoginRequest = () => ({
  type: LOGIN_LOADING
});
export const fetchingLoginError = (error) => ({
  type: LOGIN_ERROR,
  payload: error
})
export const fetchingLoginSuccess = (data) => ({
  type: LOGIN_SUCCESS,
  payload: data
})

export function login(email,password) {
  return async dispatch => {
    dispatch(fetchingLoginRequest());
    HTTP.post(POST_USER_CRED,{
      email: email,
      password: password,
      country_code: "",
      mobile_number: "",
      otp_type: "Login",
      remember: false
    })
      .then((response) => {
        if (response.data.token) {
          AsyncStorage.setItem(AUTH_TOKEN,"Bearer "+response.data.token);
          HTTP.get(GET_USER_DATA)
          .then((response) => {
            if (response.data.id) {
              console.log(response.data);
              var user_id = (response.data.id).toString();
              AsyncStorage.setItem(USER_ID,user_id);
              AsyncStorage.setItem(USER_NAME,response.data.name);
              AsyncStorage.setItem(USER_FIRST_NAME,response.data.first_name);
              AsyncStorage.setItem(USER_LAST_NAME,response.data.last_name);
              AsyncStorage.setItem(USER_EMAIL,response.data.email);
              AsyncStorage.setItem(USER_PHONE,response.data.mobile_number);
              AsyncStorage.setItem(USER_COUNTRY_CODE,response.data.country_code);
              AsyncStorage.setItem(USER_ROLE,(response.data.user_role_id).toString());
              if(response.data.user_role_id != 6) {
                AsyncStorage.setItem(USER_OPERATOR_ID,(response.data.operator_id).toString());
              }
              dispatch(fetchingLoginSuccess(response.data));
            } else {
              dispatch(fetchingLoginError(response))
            }
          })
          .catch((error) => {
            dispatch(fetchingLoginError(error))
          });
        } else {
          dispatch(fetchingLoginError(response))
        }
      })
      .catch((error) => {
        dispatch(fetchingLoginError(error))
      });
  }
};