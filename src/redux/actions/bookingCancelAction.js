import {BOOKING_CANCEL_SUCCESS, BOOKING_CANCEL_ERROR, BOOKING_CANCEL_LOADING} from './types';
import {
  HTTP,
  POST_SEAT_BOOKING
} from '../../utilities/api';

export const fetchingBookingCancelRequest = () => ({
  type: BOOKING_CANCEL_LOADING
});
export const fetchingBookingCancelError = (error) => ({
  type: BOOKING_CANCEL_ERROR,
  payload: error
})
export const fetchingBookingCancelSuccess = (data) => ({
  type: BOOKING_CANCEL_SUCCESS,
  payload: data
})

export function bookingCancel(data) {
  return async dispatch => {
    dispatch(fetchingBookingCancelRequest());
    console.log("booking cancel ",POST_SEAT_BOOKING+"/"+data);
    HTTP.put(POST_SEAT_BOOKING+"/"+data)
      .then((response) => {
        console.log("booking data",response.data); 
        if (response.data.status == "success") {
          dispatch(fetchingBookingCancelSuccess(response.data));
        } else {
          dispatch(fetchingBookingCancelError(response.data))
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingBookingCancelError(error))
      });
  }
};