import { MODIFY_BOOKING_SUCCESS, MODIFY_BOOKING_ERROR, MODIFY_BOOKING_LOADING } from "./types";
import {
  HTTP,
  POST_MODIFY_JOURNEY
} from '../../utilities/api';

export const fetchingRequest = () => ({
  type: MODIFY_BOOKING_LOADING
});
export const fetchingError = (error) => ({
  type: MODIFY_BOOKING_ERROR,
  payload: error
})
export const fetchingSuccess = (data) => ({
  type: MODIFY_BOOKING_SUCCESS,
  payload: data
})

export function modifyBooking(data) {
  return async dispatch => {
    dispatch(fetchingRequest());
    HTTP.post(POST_MODIFY_JOURNEY,data)
      .then((response) => {
        if (response.data.status == "success") {
          dispatch(fetchingSuccess(response.data));
        } else {
          dispatch(fetchingError(response))
        }
      })
      .catch((error) => {
        dispatch(fetchingError(error))
      });
  }
};