import {ACCOUNT_UPDATE_SUCCESS, ACCOUNT_UPDATE_ERROR, ACCOUNT_UPDATE_LOADING} from './types';
import {
  HTTP,
  PATCH_PROFILE
} from '../../utilities/api';

export const fetchingAccountUpdateRequest = () => ({
  type: ACCOUNT_UPDATE_LOADING
});
export const fetchingAccountUpdateError = (error) => ({
  type: ACCOUNT_UPDATE_ERROR,
  payload: error
})
export const fetchingAccountUpdateSuccess = (data) => ({
  type: ACCOUNT_UPDATE_SUCCESS,
  payload: data
})

export function accountUpdate(data) {
  return async dispatch => {
    dispatch(fetchingAccountUpdateRequest());
    HTTP.patch(PATCH_PROFILE,{data})
      .then((response) => {
        if (response.data) {
          dispatch(fetchingAccountUpdateSuccess(response.data));
        } else {
          dispatch(fetchingAccountUpdateError(response.data))
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingAccountUpdateError(error))
      });
  }
};