import {BOOKINGS_SUCCESS, BOOKINGS_ERROR, BOOKINGS_LOADING} from './types';
import {
  HTTP,
  GET_OPERATOR_BOOKINGS,
  GET_BOOKINGS_ALL
} from '../../utilities/api';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AUTH_TOKEN, USER_ID, USER_ROLE, USER_OPERATOR_ID } from '../../utilities/constants';

export const fetchingBookingsRequest = () => ({type: BOOKINGS_LOADING});
export const fetchingBookingsError = (error) => ({
  type: BOOKINGS_ERROR,
  payload: error
})
export const fetchingBookingsSuccess = (data) => ({
  type: BOOKINGS_SUCCESS,
  payload: data
})

export function bookings(string) {
  return async dispatch => {
    dispatch(fetchingBookingsRequest());
    const user_role = await AsyncStorage.getItem(USER_ROLE);
    var url = GET_BOOKINGS_ALL;
    if(user_role != 6) {
      const op_id = await AsyncStorage.getItem(USER_OPERATOR_ID);
      url = GET_OPERATOR_BOOKINGS+string;
    }
    // var url = GET_OPERATOR_BOOKINGS + string;
    
    HTTP.get(url)
      .then((response) => {
        if (response.data) {
          dispatch(fetchingBookingsSuccess(response.data));
        } else {
          dispatch(fetchingBookingsError(response))
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingBookingsError(error))
      });
  }
};