import {CITY_TO_SUCCESS, CITY_TO_ERROR, CITY_TO_LOADING} from './types';
import {
  HTTP,
  GET_ALL_ROUTES
} from '../../utilities/api';

export const fetchingCityResultRequest = () => ({type: CITY_TO_LOADING});
export const fetchingCityResultError = (error) => ({
  type: CITY_TO_ERROR,
  payload: error
})
export const fetchingCityResultSuccess = (data) => ({
  type: CITY_TO_SUCCESS,
  payload: data
})

export function cityTo(from_id) {
  return async dispatch => {
    dispatch(fetchingCityResultRequest());
    HTTP.get(GET_ALL_ROUTES)
      .then((response) => {
        console.log("from_id here inside",from_id);
        if (response.data.status == 'success') {
          dispatch(fetchingCityResultSuccess(response.data));
        } else {
          dispatch(fetchingCityResultError(response))
        }
      })
      .catch((error) => {
        dispatch(fetchingCityResultError(error))
      });
  }
};