import { OPERATOR_STOPS_SUCCESS, OPERATOR_STOPS_ERROR, OPERATOR_STOPS_LOADING } from "../actions/types";
import {
  HTTP,
  GET_ALL_STOPS
} from '../../utilities/api';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { OPERATOR_ID, OPERATOR_STOPS, USER_OPERATOR_ID, USER_ROLE } from "../../utilities/constants";

export const fetchingOperatorStopsRequest = () => ({
  type: OPERATOR_STOPS_LOADING
});
export const fetchingOperatorStopsError = (error) => ({
  type: OPERATOR_STOPS_ERROR,
  payload: error
})
export const fetchingOperatorStopsSuccess = (data) => ({
  type: OPERATOR_STOPS_SUCCESS,
  payload: data
})

export function getStops() {
  return async dispatch => {
    dispatch(fetchingOperatorStopsRequest());
    const user_role = await AsyncStorage.getItem(USER_ROLE);
    var url = GET_ALL_STOPS+OPERATOR_ID;
    if(user_role != 6) {
      const op_id = await AsyncStorage.getItem(USER_OPERATOR_ID);
      url = GET_ALL_STOPS+op_id;
    }
    HTTP.get(url)
      .then((response) => {
        if (response.data.status == "success") {
          var newString = '', i=0;
          for (const item of response.data.result) {
            if(i==0){
              newString+=item;
            } else {
              newString+=','+item;
            }
            i++;
          }
          AsyncStorage.setItem(OPERATOR_STOPS,newString);
          dispatch(fetchingOperatorStopsSuccess(response.data));
        } else {
          dispatch(fetchingOperatorStopsError(response))
        }
      })
      .catch((error) => {
        dispatch(fetchingOperatorStopsError(error))
    });
  }
};