import { OPERATOR_DATA_SUCCESS, OPERATOR_DATA_LOADING, OPERATOR_DATA_ERROR } from './types';
import {
  HTTP,
  GET_OPERATOR_ROUTE_DETAILS
} from '../../utilities/api';

export const fetchingOperatorDataRequest = () => ({
  type: OPERATOR_DATA_LOADING
});
export const fetchingOperatorDataError = (error) => ({
  type: OPERATOR_DATA_ERROR,
  payload: error
})
export const fetchingOperatorDataSuccess = (data) => ({
  type: OPERATOR_DATA_SUCCESS,
  payload: data
})

export function getOperatorDetail(ticket_id) {
  return async dispatch => {
    dispatch(fetchingOperatorDataRequest());
    HTTP.get(GET_OPERATOR_ROUTE_DETAILS+ticket_id)
      .then((response) => {
        console.log("res ", response.data, ticket_id);
        if (response.data) {
          dispatch(fetchingOperatorDataSuccess(response.data));
        } else {
          dispatch(fetchingOperatorDataError(response.data))
        }
      })
      .catch((error) => {
        dispatch(fetchingOperatorDataError(error))
      });
  }
};