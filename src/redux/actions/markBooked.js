import { MARK_BOOKING_SUCCESS, MARK_BOOKING_ERROR, MARK_BOOKING_LOADING } from "../actions/types";
import {
  HTTP,
  POST_SEAT_BOOKING,
} from '../../utilities/api';

export const fetchingMarkBookingRequest = () => ({
  type: MARK_BOOKING_LOADING
});
export const fetchingMarkBookingError = (error) => ({
  type: MARK_BOOKING_ERROR,
  payload: error
})
export const fetchingMarkBookingSuccess = (data) => ({
  type: MARK_BOOKING_SUCCESS,
  payload: data
})

export function markBooking(booking_id) {
  return async dispatch => {
    dispatch(fetchingMarkBookingRequest());
    HTTP.put(POST_SEAT_BOOKING+"/"+booking_id+"?action=confirm")
      .then((response) => {
        if (response.data.status == "success") {
          dispatch(fetchingMarkBookingSuccess(response.data));
        } else {
          dispatch(fetchingMarkBookingError(response))
        }
      })
      .catch((error) => {
        dispatch(fetchingMarkBookingError(error))
      });
  }
};