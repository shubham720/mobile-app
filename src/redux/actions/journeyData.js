import { JOURNEY_DATA_SUCCESS, JOURNEY_DATA_ERROR, JOURNEY_DATA_LOADING } from "../actions/types";
import {
  HTTP,
  GET_JOURNEY_DETAILS
} from '../../utilities/api';

export const fetchingRequest = () => ({
  type: JOURNEY_DATA_LOADING
});
export const fetchingError = (error) => ({
  type: JOURNEY_DATA_ERROR,
  payload: error
})
export const fetchingSuccess = (data) => ({
  type: JOURNEY_DATA_SUCCESS,
  payload: data
})

export function getJourneyData(booking_id,ticket_id,date) {
  return async dispatch => {
    dispatch(fetchingRequest());
    HTTP.get(GET_JOURNEY_DETAILS+booking_id + "/" + ticket_id + "/" + date + "/0")
      .then((response) => {
        if (response.data.status == "success") {
          dispatch(fetchingSuccess(response.data));
        } else {
          dispatch(fetchingError(response))
        }
      })
      .catch((error) => {
        dispatch(fetchingError(error))
      });
  }
};