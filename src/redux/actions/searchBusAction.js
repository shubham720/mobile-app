import {SEARCH_SUCCESS, SEARCH_ERROR, SEARCH_LOADING} from './types';
import {
  HTTP,
  SEARCH_API_URL
} from '../../utilities/api';
import { OPERATOR_ID, USER_OPERATOR_ID, USER_ROLE } from '../../utilities/constants';
import AsyncStorage from "@react-native-async-storage/async-storage";
export const fetchingSearchResultRequest = () => ({type: SEARCH_LOADING});
export const fetchingSearchResultError = (error) => ({
  type: SEARCH_ERROR,
  payload: error
});
export const fetchingSearchResultSuccess = (data) => ({
  type: SEARCH_SUCCESS,
  payload: data
});

export function searchBus(date,from_id,to_id) {
  return async dispatch => {
    dispatch(fetchingSearchResultRequest());
    const user_role = await AsyncStorage.getItem(USER_ROLE);
    var url = SEARCH_API_URL+date+"/"+from_id+"/"+to_id+"/"+OPERATOR_ID+"/0/0";
    if(user_role != 6) {
      const op_id = await AsyncStorage.getItem(USER_OPERATOR_ID);
      url = SEARCH_API_URL+date+"/"+from_id+"/"+to_id+"/"+op_id+"/0/0";
    }
    HTTP.get(url)
      .then((response) => {
        if (response.data.status == 'success') {
          dispatch(fetchingSearchResultSuccess(response.data));
        } else {
          dispatch(fetchingSearchResultError(response))
        }
      })
      .catch((error) => {
        console.log("issue",error);
        dispatch(fetchingSearchResultError(error))
      });
  }
};