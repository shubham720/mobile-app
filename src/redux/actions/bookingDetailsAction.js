import {BOOKING_DETAILS_SUCCESS, BOOKING_DETAILS_ERROR, BOOKING_DETAILS_LOADING} from './types';
import {
  HTTP,
  GET_BOOKING_DETAILS
} from '../../utilities/api';

export const fetchingBookingDetailsRequest = () => ({
  type: BOOKING_DETAILS_LOADING
});
export const fetchingBookingDetailsError = (error) => ({
  type: BOOKING_DETAILS_ERROR,
  payload: error
})
export const fetchingBookingDetailsSuccess = (data) => ({
  type: BOOKING_DETAILS_SUCCESS,
  payload: data
})

export function bookingDetail(booking_id) {
  return async dispatch => {
    dispatch(fetchingBookingDetailsRequest());
    HTTP.get(GET_BOOKING_DETAILS+booking_id)
      .then((response) => {
        if (response.data.booking_id) {
          dispatch(fetchingBookingDetailsSuccess(response.data));
        } else {
          dispatch(fetchingBookingDetailsError(response.data))
        }
      })
      .catch((error) => {
        console.log('http errors where coming', error);
        dispatch(fetchingBookingDetailsError(error))
      });
  }
};