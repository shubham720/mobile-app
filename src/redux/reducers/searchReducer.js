export const defaultState = {
  searchResults: [],
  error: false,
  loading: false
};

const searchReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'SEARCH_SUCCESS':
          return {
            ...state,
            error: false,
            loading: false,
            searchResults: action.payload
        };
        case 'SEARCH_ERROR':
          return {
            ...state,
            error: action.payload,
        };
        case 'SEARCH_LOADING':
          return {
            ...state,
            loading: true,
        };
        default:
          return state;
    }
}

export default searchReducer;