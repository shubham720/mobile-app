import { MODIFY_BOOKING_SUCCESS, MODIFY_BOOKING_ERROR, MODIFY_BOOKING_LOADING } from "../actions/types";

export const defaultState = {
  payload: null,
  success: false,
  error: false,
  loading: false
};

const index = (state = defaultState, action) => {
    switch (action.type) {
        case MODIFY_BOOKING_SUCCESS:
          return {
            ...state,
            success: true,
            error: false,
            loading: false,
            payload: action.payload
          };
        case MODIFY_BOOKING_ERROR:
          return {
            ...state,
            success: false,
            error: true,
            loading: false,
            payload: action.payload,
          };
        case MODIFY_BOOKING_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            success: false,
            payload: null
          };
        default:
          return state;
    }
}

export default index;