import { MARK_BOOKING_SUCCESS, MARK_BOOKING_LOADING, MARK_BOOKING_ERROR } from "../actions/types";
export const defaultState = {
  payload: null,
  error: false,
  loading: false,
  success: false
};

const operatorDataReducer = (state = defaultState, action) => {
    switch (action.type) {
        case MARK_BOOKING_SUCCESS:
          return {
            ...state,
            error: false,
            loading: false,
            success: true,
            payload: action.payload
          };
        case MARK_BOOKING_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            success: false,
            payload: action.payload
          };
        case MARK_BOOKING_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            success: false,
            payload: action.payload
          };
        default:
          return state;
    }
}

export default operatorDataReducer;