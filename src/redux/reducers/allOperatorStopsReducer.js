import { OPERATOR_STOPS_SUCCESS, OPERATOR_STOPS_ERROR, OPERATOR_STOPS_LOADING } from "../actions/types";
export const defaultState = {
  payload: null,
  error: false,
  loading: false,
  success: false
};

const allOperatorStopsReducer = (state = defaultState, action) => {
    switch (action.type) {
        case OPERATOR_STOPS_SUCCESS:
          return {
            ...state,
            error: false,
            loading: false,
            success: true,
            payload: action.payload
          };
        case OPERATOR_STOPS_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            success: false,
            payload: action.payload
          };
        case OPERATOR_STOPS_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            success: false,
            payload: action.payload
          };
        default:
          return state;
    }
}

export default allOperatorStopsReducer;