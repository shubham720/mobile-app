import {BOOKING_DETAILS_SUCCESS, BOOKING_DETAILS_ERROR, BOOKING_DETAILS_LOADING} from '../actions/types';
export const defaultState = {
  payload: null,
};

const bookingDetailsReducer = (state = defaultState, action) => {
    switch (action.type) {
        case BOOKING_DETAILS_SUCCESS:
          return {
            ...state,
            error: false,
            loading: false,
            payload: action.payload
          };
        case BOOKING_DETAILS_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            payload: action.payload
          };
        case BOOKING_DETAILS_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            payload: null
          };
        default:
          return state;
    }
}

export default bookingDetailsReducer;