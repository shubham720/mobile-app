import {SEAT_ERROR, SEAT_LOADING, SEAT_SUCCESS, SEAT_RESET} from '../actions/types';
export const defaultState = {
  payload: [],
  success: false,
  error: false,
  loading: false
};

const seatReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SEAT_SUCCESS :
          return {
            ...state,
            success: true,
            error: false,
            loading: false,
            payload: action.payload
          };
        case SEAT_ERROR:
          return {
            ...state,
            success: false,
            error: true,
            loading: false,
            payload: action.payload
          };
        case SEAT_LOADING:
          return {
            ...state,
            success: false,
            error: false,
            loading: true,
            payload: []
          };
        case SEAT_RESET:
          return {
            ...state,
            success: false,
            error: false,
            loading: true,
            payload: []
          }
        default:
          return state;
    }
}

export default seatReducer;