import {BOOKINGS_SUCCESS,BOOKINGS_ERROR, BOOKINGS_LOADING} from '../actions/types';

export const defaultState = {
  success: false,
  error: false,
  loading: false,
  payload: null
};

const bookingsReducer = (state = defaultState, action) => {
    switch (action.type) {
        case BOOKINGS_SUCCESS:
          return {
            ...state,
            success: true,
            error: false,
            loading: false,
            payload: action.payload
          };
        case BOOKINGS_ERROR:
          return {
            ...state,
            error: false,
            loading: true,
            success: false,
            payload: action.payload
          };
        case BOOKINGS_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            success: false,
            payload: null
          };
        default:
          return state;
    }
}

export default bookingsReducer;