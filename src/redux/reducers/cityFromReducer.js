export const defaultState = {
  cityList: [],
};

const cityFromReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'CITY_FROM_SUCCESS':
          return {
            ...state,
            error: false,
            loading: false,
            cityList: action.payload.result.details
          };
        case 'CITY_FROM_ERROR':
          return {
            ...state,
            error: action.payload,
          };
        case 'CITY_FROM_LOADING':
          return {
            ...state,
            loading: true,
          };
        default:
          return state;
    }
}

export default cityFromReducer;