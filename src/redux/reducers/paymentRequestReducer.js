import {PAYMENT_REQUEST_SUCCESS, PAYMENT_REQUEST_ERROR, PAYMENT_REQUEST_LOADING} from '../actions/types';
export const defaultState = {
  payload: [],
};

const paymentRequestReducer = (state = defaultState, action) => {
    switch (action.type) {
        case PAYMENT_REQUEST_SUCCESS:
          return {
            ...state,
            error: false,
            loading: false,
            payload: action.payload
          };
        case PAYMENT_REQUEST_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            payload: action.payload
          };
        case PAYMENT_REQUEST_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            payload: null
          };
        default:
          return state;
    }
}

export default paymentRequestReducer;