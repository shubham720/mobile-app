import {CUSTOMER_BOOKING_SUCCESS, CUSTOMER_BOOKING_ERROR, CUSTOMER_BOOKING_LOADING} from '../actions/types';
export const defaultState = {
  payload: [],
  success: false,
  error: false,
  loading: false
};

const customerBookingReducer = (state = defaultState, action) => {
    switch (action.type) {
        case CUSTOMER_BOOKING_SUCCESS:
          return {
            ...state,
            success: true,
            error: false,
            loading: false,
            payload: action.payload
          };
        case CUSTOMER_BOOKING_ERROR:
          return {
            ...state,
            success: false,
            error: true,
            loading: false,
            payload: action.payload
          };
        case CUSTOMER_BOOKING_LOADING:
          return {
            ...state,
            success: false,
            error: false,
            payload: [],
            loading: true,
          };
        default:
          return state;
    }
}

export default customerBookingReducer;