import { LOGIN_SUCCESS, LOGIN_ERROR, LOGIN_LOADING } from "../actions/types";

export const defaultState = {
  payload: null,
  success: false,
  error: false,
  loading: false
};

const loginReducer = (state = defaultState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
          return {
            ...state,
            success: true,
            error: false,
            loading: false,
            payload: action.payload
          };
        case LOGIN_ERROR:
          return {
            ...state,
            success: false,
            error: true,
            loading: false,
            payload: action.payload,
          };
        case LOGIN_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            success: false,
            payload: null
          };
        default:
          return state;
    }
}

export default loginReducer;