import {BOOKING_CANCEL_SUCCESS, BOOKING_CANCEL_ERROR, BOOKING_CANCEL_LOADING} from '../actions/types';
export const defaultState = {
  error: false,
  loading: false,
  success: false,
  payload: null
};

const bookingCancelReducer = (state = defaultState, action) => {
    switch (action.type) {
        case BOOKING_CANCEL_SUCCESS:
          return {
            ...state,
            success: true,
            error: false,
            loading: false,
            payload: action.payload
          };
        case BOOKING_CANCEL_ERROR:
          return {
            ...state,
            success: false,
            error: true,
            loading: false,
            payload: action.payload
          };
        case BOOKING_CANCEL_LOADING:
          return {
            ...state,
            success: false,
            error: false,
            loading: true,
            payload: null
          };
        default:
          return state;
    }
}

export default bookingCancelReducer;