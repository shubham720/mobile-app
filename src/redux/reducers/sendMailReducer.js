import {SEND_MAIL_SUCCESS,SEND_MAIL_ERROR,SEND_MAIL_LOADING} from '../actions/types';
export const defaultState = {
  payload: null,
  error: false,
  loading: false,
  success: false,
};

const sendMailReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SEND_MAIL_SUCCESS:
          return {
            ...state,
            error: false,
            loading: false,
            success: true,
            payload: action.payload
          };
        case SEND_MAIL_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            success: false,
            payload: action.payload
          };
        case SEND_MAIL_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            success: false,
            payload: null
          };
        default:
          return state;
    }
}

export default sendMailReducer;