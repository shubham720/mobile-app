import {combineReducers} from 'redux';
import searchReducer from './searchReducer';
import cityFromReducer from './cityFromReducer';
import cityToReducer from './cityToReducer';
import seatReducer from './seatReducer';
import bookingsReducer from './bookingsReducer';
import loginReducer from './loginReducer';
import bookingDetailsReducer from './bookingDetailsReducer';
import customerBookingReducer from './customerBookingReducer';
import paymentOfferReducer from './paymentOfferReducer';
import paymentRequestReducer from './paymentRequestReducer';
import registerReducer from './registerReducer';
import operatorDataReducer from './operatorDataReducer';
import sendMailReducer from './sendMailReducer';
import bookingCancelReducer from './bookingCancelReducer';
import markBooking from './markBookingReducer';
import journeyData  from './journeyDataReducer';
import modifyBooking from './modifyBookingReducer';

const reducers = combineReducers({
    cityFromList: cityFromReducer,
    searchResults: searchReducer,
    cityToList: cityToReducer,
    seatReducer: seatReducer,
    bookings: bookingsReducer,
    login: loginReducer,
    bookingDetail: bookingDetailsReducer,
    customerBooking: customerBookingReducer,
    paymentDetails: paymentOfferReducer,
    paymentRequest: paymentRequestReducer,
    registerReducer: registerReducer,
    operatorDataReducer: operatorDataReducer,
    sendMail: sendMailReducer,
    bookingCancel: bookingCancelReducer,
    markBooking: markBooking,
    journeyData: journeyData,
    modifyBooking: modifyBooking
});
  
export default reducers;