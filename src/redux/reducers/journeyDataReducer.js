import {JOURNEY_DATA_SUCCESS, JOURNEY_DATA_ERROR, JOURNEY_DATA_LOADING} from '../actions/types';
export const defaultState = {
  payload: null,
  error: false,
  loading: false,
  success: false
};

const index = (state = defaultState, action) => {
  switch (action.type) {
      case JOURNEY_DATA_SUCCESS:
        return {
          ...state,
          error: false,
          loading: false,
          success: true,
          payload: action.payload
        };
      case JOURNEY_DATA_ERROR:
        return {
          ...state,
          error: true,
          loading: false,
          success: false,
          payload: action.payload
        };
      case JOURNEY_DATA_LOADING:
        return {
          ...state,
          error: false,
          loading: true,
          success: false,
          payload: action.payload
        };
      default:
        return state;
  }
}

export default index;