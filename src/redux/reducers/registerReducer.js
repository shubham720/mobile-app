import { REGISTER_SUCCESS, REGISTER_ERROR, REGISTER_LOADING } from "../actions/types";

export const defaultState = {
  payload: null,
  loading: false,
  error: false,
  success: false
};

const registerReducer = (state = defaultState, action) => {
    switch (action.type) {
        case REGISTER_SUCCESS:
          return {
            ...state,
            loading: false,
            error: false,
            success: true,
            payload: action.payload
          };
        case REGISTER_ERROR:
          return {
            ...state,
            loading: false,
            error: true,
            success: false,
            payload: action.payload,
          };
        case REGISTER_LOADING:
          return {
            ...state,
            loading: false,
            error: false,
            success: false
          };
        default:
          return state;
    }
}

export default registerReducer;