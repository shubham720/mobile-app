import { OPERATOR_DATA_SUCCESS, OPERATOR_DATA_LOADING, OPERATOR_DATA_ERROR } from "../actions/types";
export const defaultState = {
  payload: null,
};

const operatorDataReducer = (state = defaultState, action) => {
    switch (action.type) {
        case OPERATOR_DATA_SUCCESS:
          return {
            ...state,
            error: false,
            loading: false,
            payload: action.payload
          };
        case OPERATOR_DATA_ERROR:
          return {
            ...state,
            error: true,
            loading: false,
            payload: action.payload
          };
        case OPERATOR_DATA_LOADING:
          return {
            ...state,
            error: false,
            loading: true,
            payload: action.payload
          };
        default:
          return state;
    }
}

export default operatorDataReducer;