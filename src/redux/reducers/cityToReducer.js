export const defaultState = {
  cityToList: [],
};

const cityToReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'CITY_TO_SUCCESS':
          return {
            ...state,
            error: false,
            loading: false,
            cityToList: action.payload.result.destinations
          };
        case 'CITY_TO_ERROR':
          return {
            ...state,
            error: action.payload,
          };
        case 'CITY_TO_LOADING':
          return {
            ...state,
            loading: true,
          };
        default:
          return state;
    }
}

export default cityToReducer;