import {PAYMENT_OFFER_SUCCESS, PAYMENT_OFFER_ERROR, PAYMENT_OFFER_LOADING} from '../actions/types';
export const defaultState = {
  payload: [],
};

const paymentOfferReducer = (state = defaultState, action) => {
    switch (action.type) {
        case PAYMENT_OFFER_SUCCESS:
          return {
            ...state,
            error: false,
            loading: false,
            payload: action.payload
          };
        case PAYMENT_OFFER_ERROR:
          return {
            ...state,
            error: action.payload,
          };
        case PAYMENT_OFFER_LOADING:
          return {
            ...state,
            loading: true,
          };
        default:
          return state;
    }
}

export default paymentOfferReducer;