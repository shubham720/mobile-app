import axios from 'axios';
import {AUTH_TOKEN} from '../utilities/constants';
import { BASE_API_URL, BASE_URL } from '../utilities/constants';
import AsyncStorage from "@react-native-async-storage/async-storage";
export const HTTP = axios.create({
    baseURL: BASE_API_URL,
    timeout: 20000,
    headers: {'Content-Type': 'application/json'},
});
HTTP.interceptors.request.use(
    async (config) => {
      const token = await AsyncStorage.getItem(AUTH_TOKEN);
      if(token) {
        config.headers.Authorization = await AsyncStorage.getItem(AUTH_TOKEN);
      }
      return config;
    },
    (error) => Promise.reject(error),
);

// In use
export const GET_ALL_ROUTES = BASE_API_URL + "v1/locations/pairs";
export const GET_ALL_STOPS = BASE_API_URL + "v1/stops/availableLocation/"; // op_id
export const SEARCH_API_URL = BASE_API_URL + "v1/route-schedules/";
export const GET_SEAT_LAYOUT = BASE_API_URL + "v1/route-schedules/";
export const GET_BOOKINGS_ALL = BASE_API_URL + "admin/v1/bookings?include=passengers,tickets.ticketSeats,locationFrom,locationTo,user,tickets,payment,tickets.stopFrom,tickets.stopTo,vehicleTypes&lastId=0&pos=0&previous=0&limit=20&filter=";
export const GET_OPERATOR_BOOKINGS = BASE_API_URL + "admin/v1/bookings?include=tickets.seats,passengers,points,tickets.ticketSeats,locationFrom,locationTo,user,tickets,payment,tickets.stopFrom,tickets.stopTo,vehicleTypes,paymentMeth&";
export const GET_USER_BOOKINGS = BASE_API_URL + "admin/v1/bookings/user/mobile/"; // User id
export const GET_BOOKING_DETAILS = BASE_API_URL + "admin/v1/getuserticket/"; // Booking id
export const POST_USER_CRED = BASE_API_URL + "login";
export const GET_USER_DATA = BASE_API_URL + "user";
export const POST_SEAT_BOOKING = BASE_API_URL + "v1/bookings";
export const POST_VALIDATE_OFFER_APP = BASE_API_URL + "v1/validateOfferApp";
export const POST_PAYMENT_ENTRY = BASE_API_URL + "v1/payments";
export const GET_PAYMENT_STATUS = BASE_API_URL + "v1/payments/";
export const GET_USER_EXIST = BASE_API_URL + "user/exists/check/";
export const POST_REGISTER_USER = BASE_API_URL + "register";
export const GET_JOURNEY_DETAILS = BASE_API_URL + "admin/v1/getJourneyData/";
export const GET_OPERATOR_ROUTE_DETAILS = BASE_API_URL + "admin/v1/routes/getOperatorRouteDetails/"; // TICKET ID
export const GET_SEND_EMAIL_BOOKING = BASE_API_URL + "admin/v1/bookings/sendnotif/"; //mail or phone
export const GET_SEND_APP_NOTIFICATION = BASE_API_URL +"admin/v1/booking/send/app/notification/";
export const POST_MODIFY_JOURNEY = BASE_API_URL + "admin/v1/modifyJourney";

export const WING_PAY_URL = BASE_URL + "wing/native/start";
export const ABA_PAY_URL = BASE_URL + "aba/native/start";
