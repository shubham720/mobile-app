import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity,Image } from 'react-native';
import { DARKGREY, GREY, WHITE } from '../utilities/colors';

export default function TextReview({ label, textMain, onPress, imgIndex, error, ...props }) {
    var data = [
      require('../images/png/location.png'),
      require('../images/png/seat.png'),
      require('../images/png/bus_white_back.png'),
      require('../images/png/date.png'),
      require('../images/png/time.png'),
      require('../images/png/price.png')
    ];
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.inputContainer} onPress={onPress}>
          <View style={styles.imageDecor}>
            <Image source={data[imgIndex]}/>
          </View>
          <View style={styles.row}>
            <Text onPress={onPress} style={styles.mainText}>{textMain}</Text>
            <Text style={styles.inputLabel} onPress={onPress}>{label}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: WHITE,
    paddingVertical: 10,
    paddingHorizontal: 5,
    marginVertical: 5,
    marginBottom:10,
    flexDirection: 'row'
  },
  image: {
    width: 50,
    paddingEnd: 10
  },
  imageDecor: {
    borderBottomColor: GREY,
    borderTopColor: GREY,
    borderStartColor: GREY,
    borderEndColor: GREY,
    borderWidth: 1,
    borderRadius: 10,
    marginEnd: 10
  },
  row: {
    flex: 7,
    borderBottomColor: GREY,
    borderTopColor: WHITE,
    borderStartColor: WHITE,
    borderEndColor: WHITE,
    borderWidth: 1,
  },
  inputLabel: {
    paddingTop: 5,
    paddingBottom: 3,
    fontSize: 10,
    color: 'grey',
    fontFamily: 'Montserrat-Medium'
  },
  input: {
    color: GREY,
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 3,
    marginRight: 10,
    flex: 1,
  },
  mainText: {
    fontWeight: '800',
    color: DARKGREY,
    fontFamily: 'Montserrat-Medium'
  }
});



