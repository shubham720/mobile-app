import React, {Component,useState} from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {BLUE,WHITE,ORANGE} from '../utilities/colors';

export default function RadioButton({data,columns, onPress, ...props}) {
    var mainData = [];
    var i = 0;
    if(data){
      if(data.length > 0){
        mainData = data;
          for (const item of mainData) {
            mainData[i].style = styles.buttonWhite;
            mainData[i].styleInner = styles.whiteText;
            mainData[i].selected = false;
            mainData[i].indexMain = i;
            i++;
          }
      }
    }
    
    const makeStylesChanges = (itemData) => {
      itemData.selected = !itemData.selected;
      itemData.style = itemData.selected ? styles.button : styles.buttonWhite;
      itemData.styleInner = itemData.selected ? styles.buttonText : styles.whiteText;
      data[itemData.indexMain] = itemData;
      var newData = mainData;
      const index = newData.indexOf(itemData.text);
      if (index > -1) {
        newData.splice(index, 1);
        newData[index] = itemData;
      } else {
        newData[index] = itemData;
      }
      mainData = newData;
      console.log("after change",mainData);
    }

    return (
      <View style={styles.mainView}>
        { mainData &&
          (<FlatList
          data={mainData}
          numColumns={columns}
          extraData={mainData}
          renderItem = {({item}) => (
            <TouchableOpacity onPress={() => {
              onPress(item.text);
              makeStylesChanges(item);
              }}>
              <View style={item.style} key={item.indexMain}>
                  <Text style={item.styleInner}>
                      {item.text}
                  </Text>
              </View>
          </TouchableOpacity>
          )}
        >
        </FlatList>)
      }
      </View>
    );
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 8,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: BLUE
    },
    buttonText:{
        color: WHITE,
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center'
    },
    buttonWhite: {
      borderRadius: 8,
      paddingVertical: 14,
      paddingHorizontal: 10,
      backgroundColor: WHITE
    },
    whiteText: {
      color: ORANGE,
      fontWeight: 'bold',
      fontSize: 16,
      textAlign: 'center'
  },
  mainView: {
    marginBottom: 20
  }
})