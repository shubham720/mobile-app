import React, {Component,useState} from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {BLUE,WHITE,ORANGE} from '../utilities/colors';

export default function Radio({data,value,columns, onPress, ...props}) {
    return (
        <View style={styles.mainView}>
            {data.map(res => {
                return (
                  <TouchableOpacity key={res.id} onPress={() => { onPress(res.id,res.text); }}>
                    <View style={styles.container}>
                        <View style={styles.radioCircle}>
                              {value === res.id && <View style={styles.selectedRb} />}
                        </View>
                        <Text style={styles.radioText}>{res.text}</Text>
                    </View>
                  </TouchableOpacity>
                );
            })}
        </View>
    );
}

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    padding: 10
  },
	container: {
    marginBottom: 10,
    alignItems: 'center',
    flexDirection: 'row',
		justifyContent: 'space-between',
	},
  radioText: {
    marginStart: 10,
    marginRight: 35,
    fontSize: 14,
    color: '#000',
    fontFamily: 'Montserrat-Bold'
  },
	radioCircle: {
		height: 20,
		width: 20,
		borderRadius: 100,
		borderWidth: 2,
		borderColor: BLUE,
		alignItems: 'center',
		justifyContent: 'center',
	},
	selectedRb: {
		width: 10,
		height: 10,
		borderRadius: 50,
		backgroundColor: BLUE,
  },
  result: {
    marginTop: 20,
    color: 'white',
    fontWeight: '600',
    backgroundColor: '#F3FBFE',
  },
});