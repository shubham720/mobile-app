import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { BLUE, LIGHTBLUE, ORANGE } from '../utilities/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function SearchFromTo({ label, textMain, onPress, type, error, ...props }) {
    return(
      <TouchableOpacity onPress={onPress}>
        <View style={styles.inputContainer} onPress={onPress}>
          <Text style={styles.inputLabel} onPress={onPress}>{label}</Text>
          <View style={styles.row}>
            <View style={styles.imageHolder}>
              {type == 1 && 
                <MaterialCommunityIcons style={styles.image} name="checkbox-blank-circle" color={BLUE} size={10} />
              }
              {type == 2 && 
                <MaterialCommunityIcons style={styles.image} name="checkbox-blank-circle" color={ORANGE} size={10} />
              }
            </View>
            <View style={styles.textHolder}>
              <Text onPress={onPress} style={styles.textIn}>{textMain}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#f4f6f8',
    paddingVertical: 8,
    paddingHorizontal: 15,
    borderRadius: 10,
    width: '100%'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 6
  },
  inputLabel: {
    fontSize: 12,
    color: 'grey',
    fontFamily: 'Montserrat-Medium'
  },
  input: {
    color: '#353031',
    fontWeight: "800",
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    marginTop: 3,
    marginRight: 10,
    flex: 1,
  },
  textIn: {
    color: LIGHTBLUE,
    fontFamily: 'Montserrat-Bold',
  },
  textHolder: {
    flex: 20
  },
  image: {
    width: 15,
    height: 15,
    paddingTop: 3
  },
  imageHolder: {
    flex: 1
  }
});



