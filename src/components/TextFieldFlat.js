import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native';

export default function TextInputFlat({ label, error,onChangeText,secureTextEntry,keyboardType, ...props }) {
    return(
      <View style={styles.inputContainer}>
      <Text style={styles.inputLabel}>{label}</Text>
      <View style={styles.row}>
        <TextInput autoCapitalize="none" style={styles.input} secureTextEntry={secureTextEntry} onChangeText={onChangeText} keyboardType={keyboardType} {...props} />
        {/* <Error display={error} /> */}
      </View>
    </View>
    );
};
const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#f4f6f8',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 10,
    marginVertical: 5,
    marginBottom:10
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputLabel: {
    fontSize: 12,
    color: 'grey',
    fontFamily: 'Montserrat-Medium'
  },
  input: {
    color: '#353031',
    backgroundColor: '#f4f6f8',
    fontSize: 16,
    marginTop: 3,
    marginRight: 10,
    padding: 10,
    flex: 1,
    fontFamily: 'Montserrat-Bold'
  }
});



