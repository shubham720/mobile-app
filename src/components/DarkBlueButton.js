import React, {Component} from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {WHITE,DARKPURPLE} from '../utilities/colors';

export default function DarkBlueButton({ text, onPress }) {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>
                    {text}
                </Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 8,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: DARKPURPLE
    },
    buttonText: {
        color: WHITE,
        fontSize: 16,
        textAlign: 'center',
        fontFamily: 'Montserrat-Bold'
    }
})