import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Image } from 'react-native';
import { BLUE, LIGHTBLUE } from '../utilities/colors';

export default function DatesField({ label, textMain, onPress, error, ...props }) {
    return(
      <TouchableOpacity onPress={onPress}>
        <View style={styles.inputContainer} onPress={onPress}>
          <Text style={styles.inputLabel} onPress={onPress}>{label}</Text>
          <View style={styles.row}>
            <View style={styles.imageHolder}>
              <Image style={styles.image} source={require('../images/png/date_input.png')}></Image>
            </View>
            <View style={styles.textHolder}>
              <Text onPress={onPress} style={styles.textIn}>{textMain}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
  inputContainer: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 10,
    width: '100%'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: 10
  },
  inputLabel: {
    fontSize: 12,
    color: 'grey',
    fontFamily: 'Montserrat-Medium'
  },
  input: {
    color: '#353031',
    fontWeight: "800",
    fontSize: 16,
    marginTop: 3,
    marginRight: 10,
    flex: 1,
  },
  textIn: {
    color: LIGHTBLUE,
    fontFamily: 'Montserrat-Bold',
  },
  textHolder:{
    paddingStart: 5
  },
  image: {
    width: 20,
    height: 20,
  }
});



