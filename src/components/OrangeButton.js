import React, {Component} from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {ORANGE,WHITE} from '../utilities/colors';

export default function LightBlueButton({ text, onPress }) {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>
                    {text}
                </Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 8,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: ORANGE,
    },
    buttonText:{
        color: WHITE,
        fontSize: 16,
        textAlign: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat-Bold'
    }
})