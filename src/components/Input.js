import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

export default function Input({ label, error, ...props }) {
    return(
      <View style={styles.inputContainer}>
        <Text style={styles.inputLabel}>{label}</Text>
  
        <View style={styles.row}>
          <TextInput autoCapitalize="none" style={styles.input} {...props} />

        </View>
      </View>
    );
};
const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#f4f6f8',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 10,
    marginVertical: 5,
    marginBottom:10
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputLabel: {
    fontSize: 12,
    color: '#b4b6b8',
  },
  input: {
    color: '#353031',
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 3,
    marginRight: 10,
    flex: 1,
  }
});