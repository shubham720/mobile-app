import React, {Component} from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {DARKGREY, GREY, ORANGE,WHITE} from '../utilities/colors';

export default function LightBlueButton({ text, onPress }) {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>
                    {text}
                </Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 8,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: DARKGREY,
    },
    buttonText:{
        color: WHITE,
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat-Medium'
    }
})