import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity } from 'react-native';
import { GREY, WHITE } from '../utilities/colors';

export default function TextInputFlat({ label, textMain, onPress, error, ...props }) {
    return(
      <TouchableOpacity onPress={onPress}>
        <View style={styles.inputContainer} onPress={onPress}>
          {/* <Text style={styles.inputLabel} onPress={onPress}>{label}</Text> */}
          <View style={styles.row}>
            <Text onPress={onPress}>{textMain}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: WHITE,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    borderColor: GREY,
    borderWidth: 1,
    marginStart: 10,
    marginEnd: 10
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputLabel: {
    fontSize: 12,
    color: 'grey',
    fontFamily: 'Montserrat-Medium'
  },
  input: {
    color: '#353031',
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 3,
    marginRight: 10,
    flex: 1,
    fontFamily: 'Montserrat-Medium'
  }
});



