import React, {Component,useState} from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import {BLUE,WHITE,ORANGE} from '../utilities/colors';

export default function RadioImages({data,value,columns, onPress, ...props}) {
    return (
        <View style={styles.mainView}>
            {data.map(item => {
                return (
                  <TouchableOpacity key={item.id} onPress={() => { onPress(item.id,item.text); }}>
                    <View style={styles.container}>
                        <View style={styles.radioCircle}>
                              {value === item.id && <View style={styles.selectedRb} />}
                        </View>
                        {/* <View style={styles.imageContainer}> */}
                          <Image source={item.image_url} style={{
                            marginStart: 10,
                            width: item.width,
                            height: 40,
                            resizeMode: 'contain',
                          }} />
                        {/* </View> */}
                    </View>
                  </TouchableOpacity>
                );
            })}
        </View>
    );
}

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'column',
    padding: 10,
  },
	container: {
    flex: 1,
    marginBottom: 10,
    alignItems: 'center',
    flexDirection: 'row',
		justifyContent: 'space-between',
	},
  imageContainer: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    flexDirection: 'row',
  },
  image: {
    marginStart: 10,
    width: 100,
    height: 40,
    resizeMode: 'contain',
  },
	radioCircle: {
		height: 20,
		width: 20,
		borderRadius: 100,
		borderWidth: 2,
		borderColor: BLUE,
		alignItems: 'center',
		justifyContent: 'center',
	},
	selectedRb: {
		width: 10,
		height: 10,
		borderRadius: 50,
		backgroundColor: BLUE,
  }
});