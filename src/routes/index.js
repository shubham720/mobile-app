import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image } from 'react-native';
import { 
  SPLASH_SCREEN_NAME,
  LOGIN_SCREEN_NAME,
  HOME_SCREEN_NAME,
  CITY_FROM_SCREEN_NAME,
  CITY_TO_SCREEN_NAME,
  SEARCH_SCREEN_NAME,
  SEARCH_RESULTS_SCREEN_NAME,
  SEAT_LAYOUT_SCREEN_NAME,
  PASSENGER_DETAILS_SCREEN_NAME,
  TRIP_PREVIEW_SCREEN_NAME,
  BOOKING_DETAILS_SCREEN_NAME,
  CHECKOUT_SCREEN_NAME,
  PAYMENT_SCREEN_NAME,
  LANGUAGE_SCREEN_NAME,
  RETURN_SEAT_LAYOUT_NAME,
  RETURN_SEARCH_RESULT_NAME,
  REGISTER_SCREEN_NAME,
  PAYMENT_CONFIRM_NAME,
  MODIFY_BOOKING_SCREEN_NAME,
  ACCOUNT_INFO_SCREEN_NAME,
  LOGIN_REGISTER_SCREEN_NAME,
  BOARDING_ARRIVAL_SCREEN_NAME,
  FILTER_BOOKINGS_SCREEN_NAME
 } from '../utilities/screenNames';
import {
  HOME_SCREEN,
  LOGIN_SCREEN,
  CITY_FROM_SCREEN,
  CITY_TO_SCREEN,
  SEARCH_SCREEN,
  SEARCH_RESULTS_SCREEN,
  SEAT_LAYOUT_SCREEN,
  PASSENGER_DETAILS_SCREEN,
  TRIP_PREVIEW_SCREEN,
  SPLASH_SCREEN,
  BOOKING_DETAILS_SCREEN,
  CHECKOUT_SCREEN,
  PAYMENT_SCREEN,
  LANGUAGE_SCREEN,
  REGISTER_SCREEN,
  RETURN_SEARCH_RESULT_SCREEN,
  RETURN_SEAT_LAYOUT_SCREEN,
  PAYMENT_CONFIRM_SCREEN,
  MODIFY_BOOKING_SCREEN,
  ACCOUNT_INFO_SCREEN,
  LOGIN_REGISTER_SCREEN,
  BOARDING_ARRIVAL_SCREEN,
  FILTER_BOOKINGS_SCREEN
} from '../screens';
import { BLUE, WHITE } from '../utilities/colors';

const Stack = createStackNavigator();

export default function Navigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: BLUE,
          },
          headerTintColor: WHITE,
          headerTitleStyle: {
            fontFamily: 'Montserrat-Bold'
          }
        }}
      >
        <Stack.Screen name={LOGIN_REGISTER_SCREEN_NAME} options={{headerShown: false}} component={LOGIN_REGISTER_SCREEN} />
        <Stack.Screen name={LANGUAGE_SCREEN_NAME} options={{headerShown: false,}} component={LANGUAGE_SCREEN} />
        <Stack.Screen name={LOGIN_SCREEN_NAME} options={{headerShown: false}} component={LOGIN_SCREEN} />
        <Stack.Screen name={HOME_SCREEN_NAME} options={{headerShown: false}} component={HOME_SCREEN} />
        <Stack.Screen name={REGISTER_SCREEN_NAME} options={{headerShown: false}} component={REGISTER_SCREEN} />
        <Stack.Screen name={CITY_FROM_SCREEN_NAME} options={{headerShown: false}} component={CITY_FROM_SCREEN} />
        <Stack.Screen name={CITY_TO_SCREEN_NAME} options={{headerShown: false}} component={CITY_TO_SCREEN} />
        <Stack.Screen name={SEARCH_SCREEN_NAME} component={SEARCH_SCREEN} />
        <Stack.Screen name={SEARCH_RESULTS_SCREEN_NAME} component={SEARCH_RESULTS_SCREEN} />
        <Stack.Screen name={SEAT_LAYOUT_SCREEN_NAME} component={SEAT_LAYOUT_SCREEN} />
        <Stack.Screen name={PASSENGER_DETAILS_SCREEN_NAME} component={PASSENGER_DETAILS_SCREEN} />
        <Stack.Screen name={TRIP_PREVIEW_SCREEN_NAME} component={TRIP_PREVIEW_SCREEN} />
        <Stack.Screen name={BOOKING_DETAILS_SCREEN_NAME} options={{headerShown: false}} component={BOOKING_DETAILS_SCREEN} />
        <Stack.Screen name={CHECKOUT_SCREEN_NAME} component={CHECKOUT_SCREEN} />
        <Stack.Screen name={PAYMENT_SCREEN_NAME} component={PAYMENT_SCREEN} />
        <Stack.Screen name={RETURN_SEARCH_RESULT_NAME} component={RETURN_SEARCH_RESULT_SCREEN} />
        <Stack.Screen name={RETURN_SEAT_LAYOUT_NAME} component={RETURN_SEAT_LAYOUT_SCREEN} />
        <Stack.Screen name={PAYMENT_CONFIRM_NAME} component={PAYMENT_CONFIRM_SCREEN} />
        <Stack.Screen name={MODIFY_BOOKING_SCREEN_NAME} component={MODIFY_BOOKING_SCREEN} />
        <Stack.Screen name={ACCOUNT_INFO_SCREEN_NAME} component={ACCOUNT_INFO_SCREEN} />
        <Stack.Screen name={BOARDING_ARRIVAL_SCREEN_NAME} component={BOARDING_ARRIVAL_SCREEN} />
        <Stack.Screen name={FILTER_BOOKINGS_SCREEN_NAME} component={FILTER_BOOKINGS_SCREEN} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}