import React from 'react';
import { StyleSheet, Text, View, Platform } from 'react-native';
import Navigator from './src/routes/index'
import {Provider} from 'react-redux';
import store from './src/redux/index';
import { ORANGE } from './src/utilities/colors';


export default function App() {

  const platform = Platform.OS;

  return (
    <Provider store={store}>
        { platform === 'ios' &&
          <View style={styles.iosContainer}>
            <Navigator/>
          </View>
        }
        { platform != 'ios' &&
          <View style={styles.container}>
            <Navigator/>
          </View>
        }
    </Provider>
  );
}

const styles = StyleSheet.create({
  // main:{
  //   backgroundColor
  // },
  container: {
    flex: 1,
    backgroundColor: ORANGE,
    justifyContent: 'center',
  },
  iosContainer: {
    paddingTop: 35,
    paddingBottom: 0,
    flex: 1,
    backgroundColor: ORANGE,
    justifyContent: 'center',
  }
});
